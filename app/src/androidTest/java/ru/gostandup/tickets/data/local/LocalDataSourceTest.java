package ru.gostandup.tickets.data.local;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.observers.TestObserver;
import ru.gostandup.tickets.data.DataSource;
import ru.gostandup.tickets.data.entities.Event;
import ru.gostandup.tickets.data.entities.Order;
import ru.gostandup.tickets.data.entities.Seat;
import ru.gostandup.tickets.data.entities.Ticket;
import ru.gostandup.tickets.data.entities.TicketUpdateInfo;
import ru.gostandup.tickets.injection.Injection;

/**
 * Created by ishmukhametov on 06.04.17.
 */
public class LocalDataSourceTest {

    private DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        dataSource = Injection.provideLocalDataSource(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void tearDown() throws Exception {
        dataSource.deleteAllEvents().blockingGet();
        dataSource.deleteAllTickets().blockingGet();
    }

    @Test
    public void saveEvents_retrievesEvents() {

        Event event1 = Event.builder()
                .setCity("city1")
                .setDate("date")
                .setId(123)
                .setPlace("place")
                .setTime("time")
                .setTitle("title")
                .build();

        Event event2 = Event.builder()
                .setCity("city2")
                .setDate("date")
                .setId(1234)
                .setPlace("place")
                .setTime("time")
                .setTitle("title")
                .build();

        List<Event> events1 = Arrays.asList(event1, event2);
        TestObserver<List<Event>> test = dataSource.saveEvents(events1)
                .flatMap(events -> dataSource.events)
                .test();

        test.assertValue(events1);
    }

    @Test
    public void saveTickets_retrievesTickets() {
        dataSource.deleteAllTickets().blockingGet();

        List<Seat> seats1 = Arrays.asList(Seat.create(0, 1, "a", "b", 1), Seat.create(2, 3, "a", "b", 4));
        List<Seat> seats2 = Arrays.asList(Seat.create(4, 5, "a", "b", 1), Seat.create(7, 8, "a", "b", 4));

        Collections.sort(seats1);
        Collections.sort(seats2);

        Ticket ticket1 = Ticket.create(1, "a", seats1, Order.create("a", "+5", 1));
        Ticket ticket2 = Ticket.create(2, "a", seats2, Order.create("a", "+5", 1));

        List<Ticket> tickets = Arrays.asList(ticket1, ticket2);
        int eventId = 666;
        TestObserver<List<Ticket>> test = dataSource.saveTickets(tickets, eventId)
                .flatMap(tickets1 -> dataSource.getTickets(eventId))
                .test();

        test.assertValue(tickets);
    }


    @Test
    public void saveTicket_retrievesTicket() {

        dataSource.deleteAllTickets().blockingGet();

        List<Seat> seats1 = Arrays.asList(Seat.create(0, 1, "a", "b", 1), Seat.create(2, 3, "a", "b", 4));

        Collections.sort(seats1);
        Ticket ticket1 = Ticket.create(1, "a", seats1, Order.create("a", "+5", 1));

        int eventId = 777;
        TestObserver<Ticket> test = dataSource.saveTickets(Collections.singletonList(ticket1), eventId)
                .flatMap(tickets1 -> dataSource.getTicket(ticket1.id()))
                .test();

        test.assertValue(ticket1);
    }

    @Test
    public void updateTicketStatus_retrievesUpdatedTicket() {

        dataSource.deleteAllTickets().blockingGet();

        Seat seat = Seat.create(0, 1, "a", "b", 1);
        List<Seat> seats1 = Arrays.asList(seat, Seat.create(2, 3, "a", "b", 4));

        Collections.sort(seats1);
        Ticket ticket1 = Ticket.create(1, "a", seats1, Order.create("a", "+5", 1));

        int eventId = 777;
        TestObserver<Ticket> test = dataSource.saveTickets(Collections.singletonList(ticket1), eventId)
                .flatMap(tickets1 -> dataSource.updateTicketStatus(eventId, ticket1.id(), seat.row(), seat.col(), 100))
                .test();


        test.assertValue(ticket -> ticket.seats().get(0).status() == 100);
    }

    @Test
    public void addTicketUpdateToQueue_retrievesUpdateInfo() {

        dataSource.deleteAllTickets().blockingGet();

        Seat seat1 = Seat.create(0, 1, "a", "b", 1);
        Seat seat2 = Seat.create(2, 3, "a", "b", 4);
        List<Seat> seats1 = Collections.singletonList(seat1);
        List<Seat> seats2 = Collections.singletonList(seat2);


        int ticket1Id = 8941;
        int ticket2Id = 3412;
        Ticket ticket1 = Ticket.create(ticket1Id, "a", seats1, Order.create("a", "+5", 1));
        Ticket ticket2 = Ticket.create(ticket2Id, "a", seats2, Order.create("a", "+5", 1));

        int eventId = 777;

        dataSource.saveTickets(Arrays.asList(ticket1, ticket2), eventId).blockingGet();



        TestObserver<List<TicketUpdateInfo>> test = dataSource.addToUpdateTicketQueue(ticket1Id, seat1.row(),seat1.col(), 10)
                .mergeWith(dataSource.addToUpdateTicketQueue(ticket2Id, seat2.row(),seat2.col(), 10))
                .lastOrError()
                .flatMap(ticket -> dataSource.getUpdateTicketQueue())
                .test();


        test.assertValue(Arrays.asList(TicketUpdateInfo.create(ticket1Id, seat1.row(),seat1.col(), 10),
                TicketUpdateInfo.create(ticket2Id, seat2.row(),seat2.col(), 10)));
    }


    @Test
    public void removeTicketUpdateToQueue_retrievesEmptyUpdateInfo() {

        dataSource.deleteAllTickets().blockingGet();

        Seat seat1 = Seat.create(0, 1, "a", "b", 1);
        Seat seat2 = Seat.create(2, 3, "a", "b", 4);
        List<Seat> seats1 = Collections.singletonList(seat1);
        List<Seat> seats2 = Collections.singletonList(seat2);


        int ticket1Id = 8941;
        int ticket2Id = 3412;
        Ticket ticket1 = Ticket.create(ticket1Id, "a", seats1, Order.create("a", "+5", 1));
        Ticket ticket2 = Ticket.create(ticket2Id, "a", seats2, Order.create("a", "+5", 1));

        int eventId = 777;

        dataSource.saveTickets(Arrays.asList(ticket1, ticket2), eventId).blockingGet();



        dataSource.addToUpdateTicketQueue(ticket1Id, seat1.row(),seat1.col(), 10)
                .mergeWith(dataSource.addToUpdateTicketQueue(ticket2Id, seat2.row(),seat2.col(), 10))
                .toList().blockingGet();



        TestObserver<List<TicketUpdateInfo>> test = dataSource.removeTicketUpdateInfo(ticket1Id, seat1.row(),seat1.col())
                .mergeWith(dataSource.removeTicketUpdateInfo(ticket2Id, seat2.row(),seat2.col()))
                .lastOrError()
                .flatMap(ticket -> dataSource.getUpdateTicketQueue())
                .test();

        test.assertValue(ticketUpdateInfos -> ticketUpdateInfos.size() == 0);
    }

}