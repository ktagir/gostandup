package ru.gostandup.tickets.matchers;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.appcompat.view.menu.ActionMenuItemView;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class MenuIconMatcher extends TypeSafeMatcher<View> {

    private final int expectedId;
    String resourceName;

    public MenuIconMatcher(int expectedId) {
        super(View.class);
        this.expectedId = expectedId;
    }


    @Override
    protected boolean matchesSafely(View target) {
        if (!(target instanceof ActionMenuItemView)){
            return false;
        }
        ActionMenuItemView menuItemView = (ActionMenuItemView) target;
        if (expectedId < 0){
            return menuItemView.getItemData().getIcon()== null;
        }
        Resources resources = target.getContext().getResources();
        Drawable expectedDrawable = resources.getDrawable(expectedId);
        resourceName = resources.getResourceEntryName(expectedId);

        if (expectedDrawable == null) {
            return false;
        }

        Bitmap bitmap = ((BitmapDrawable) menuItemView.getItemData().getIcon()).getBitmap();
        Bitmap otherBitmap = ((BitmapDrawable) expectedDrawable).getBitmap();
        return bitmap.sameAs(otherBitmap);
    }


    @Override
    public void describeTo(Description description) {
        description.appendText("with drawable from resource id: ");
        description.appendValue(expectedId);
        if (resourceName != null) {
            description.appendText("[");
            description.appendText(resourceName);
            description.appendText("]");
        }
    }
}
