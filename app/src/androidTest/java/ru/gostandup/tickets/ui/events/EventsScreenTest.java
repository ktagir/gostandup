package ru.gostandup.tickets.ui.events;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import ru.gostandup.tickets.R;
import ru.gostandup.tickets.data.entities.Event;
import ru.gostandup.tickets.injection.Injection;
import ru.gostandup.tickets.ui.main.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by ishmukhametov on 22.03.17.
 */
public class EventsScreenTest {

    private static final List<Event> EVENTS = getEvents();
    private static final String TITLE_1 = "titleMock1";
    private static final String TITLE_2 = "titleMock2";


    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    super.beforeActivityLaunched();
                    saveTestEvents(EVENTS);
                }
            };

    private Matcher<View> withItemText(final String itemText) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View item) {
                return allOf(
                        isDescendantOfA(isAssignableFrom(RecyclerView.class)),
                        withText(itemText)).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is isDescendantOfA LV with text " + itemText);
            }
        };
    }

    @Test
    public void eventsToolbarTitle_DisplayedInUi() {
        onView(withText(R.string.events)).check(matches(isDisplayed()));
    }

    @Test
    public void events_DisplayedInUi() throws Exception {
        onView(withItemText(TITLE_1)).check(matches(isDisplayed()));
        onView(withItemText(TITLE_2)).check(matches(isDisplayed()));
    }

    private void saveTestEvents(List<Event> events){
        Injection.provideDataSource(InstrumentationRegistry.getTargetContext()).saveEvents(events).blockingGet();
    }

    private static List<Event> getEvents() {
        Event event1 = Event.builder()
                .setCity("city1")
                .setDate("date")
                .setId(123)
                .setPlace("place")
                .setTime("time")
                .build();
        Event event2 = Event.builder()
                .setCity("city2")
                .setDate("date")
                .setId(1234)
                .setPlace("place")
                .setTime("time")
                .build();;
        return Arrays.asList(event1, event2);
    }
}