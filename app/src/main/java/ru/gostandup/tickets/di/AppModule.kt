package ru.gostandup.tickets.di

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import ru.gostandup.tickets.GoStandUpDb
import ru.gostandup.tickets.model.Repository
import ru.gostandup.tickets.model.auth.AuthHolder
import ru.gostandup.tickets.model.auth.GoStandUpAuthHolder
import ru.gostandup.tickets.model.createQueryWrapper
import ru.gostandup.tickets.platform.driver
import ru.gostandup.tickets.platform.network.RestClient
import ru.gostandup.tickets.platform.preferences.Preferences
import ru.gostandup.tickets.platform.sqlOpenHelper
import ru.gostandup.tickets.ui.control.ControlContract
import ru.gostandup.tickets.ui.control.ControlPresenter
import ru.gostandup.tickets.ui.control.history.HistoryContract
import ru.gostandup.tickets.ui.control.history.HistoryPresenter
import ru.gostandup.tickets.ui.control.statistics.StatisticsContract
import ru.gostandup.tickets.ui.control.statistics.StatisticsPresenter
import ru.gostandup.tickets.ui.events.EventsContract
import ru.gostandup.tickets.ui.events.EventsPresenter
import ru.gostandup.tickets.ui.login.LoginContract
import ru.gostandup.tickets.ui.login.LoginPresenter
import ru.gostandup.tickets.ui.navigator.RouterImpl
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {


    single<Preferences> { Preferences(androidApplication()) }
}

val dataModule = module {

    single<AuthHolder> { GoStandUpAuthHolder(get()) }

    single<GoStandUpDb> {
        createQueryWrapper(driver(sqlOpenHelper("database.db", androidApplication())))
    }

    single<RestClient> {

        RestClient(get(), get()) }

    single<Repository> { Repository.Impl(get(), get(), get()) }
}

val navigationModule = module {

    single<Cicerone<Router>> { Cicerone.create() }

    single<Router> { get<Cicerone<Router>>().router }

    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }

    single<ru.gostandup.tickets.ui.navigator.Router> { RouterImpl(get()) }
}

val mvpModule = module {

    factory<LoginContract.Presenter> { LoginPresenter(get(), get()) }

    factory<EventsContract.Presenter> { EventsPresenter(get(), get()) }

    factory<ControlContract.Presenter> { (eventId: Long) -> ControlPresenter(eventId, get()) }

    factory<HistoryContract.Presenter> { (eventId: Long) -> HistoryPresenter(eventId, get()) }

    factory<StatisticsContract.Presenter> { (eventId: Long) -> StatisticsPresenter(eventId, get()) }
}

val allModules = developerSettingsModule + appModule + navigationModule + dataModule + mvpModule