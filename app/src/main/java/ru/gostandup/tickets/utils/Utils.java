package ru.gostandup.tickets.utils;

import android.util.Base64;

public final class Utils {

    private Utils(){
        throw  new UnsupportedOperationException();
    }

    public static String encodeAuthData(String login, String passwd) {
        byte[] data = (login + ":" + passwd).getBytes();
        return Base64.encodeToString(data, Base64.DEFAULT).trim();
    }
}
