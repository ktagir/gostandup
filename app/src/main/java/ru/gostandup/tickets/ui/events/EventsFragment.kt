package ru.gostandup.tickets.ui.events

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import org.koin.android.ext.android.get
import ru.gostandup.tickets.R
import ru.gostandup.tickets.data.entries.EventEntry
import ru.gostandup.tickets.ui.main.FragmentInteractionListener
import su.tagir.lib.android.mvp.BindingListAdapter
import su.tagir.lib.android.mvp.BindingViewHolder
import su.tagir.lib.android.mvp.MvpListFragment

class EventsFragment : MvpListFragment<EventItem, EventsContract.View, EventsContract.Presenter>(), EventsContract.View {
    private var interactionListener: FragmentInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        interactionListener = context as FragmentInteractionListener
    }

    override fun onDetach() {
        interactionListener = null
        super.onDetach()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_events, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        toolbar.setNavigationOnClickListener { v: View? ->
            if (interactionListener != null) {
                interactionListener!!.onSandwichClick()
            }
        }
        presenter.loadData()
    }

    override fun createAdapter(): BindingListAdapter<EventItem> {
        return EventsAdapter { event -> presenter.showEvent(event)}
    }

    override fun createPresenter(): EventsContract.Presenter {
        return get()
    }

    private class EventsAdapter(val onSelect: (EventEntry) -> Unit) : BindingListAdapter<EventItem>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.event_list_item, parent, false)
            val holder = ViewHolder(view)
            holder.itemView.setOnClickListener {
                val event = items[holder.adapterPosition]
                onSelect(event.event)
            }
            return holder
        }

        override fun areItemsTheSame(oldItem: EventItem, newItem: EventItem): Boolean {
            return oldItem.event.id == newItem.event.id
        }

        override fun areContentsTheSame(oldItem: EventItem, newItem: EventItem): Boolean {
            return oldItem == newItem
        }

        private class ViewHolder(itemView: View) : BindingViewHolder<EventItem>(itemView) {
            private val city: TextView = itemView.findViewById(R.id.cityTimeTextView)
            private val title: TextView = itemView.findViewById(R.id.titleTextView)
            private val places: TextView = itemView.findViewById(R.id.numberPlacesTextView)
            private val club: TextView = itemView.findViewById(R.id.clubTextView)

            @SuppressLint("SetTextI18n")
            override fun bind(t: EventItem) {
                city.text = t.event.city + " " + t.event.date
                title.text = t.event.title
                places.text = t.ticketsCount.toString()
                club.text = t.event.place
            }

            init {
                places.visibility = View.GONE
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): EventsFragment {
            val args = Bundle()
            val fragment = EventsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}