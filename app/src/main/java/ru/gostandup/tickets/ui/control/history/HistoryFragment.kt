package ru.gostandup.tickets.ui.control.history

import android.annotation.SuppressLint
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.format.DateFormat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.widget.PopupWindowCompat
import androidx.recyclerview.widget.AsyncListDiffer
import org.koin.android.ext.android.get
import org.koin.core.parameter.parametersOf
import ru.gostandup.tickets.R
import ru.gostandup.tickets.data.entries.TicketFull
import ru.gostandup.tickets.data.entries.TicketWithSeat
import su.tagir.lib.android.mvp.BindingListAdapter
import su.tagir.lib.android.mvp.BindingViewHolder
import su.tagir.lib.android.mvp.MvpListFragment
import java.util.*

class HistoryFragment : MvpListFragment<TicketWithSeat, HistoryContract.View, HistoryContract.Presenter>(), HistoryContract.View, AsyncListDiffer.ListListener<TicketWithSeat> {

    companion object {
        @JvmStatic
        fun newInstance(eventId: Long): HistoryFragment {
            val args = Bundle()
            args.putLong("eventId", eventId)
            val f = HistoryFragment()
            f.arguments = args
            return f
        }
    }

    override fun createPresenter(): HistoryContract.Presenter {
        val eventId = requireArguments().getLong("eventId")
        return get { parametersOf(eventId) }
    }

    override fun createAdapter(): BindingListAdapter<TicketWithSeat> = TicketsAdapter()

    override fun showInfo(ticket: TicketFull) {
        val layoutInflater = requireActivity().baseContext
                .getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val v = layoutInflater.inflate(R.layout.popup_layout, null)

        v.findViewById<TextView>(R.id.name).text = ticket.fio
        v.findViewById<TextView>(R.id.phone).text = ticket.phone
        v.findViewById<TextView>(R.id.ticket_class).text = ticket.code

        val popupWindow = PopupWindow(
                v,
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)


        popupWindow.setBackgroundDrawable(BitmapDrawable())
        popupWindow.isOutsideTouchable = true

        PopupWindowCompat.showAsDropDown(popupWindow,requireView(), 0, -requireView().height, Gravity.CENTER_HORIZONTAL)
    }

    override fun onResume() {
        super.onResume()
        adapter.addListener(this)
    }

    override fun onPause() {
        adapter.removeListener(this)
        super.onPause()
    }

    private inner class TicketsAdapter : BindingListAdapter<TicketWithSeat>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<TicketWithSeat> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_ticket, parent, false)
            val holder = ViewHolder(view)
            holder.itemView.setOnLongClickListener {
                val ticket = items[holder.adapterPosition]
                presenter.showTicket(ticket)
                true
            }
            return holder
        }

        override fun areItemsTheSame(oldItem: TicketWithSeat, newItem: TicketWithSeat): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TicketWithSeat, newItem: TicketWithSeat): Boolean {
            return oldItem.isUsed == newItem.isUsed
                    && oldItem.passTime == oldItem.passTime
                    && newItem.isAdmission == oldItem.isAdmission
        }

        inner class ViewHolder(itemView: View) : BindingViewHolder<TicketWithSeat>(itemView) {
            val place: TextView
            val passed: TextView

            @SuppressLint("SetTextI18n")
            override fun bind(t: TicketWithSeat) {
                var placeStr = t.sector ?: ""
                if (!t.row.isNullOrBlank()) {
                    if (placeStr.isNotBlank()) {
                        placeStr += ", "
                    }
                    placeStr += getString(R.string.row, t.row)
                }
                if (!t.column.isNullOrBlank()) {
                    if (placeStr.isNotBlank()) {
                        placeStr += ", "
                    }
                    placeStr += getString(R.string.place, t.column)
                }
                if(placeStr.isBlank()){
                    placeStr = "Без места"
                }
                place.text = placeStr

                passed.visibility = if (t.isUsed) View.VISIBLE else View.GONE
                val passTime = DateFormat.getTimeFormat(itemView.context).format(Date((t.passTime?.times(1000))
                        ?: 0))
                passed.text = getString(R.string.passed) + "\n" + passTime
            }

            init {
                place = itemView.findViewById(R.id.place)
                passed = itemView.findViewById(R.id.passed)
            }
        }
    }

    override fun onCurrentListChanged(previousList: MutableList<TicketWithSeat>, currentList: MutableList<TicketWithSeat>) {
        if(previousList.size > 0 && currentList.size > 0 &&
                currentList[0].id != previousList[0].id) {
            list?.scrollToPosition(0)
        }
    }
}