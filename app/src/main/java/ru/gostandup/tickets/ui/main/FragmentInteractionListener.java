package ru.gostandup.tickets.ui.main;

public interface FragmentInteractionListener {

    void onSandwichClick();
}
