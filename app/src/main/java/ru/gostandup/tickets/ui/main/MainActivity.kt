package ru.gostandup.tickets.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import org.koin.android.ext.android.inject
import ru.gostandup.tickets.R
import ru.gostandup.tickets.databinding.ActivityMainBinding
import ru.gostandup.tickets.model.auth.AuthHolder
import ru.gostandup.tickets.model.auth.SessionListener
import ru.gostandup.tickets.ui.events.EventsFragment.Companion.newInstance
import ru.gostandup.tickets.ui.login.LoginFragment
import ru.gostandup.tickets.ui.navigator.Router
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import su.tagir.lib.developersettings.other.ViewModifier

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, FragmentInteractionListener {

    private val navigatorHolder: NavigatorHolder by inject()
    private val authHolder: AuthHolder by inject()
    private val router: Router by inject()
    private val viewModifier: ViewModifier by inject()

    private lateinit var binding: ActivityMainBinding

    var nameTextView: TextView? = null
    var emailTextView: TextView? = null
    var noConnection: TextView? = null



    private val connectionBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.extras == null) return
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = manager.activeNetworkInfo
            if (ni != null && ni.state == NetworkInfo.State.CONNECTED) {
                noConnection!!.visibility = View.GONE
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, java.lang.Boolean.FALSE)) {
                noConnection!!.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewModifier.modify(binding.root))
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        noConnection = findViewById(R.id.no_connection)

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        prepareNavHeader(navigationView.getHeaderView(0))
        if (authHolder.accessToken == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, LoginFragment())
                    .commitNowAllowingStateLoss()
        } else {
            initMainScreen()
        }
        authHolder.subscribeToSessionExpired(object : SessionListener{
            override fun sessionExpired() {
                router.navigateToLogin()
            }

        })
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(connectionBroadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        unregisterReceiver(connectionBroadcastReceiver)
        super.onStop()
    }

    private fun prepareNavHeader(view: View) {
        nameTextView = view.findViewById(R.id.userNameTextView)
        emailTextView = view.findViewById(R.id.emailTextView)

        val logout = view.findViewById<View>(R.id.logout)
        logout.setOnClickListener {
            authHolder.refresh()
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            }
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.nav_events) {
            onShowEvents()
        } else if (id == R.id.nav_info) {
            showAbout()
        }
        item.isChecked = false
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun uncheckedNavigationMenu() {
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val size = navigationView.menu.size()
        for (i in 0 until size) {
            navigationView.menu.getItem(i).isChecked = false
        }
    }

    private fun initMainScreen() {
        if (supportFragmentManager.findFragmentById(R.id.content_main) == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, newInstance())
                    .commitNowAllowingStateLoss()
        }
    }

    private fun onShowEvents() {
        router.navigateToEvents()
    }

    private fun showAbout() {
        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            val view: View = LayoutInflater.from(this).inflate(R.layout.dialog_about, null)
            val textView = view.findViewById<TextView>(R.id.version)
            textView.text = version
            AlertDialog.Builder(this)
                    .setTitle("О приложении")
                    .setView(view)
                    .setPositiveButton("OK", null)
                    .create()
                    .show()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private val navigator: Navigator = object : SupportAppNavigator(this, R.id.content_main) {}

    override fun onSandwichClick() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.openDrawer(GravityCompat.START)
        }
    }
}