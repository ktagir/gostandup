package ru.gostandup.tickets.ui.login

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.material.textfield.TextInputLayout
import org.koin.android.ext.android.get
import ru.gostandup.tickets.R
import su.tagir.lib.android.mvp.MvpFragment

class LoginFragment : MvpFragment<LoginContract.View, LoginContract.Presenter>(), LoginContract.View {

    private var loginInputLayout: TextInputLayout? = null
    private var passwordInputLayout: TextInputLayout? = null
    private var errorTextView: TextView? = null
    private var progressBar: ProgressBar? = null

    override fun createPresenter(): LoginContract.Presenter {
        return get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_login, container, false)
        loginInputLayout = view.findViewById(R.id.loginInputLayout)
        passwordInputLayout = view.findViewById(R.id.passwordInputLayout)
        progressBar = view.findViewById(R.id.progress)
        errorTextView = view.findViewById(R.id.errorTextView)
        progressBar!!.setVisibility(View.GONE)
        errorTextView!!.setVisibility(View.GONE)
        errorTextView!!.setText(R.string.login_error)
//        loginInputLayout!!.getEditText()!!.setText(Injection.providePrefs(container!!.context).getEmail())
        val editText = passwordInputLayout!!.getEditText()
        editText?.setOnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login()
                return@setOnEditorActionListener true
            }
            false
        }
        view.findViewById<View>(R.id.loginBtn).setOnClickListener { v: View? -> login() }
        return view
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            progressBar!!.visibility = View.VISIBLE
            errorTextView!!.visibility = View.GONE
            loginInputLayout!!.isEnabled = false
            passwordInputLayout!!.isEnabled = false
        } else {
            progressBar!!.visibility = View.GONE
            loginInputLayout!!.isEnabled = true
            passwordInputLayout!!.isEnabled = true
        }
    }

    override fun showError(t: Throwable) {
        errorTextView!!.visibility = View.VISIBLE
        loginInputLayout!!.isEnabled = true
        passwordInputLayout!!.isEnabled = true
    }

    override fun showEmptyLoginError() {
        loginInputLayout!!.error = "Поле не может быть пустым"
    }

    override fun showEmptyPasswordError() {
        passwordInputLayout!!.error = "Поле не может быть пустым"
    }

    override fun hideInputErrors() {
        loginInputLayout!!.error = null
    }

    private fun login() {
        if (loginInputLayout!!.editText == null) {
            return
        }
        if (passwordInputLayout!!.editText == null) {
            return
        }
        val login = loginInputLayout!!.editText!!.text.toString()
        val password = passwordInputLayout!!.editText!!.text.toString()
        presenter.loginWith(login, password)
    }
}