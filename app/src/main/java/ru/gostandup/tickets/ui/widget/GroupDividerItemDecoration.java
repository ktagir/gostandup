package ru.gostandup.tickets.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.ColorInt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GroupDividerItemDecoration extends RecyclerView.ItemDecoration {
    public static final int HORIZONTAL = LinearLayout.HORIZONTAL;
    public static final int VERTICAL = LinearLayout.VERTICAL;
    private static final int[] ATTRS = new int[]{ android.R.attr.listDivider };



    private Drawable mDivider;

    private int mOrientation;
    private final Rect mBounds = new Rect();
    private static final int MARGIN = 40;
    private Paint paint = new Paint();

    public GroupDividerItemDecoration(Context context, int orientation, @ColorInt int color) {
        setOrientation(orientation);
        paint.setColor(color);
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
    }


    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL && orientation != VERTICAL) {
            throw new IllegalArgumentException(
                    "Invalid orientation. It should be either HORIZONTAL or VERTICAL");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (parent.getLayoutManager() == null || !(parent.getLayoutManager() instanceof LinearLayoutManager)) {
            return;
        }
        if (mOrientation == VERTICAL) {
            drawVertical(c, parent);
        } else {
        }
    }

    @SuppressLint("NewApi")
    private void drawVertical(Canvas canvas, RecyclerView parent) {
//        if (!(parent.getAdapter() instanceof GroupedAdapter)){
//            throw new ClassCastException("It seems that recycler adapter does not implement GroupedAdapter");
//        }

//        GroupedAdapter adapter = (GroupedAdapter) parent.getAdapter();
//        canvas.save();
//        final int left;
//        final int right;
//        if (parent.getClipToPadding()) {
//            left = parent.getPaddingLeft();
//            right = parent.getWidth() - parent.getPaddingRight();
//            canvas.clipRect(left, parent.getPaddingTop(), right,
//                    parent.getHeight() - parent.getPaddingBottom());
//        } else {
//            left = 0;
//            right = parent.getWidth();
//        }
//
//        LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
//
//        int first = layoutManager.findFirstVisibleItemPosition();
//        if (first < 0) return;
//
//        int last = layoutManager.findLastVisibleItemPosition();
//
//        for (int i = first; i <= last; i++) {
//            if (adapter.isGroupEndOnPosition(i)){
//                final View child = layoutManager.findViewByPosition(i);
//                parent.getDecoratedBoundsWithMargins(child, mBounds);
//                final int bottom = mBounds.bottom + Math.round(ViewCompat.getTranslationY(child));
//                final int top = bottom - MARGIN;
//                canvas.drawRect(left, top, right, bottom, paint);
//
//                ViewCompat.setElevation(child, 3.0f);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    child.setOutlineProvider(OutlineProvider.WOTHOUT_HORIZONTAL);
//                    parent.setClipToPadding(false);
//                }
//            }else {
//                final View child = layoutManager.findViewByPosition(i);
//                parent.getDecoratedBoundsWithMargins(child, mBounds);
//                final int bottom = mBounds.bottom + Math.round(ViewCompat.getTranslationY(child));
//                final int top = bottom - mDivider.getIntrinsicHeight();
//                mDivider.setBounds(left, top, right, bottom);
//                mDivider.draw(canvas);
//            }
//        }
//        canvas.restore();
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {

//        if (!(parent.getAdapter() instanceof GroupedAdapter)){
//            throw new ClassCastException("It seems that recycler adapter does not implement GroupedAdapter");
//        }
//
//        GroupedAdapter adapter = (GroupedAdapter) parent.getAdapter();
//        int position = parent.getChildLayoutPosition(view);
//        boolean groupStarted = adapter.isGroupEndOnPosition(position);
//        if (groupStarted){
//            if (mOrientation == VERTICAL) {
//                outRect.set(0, 0, 0, MARGIN);
//            } else {
//                outRect.set(0, 0, MARGIN, 0);
//            }
//        }else {
//            if (mOrientation == VERTICAL) {
//                outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
//            } else {
//                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
//            }
//        }
    }
}
