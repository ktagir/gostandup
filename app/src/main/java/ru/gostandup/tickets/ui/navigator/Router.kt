package ru.gostandup.tickets.ui.navigator

import androidx.fragment.app.Fragment
import ru.gostandup.tickets.ui.control.ControlFargment
import ru.gostandup.tickets.ui.events.EventsFragment
import ru.gostandup.tickets.ui.login.LoginFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class RouterImpl(private val router: ru.terrakok.cicerone.Router) : Router{

    override fun navigateToLogin() {
        router.newRootScreen(Screens.Login)
    }

    override fun navigateToEvents() {
        router.newRootScreen(Screens.Events)
    }

    override fun navigateToControl(eventId: Long) {
        return router.navigateTo(Screens.Control(eventId))
    }

    override fun pop() {
        router.exit()
    }

    override fun dismiss() {
        router.exit()
    }

}

object Screens{

    object Login: SupportAppScreen(){
        override fun getFragment(): Fragment? {
            return LoginFragment()
        }
    }

    object Events: SupportAppScreen(){
        override fun getFragment(): Fragment? {
            return EventsFragment()
        }
    }

    data class Control(val eventId: Long): SupportAppScreen(){
        override fun getFragment(): Fragment? {
            return ControlFargment.newInstance(eventId)
        }
    }
}