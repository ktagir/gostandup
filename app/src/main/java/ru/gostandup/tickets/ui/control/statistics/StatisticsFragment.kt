package ru.gostandup.tickets.ui.control.statistics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.koin.android.ext.android.get
import org.koin.core.parameter.parametersOf
import ru.gostandup.tickets.R
import ru.gostandup.tickets.model.entities.StatisticsItem
import su.tagir.lib.android.mvp.BindingListAdapter
import su.tagir.lib.android.mvp.BindingViewHolder
import su.tagir.lib.android.mvp.MvpListFragment

class StatisticsFragment : MvpListFragment<StatisticsItem, StatisticsContract.View, StatisticsContract.Presenter>(), StatisticsContract.View {

    companion object{

        @JvmStatic
        fun newInstance(eventId: Long): StatisticsFragment{
            val args = Bundle()
            args.putLong("eventId", eventId)
            val f = StatisticsFragment()
            f.arguments = args
            return f
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshLayout?.isEnabled = false
    }

    override fun createPresenter(): StatisticsContract.Presenter {
        val eventId = requireArguments().getLong("eventId")
        return get { parametersOf(eventId) }
    }

    override fun createAdapter(): BindingListAdapter<StatisticsItem> = StatisticsAdapter()

    class StatisticsAdapter : BindingListAdapter<StatisticsItem>() {

        override fun areItemsTheSame(oldItem: StatisticsItem, newItem: StatisticsItem): Boolean {
            return oldItem.type == newItem.type
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<StatisticsItem> {
            val inflater = LayoutInflater.from(parent.context)
            return StatisticViewHolder(inflater.inflate(R.layout.stat_list_item, parent, false))
        }

        override fun areContentsTheSame(oldItem: StatisticsItem, newItem: StatisticsItem): Boolean {
            return oldItem.count == newItem.count
        }

    }

    class StatisticViewHolder(view: View) : BindingViewHolder<StatisticsItem>(view) {

        val title: TextView = itemView.findViewById(R.id.title)
        val count: TextView = itemView.findViewById(R.id.value)

        override fun bind(t: StatisticsItem) {
            title.text = t.type.name
            count.text = t.count.toString()
        }

    }

}

