package ru.gostandup.tickets.ui.keyboard;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import ru.gostandup.tickets.R;

public class KeyboardView extends ConstraintLayout implements View.OnClickListener {


    private String inputText = "";
    private TextView codeTextView;

    private static final int DEFAULT_CODE_LEN = 6;

    private int codeLen = DEFAULT_CODE_LEN;

    private CodeListener codeListener;


    public KeyboardView(Context context) {
        super(context);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        LayoutInflater factory = LayoutInflater.from(getContext());
        factory.inflate(R.layout.keyboard_layout, this, true);
        codeTextView = (TextView) findViewById(R.id.codeTextView);
        ImageButton clearButton = findViewById(R.id.clearButton);
        setClickListenerForButtons();
        clearButton.setOnClickListener(v -> clearText());
    }

    private void setClickListenerForButtons() {
        ViewGroup root = (ViewGroup) getChildAt(0);
        int childCount = root.getChildCount();
        for (int index = 0; index < childCount; ++index) {
            View nextChild = root.getChildAt(index);
            if (nextChild instanceof Button) {
                nextChild.setOnClickListener(this);
            }
        }
    }

    public void setCodeLen(int len) {
        codeLen = len;
    }

    public void setCodeListener(CodeListener codeListener) {
        this.codeListener = codeListener;
    }

    @Override
    public void onClick(View v) {
        char c = ((Button) v).getText().charAt(0);

//        switch (c){
//            case '1':
//                toneG.startTone(ToneGenerator.TONE_DTMF_1, 100);
//                break;
//            case '2':
//                toneG.startTone(ToneGenerator.TONE_DTMF_2, 100);
//                break;
//            case '3':
//                toneG.startTone(ToneGenerator.TONE_DTMF_3, 100);
//                break;
//            case '4':
//                toneG.startTone(ToneGenerator.TONE_DTMF_4, 100);
//                break;
//            case '5':
//                toneG.startTone(ToneGenerator.TONE_DTMF_5, 100);
//                break;
//            case '6':
//                toneG.startTone(ToneGenerator.TONE_DTMF_6, 100);
//                break;
//            case '7':
//                toneG.startTone(ToneGenerator.TONE_DTMF_7, 100);
//                break;
//            case '8':
//                toneG.startTone(ToneGenerator.TONE_DTMF_8, 100);
//                break;
//            case '9':
//                toneG.startTone(ToneGenerator.TONE_DTMF_9, 100);
//                break;
//            case '0':
//                toneG.startTone(ToneGenerator.TONE_DTMF_0, 100);
//                break;
//            case 'A':
//                toneG.startTone(ToneGenerator.TONE_DTMF_A, 100);
//                break;
//            case 'B':
//                toneG.startTone(ToneGenerator.TONE_DTMF_B, 100);
//                break;
//            case 'C':
//                toneG.startTone(ToneGenerator.TONE_DTMF_C, 100);
//                break;
//            case 'D':
//                toneG.startTone(ToneGenerator.TONE_DTMF_D, 100);
//                break;
//            case 'E':
//                toneG.startTone(ToneGenerator.TONE_DTMF_A, 100);
//                break;
//            case 'F':
//                toneG.startTone(ToneGenerator.TONE_DTMF_B, 100);
//                break;
//        }
        handleChar(c);
    }

    private void handleChar(char c) {
        if (inputText.length() < codeLen) {
            inputText += c;
            notifyChangedCodeTextView();
            if (inputText.length() == codeLen) {
                if (codeListener != null) {
                    codeListener.onNextCode(inputText);
                }
            }
        }
    }

    public interface CodeListener {
        void onNextCode(CharSequence code);
    }


    private void notifyChangedCodeTextView() {
        codeTextView.setText(inputText);
    }


    public void clearText() {
        inputText = "";
        notifyChangedCodeTextView();
    }
}
