package ru.gostandup.tickets.ui.widget;

import android.graphics.Outline;
import android.os.Build;
import android.view.View;
import android.view.ViewOutlineProvider;

import androidx.annotation.RequiresApi;

/**
 * Created by ishmukhametov on 26.03.17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class OutlineProvider {

    public static final ViewOutlineProvider WOTHOUT_HORIZONTAL = new ViewOutlineProvider() {
        @Override
        public void getOutline(View view, Outline outline) {
            outline.setRect(-100, 0, view.getWidth() + 100, view.getHeight());
        }
    };

}
