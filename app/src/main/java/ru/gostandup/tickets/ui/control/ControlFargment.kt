package ru.gostandup.tickets.ui.control

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.*
import android.text.format.DateFormat
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.zxing.BarcodeFormat
import com.google.zxing.DecodeHintType
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView.TorchListener
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import org.koin.android.ext.android.get
import org.koin.core.parameter.parametersOf
import ru.gostandup.tickets.R
import ru.gostandup.tickets.data.entries.TicketFull
import ru.gostandup.tickets.databinding.LayoutControlScreenBinding
import ru.gostandup.tickets.ui.control.history.HistoryFragment
import ru.gostandup.tickets.ui.control.statistics.StatisticsFragment
import ru.gostandup.tickets.ui.main.FragmentInteractionListener
import su.tagir.lib.android.mvp.MvpFragment
import su.tagir.lib.android.mvp.ProgressDialog
import timber.log.Timber
import java.util.*
import java.util.regex.Pattern

class ControlFargment : MvpFragment<ControlContract.View, ControlContract.Presenter>(R.layout.layout_control_screen), ControlContract.View, TorchListener, BarcodeCallback, Toolbar.OnMenuItemClickListener {

    private var vibrator: Vibrator? = null

    private var barcodeMenuItem: MenuItem? = null
    private var keyboardMenuItem: MenuItem? = null
    private var autofocusMenuItem: MenuItem? = null
    private var torchMenuItem: MenuItem? = null
    private var handler: Handler? = null
    private var toneG: ToneGenerator? = null
    private var interactionListener: FragmentInteractionListener? = null
    private var lastScanning: String? = null

    private val binding: LayoutControlScreenBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handler = Handler(Looper.getMainLooper())
        toneG = ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar: Toolbar = binding.toolbar
        toolbar.setNavigationOnClickListener {
            if (interactionListener != null) {
                interactionListener!!.onSandwichClick()
            }
        }
        toolbar.inflateMenu(R.menu.menu_control)
        toolbar.setOnMenuItemClickListener(this)
        barcodeMenuItem = toolbar.menu.findItem(R.id.camera)
        keyboardMenuItem = toolbar.menu.findItem(R.id.keyboard)
        autofocusMenuItem = toolbar.menu.findItem(R.id.auto_focus)
        autofocusMenuItem!!.isChecked = true
        torchMenuItem = toolbar.menu.findItem(R.id.flash)
        binding.cameraSourcePreview.statusView.visibility = View.GONE
        val hints = EnumMap<DecodeHintType, Any>(DecodeHintType::class.java)
        hints[DecodeHintType.POSSIBLE_FORMATS] = listOf(BarcodeFormat.QR_CODE)
        hints[DecodeHintType.TRY_HARDER] = true
        binding.cameraSourcePreview.decoderFactory = DefaultDecoderFactory(listOf(BarcodeFormat.QR_CODE), hints, null, 0)
        val eventId = requireArguments().getLong("eventId")
        val fragments = mutableListOf(HistoryFragment.newInstance(eventId), StatisticsFragment.newInstance(eventId))
        binding.viewPager.adapter = HistoryPagerAdapter(this, fragments)
        binding.viewPager.offscreenPageLimit = 2
        TabLayoutMediator(binding.tabLayout, binding.viewPager
        ) { tab: TabLayout.Tab, position: Int ->
            when (position) {
                0 -> tab.text = "История"
                1 -> tab.text = "Статистика"
            }
        }.attach()
        binding.keyboardView.setCodeListener { code: CharSequence -> presenter!!.onScan(code.toString()) }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        interactionListener = context as FragmentInteractionListener
        vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    override fun onDetach() {
        vibrator = null
        interactionListener = null
        super.onDetach()
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.camera -> {
                showCameraMode()
                true
            }
            R.id.keyboard -> {
                showKeyboardMode()
                true
            }
            R.id.flash -> {
                item.isChecked = !item.isChecked
                if (item.isChecked) {
                    binding.cameraSourcePreview.setTorchOn()
                } else {
                    binding.cameraSourcePreview.setTorchOff()
                }
                true
            }
            R.id.auto_focus -> {
                item.isChecked = !item.isChecked
                if (item.isChecked) {
                    enableAutoFocus()
                } else {
                    disableAutofocus()
                }
                true
            }
            R.id.refresh -> {
                presenter!!.loadTickets(requireArguments().getLong("eventId"))
                true
            }
            else -> false
        }
    }

    override fun onResume() {
        super.onResume()
        showCameraMode()
    }

    override fun onPause() {
        super.onPause()
        binding.cameraSourcePreview.setTorchOff()
        binding.cameraSourcePreview.barcodeView.stopDecoding()
        binding.cameraSourcePreview.pause()
        handler!!.removeCallbacksAndMessages(null)
    }

    override fun createPresenter(): ru.gostandup.tickets.ui.control.ControlContract.Presenter {
        val eventId = requireArguments().getLong("eventId", -1)
        return get { parametersOf(eventId) }
    }

    override fun showEventTitle(title: String?) {

    }

    override fun showPassedTicket(ticket: TicketFull) {
        binding.passTime.visibility = View.GONE
        var phone = ticket.phone
        if (phone == null) {
            phone = ""
        }
        binding.phoneTextView.text = phone
        var place = ""
        if (ticket.row != null && ticket.row!!.isNotEmpty()) {
            place = getString(R.string.row, ticket.row)
        }
        if (ticket.column != null && ticket.column!!.isNotEmpty()) {
            if (place.isNotEmpty()) {
                place += ", "
            }
            place += getString(R.string.place, ticket.column)
        }
        binding.place.text = place
        binding.nameTextView.text = ticket.fio
        binding.sector.text = ticket.sector
        binding.barcodeTextView.text = ticket.code
        binding.ticketStatusIndicatorView.setBackgroundColor(Color.GREEN)
        toneG!!.stopTone()
        toneG!!.startTone(ToneGenerator.TONE_PROP_BEEP, 200)
        vibrate(200)
        handler!!.postDelayed({ clearTicketData() }, 3000)
    }

    private fun showCameraMode() {
        barcodeMenuItem?.setIcon(R.drawable.ic_camera_accent_36dp)
        keyboardMenuItem?.setIcon(R.drawable.ic_keyboard_white_24dp)
        binding.barcodeTextView.visibility = View.VISIBLE
        binding.tabLayout.visibility = View.VISIBLE
        binding.viewPager.visibility = View.VISIBLE
        binding.keyboardView.visibility = View.GONE
        startDecodingWithRequestPermission()
    }

    private fun showKeyboardMode() {
        barcodeMenuItem?.setIcon(R.drawable.ic_camera_white_36dp)
        keyboardMenuItem?.setIcon(R.drawable.ic_keyboard_accent_24dp)
        binding.cameraSourcePreview.barcodeView.stopDecoding()
        binding.cameraSourcePreview.pauseAndWait()
        binding.cameraSourcePreview.viewFinder.visibility = View.GONE
        binding.viewPager.visibility = View.GONE
        binding.barcodeTextView.visibility = View.GONE
        binding.tabLayout.visibility = View.GONE
        autofocusMenuItem?.isVisible = false
        torchMenuItem?.isVisible = false
        binding.keyboardView.visibility = View.VISIBLE
        lastScanning = null
    }

    override fun setCheckTicketProgressIndicator(show: Boolean) {
        if (show) {
            vibrate(100)
            binding.checkTicketProgress.visibility = View.VISIBLE
        } else {
            binding.checkTicketProgress.visibility = View.GONE
        }
    }

    override fun showAlreadyPassedError(ticket: TicketFull?) {
        if (context == null) {
            return
        }
        var phone = ticket?.phone
        if (phone == null) {
            phone = ""
        }
        binding.passTime.visibility = View.VISIBLE
        if (ticket?.passTime != null) {
            binding.passTime.text = String.format("Был пробит в %s", DateFormat.getTimeFormat(context).format(Date(ticket.passTime!! * 1000)))
        } else {
            binding.passTime.text = ""
        }
        binding.phoneTextView.text = phone
        var place = ""
        if (ticket?.row != null && ticket.row!!.isNotEmpty()) {
            place = getString(R.string.row, ticket.row)
        }
        if (ticket?.column != null && ticket.column!!.isNotEmpty()) {
            if (place.isNotEmpty()) {
                place += ", "
            }
            place += getString(R.string.place, ticket.column)
        }
        binding.place.text = place
        binding.nameTextView.text = ticket?.fio
        binding.sector.text = ticket?.sector
        binding.barcodeTextView.text = ticket?.code
        binding.ticketStatusIndicatorView.setBackgroundColor(Color.RED)
        toneG!!.stopTone()
        toneG!!.startTone(ToneGenerator.TONE_SUP_INTERCEPT, 1400)
        vibrate(longArrayOf(0, 300, 100, 500))
        handler!!.postDelayed({ clearTicketData() }, 3000)
    }



    override fun showCheckTicketError(error: Throwable) {
        vibrate(200)
        AlertDialog.Builder(requireContext())
                .setTitle("Ошибка")
                .setMessage(error.message)
                .setCancelable(false)
                .setPositiveButton("OK") { _, _  -> clearTicketData() }
                .create()
                .show()
    }

    private fun clearTicketData() {
        binding.ticketStatusIndicatorView.setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        lastScanning = null
    }

    private fun enableAutoFocus() {
        binding.cameraSourcePreview.barcodeView.startAutofocus()
    }

    private fun disableAutofocus() {
        binding.cameraSourcePreview.barcodeView.stopAutofocus()
    }

    override fun setLoadingTicketsIndicator(show: Boolean) {
        val fr = childFragmentManager.findFragmentByTag("progress") as ProgressDialog?
        fr?.dismissAllowingStateLoss()
        if (show) {
            ProgressDialog.show(childFragmentManager)
        }
    }

    override fun showUpdateError() {
        showMessage("Ошибка", "Произошла ошибка обновления!")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode != RC_CAMERA || grantResults.size == 0) {
            return
        }
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            autofocusMenuItem!!.isVisible = true
            val hasFlash = requireContext().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
            torchMenuItem!!.isVisible = hasFlash
            binding.cameraSourcePreview.viewFinder.visibility = View.VISIBLE
            binding.cameraSourcePreview.resume()
            binding.cameraSourcePreview.decodeContinuous(this)
        } else {
            showKeyboardMode()
        }
    }

    private fun startDecodingWithRequestPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            autofocusMenuItem?.isVisible = false
            torchMenuItem?.isVisible = false
            if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.CAMERA)) {
                AlertDialog.Builder(requireActivity())
                        .setMessage("Для распознавния баркодов необходимо дать разрешения на управление камерой устройства.")
                        .setPositiveButton("OK") {_, _ -> requestPermissions(arrayOf(Manifest.permission.CAMERA), RC_CAMERA) }
                        .setCancelable(false)
                        .create()
                        .show()
                return
            }
            requestPermissions(arrayOf(Manifest.permission.CAMERA), RC_CAMERA)
            return
        }
        autofocusMenuItem?.isVisible = true
        val hasFlash = requireContext().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        torchMenuItem?.isVisible = hasFlash
        binding.cameraSourcePreview.viewFinder.visibility = View.VISIBLE
        binding.cameraSourcePreview.resume()
        binding.cameraSourcePreview.decodeContinuous(this)
    }

    fun showMessage(title: String?, message: String?) {
        AlertDialog.Builder(requireContext())
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    override fun showTickedPassedSuccess() {
        binding.ticketStatusIndicatorView.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorSuccess))
        playSuccessTone()
    }

    override fun playSuccessTone() {
        toneG!!.stopTone()
        toneG!!.startTone(ToneGenerator.TONE_CDMA_PIP, 700)
    }

    override fun onDestroy() {
        toneG!!.release()
        super.onDestroy()
    }

    override fun onTorchOn() {}
    override fun onTorchOff() {}
    override fun barcodeResult(result: BarcodeResult) {
        Timber.d("lastScanning: %s", lastScanning)
        if (lastScanning != null) {
            return
        }
        val text = result.text
        lastScanning = text
        binding.barcodeTextView.text = text
        if (text.length != 6
                || !pattern.matcher(text).matches()) {
            toneG!!.startTone(ToneGenerator.TONE_PROP_BEEP, 500)
            vibrate(300)
            handler!!.postDelayed({ lastScanning = null }, 2000)
            return
        }
        presenter!!.onScan(text)
    }

    override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    private fun pauseScanner() {
        binding.cameraSourcePreview.pause()
        binding.cameraSourcePreview.viewFinder.visibility = View.INVISIBLE
    }

    private fun resumeScanner() {
        binding.cameraSourcePreview.resume()
        binding.cameraSourcePreview.viewFinder.visibility = View.VISIBLE
    }

    private fun vibrate(durationMs: Long) {
        if (vibrator == null) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator!!.vibrate(VibrationEffect.createOneShot(durationMs, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator!!.vibrate(durationMs)
        }
    }

    private fun vibrate(pattern: LongArray) {
        if (vibrator == null) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator!!.vibrate(VibrationEffect.createWaveform(pattern, -1))
        } else {
            vibrator!!.vibrate(pattern, -1)
        }
    }

    private inner class HistoryPagerAdapter(fragment: Fragment, private var fragments: List<Fragment>) : FragmentStateAdapter(fragment) {
        override fun createFragment(position: Int): Fragment {
            return fragments[position]
        }

        override fun getItemCount(): Int {
            return fragments.size
        }

        fun update(fragments: List<Fragment>) {
            this.fragments = fragments
            notifyDataSetChanged()
        }

    }

    companion object {
        private const val RC_CAMERA = 492
        private val pattern = Pattern.compile("[0-9AETMK]+")
        @JvmStatic
        fun newInstance(eventId: Long): ControlFargment {
            val args = Bundle()
            args.putLong("eventId", eventId)
            val fragment = ControlFargment()
            fragment.arguments = args
            return fragment
        }
    }
}