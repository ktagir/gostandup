package ru.gostandup.tickets.app

import android.app.Application
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import ru.gostandup.tickets.BuildConfig
import ru.gostandup.tickets.di.allModules
import su.tagir.lib.developersettings.DeveloperSettingsModel
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            androidFileProperties()
            modules(allModules)
        }

        if(BuildConfig.DEBUG){
            get<DeveloperSettingsModel>().apply()
        }
    }
}