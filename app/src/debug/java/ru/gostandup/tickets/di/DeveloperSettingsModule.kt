package ru.gostandup.tickets.di


import hu.supercluster.paperwork.Paperwork
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import ru.gostandup.tickets.DeveloperSettingsModelImpl
import ru.gostandup.tickets.other.MainActivityViewModifier
import su.tagir.lib.developersettings.*
import su.tagir.lib.developersettings.other.ViewModifier

val developerSettingsModule = module {

    val httpLoggingInterceptor = HttpLoggingInterceptor()

    single<List<Interceptor>> { listOf(httpLoggingInterceptor) }

    single<LeakCanaryProxy> { LeakCanaryProxy.Impl(androidApplication()) }

    single<Paperwork> { Paperwork(androidApplication()) }

    single<TinyDancerProxy> { TinyDancerProxy.Impl(androidApplication()) }

    single<DeveloperSettingsModel> {
        DeveloperSettingsModelImpl(
                application = androidApplication(),
                paperwork = get(),
                leakCanaryProxy = get(),
                tinyDancerProxy = get(),
                httpLoggingInterceptor = httpLoggingInterceptor

        )
    }

    single<ViewModifier> { MainActivityViewModifier() }

    factory<DeveloperSettingsContract.Presenter> { DeveloperSettingsPresenter(get()) }

}