package ru.gostandup.tickets

import android.app.Application
import hu.supercluster.paperwork.Paperwork
import okhttp3.logging.HttpLoggingInterceptor
import su.tagir.lib.developersettings.DeveloperSettingsModel
import su.tagir.lib.developersettings.LeakCanaryProxy
import su.tagir.lib.developersettings.TinyDancerProxy

class DeveloperSettingsModelImpl(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        leakCanaryProxy: LeakCanaryProxy,
        tinyDancerProxy: TinyDancerProxy,
        paperwork: Paperwork,
        application: Application
): DeveloperSettingsModel.Impl(httpLoggingInterceptor, leakCanaryProxy, tinyDancerProxy, paperwork, application) {

    override val buildVersionCode: String
        get() = BuildConfig.VERSION_CODE.toString()

    override val buildVersionName: String
        get() = BuildConfig.VERSION_NAME
}