package ru.gostandup.tickets.other

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.drawerlayout.widget.DrawerLayout
import ru.gostandup.tickets.R
import su.tagir.lib.developersettings.other.ViewModifier


class MainActivityViewModifier: ViewModifier {

    override fun <T : View> modify(view: T): T {
        // Basically, what we do here is adding a Developer Setting Fragment to a DrawerLayout!
        val drawerLayout: DrawerLayout =
            view.findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val layoutParams: DrawerLayout.LayoutParams =
            DrawerLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        layoutParams.gravity = Gravity.END
        drawerLayout.addView(
            LayoutInflater.from(view.context)
                .inflate(R.layout.developer_settings_view, drawerLayout, false), layoutParams
        )
        return view
    }
}