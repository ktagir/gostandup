package cook4you.app.manager.ui.other

class NoOpViewModifier : ViewModifier {
    @NonNull
    @Override
    fun <T : View> modify(@NonNull view: T): T {
        // no-op
        return view
    }
}