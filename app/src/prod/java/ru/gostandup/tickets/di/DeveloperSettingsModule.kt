package cook4you.app.manager.di

import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module

val developerSettingsModule = module{

    single<List<Interceptor>> { emptyList() }

    single<DeveloperSettingsModel> {
        DeveloperSettingsModel.NoOp()
    }

    single<ViewModifier> { NoOpViewModifier() }
}