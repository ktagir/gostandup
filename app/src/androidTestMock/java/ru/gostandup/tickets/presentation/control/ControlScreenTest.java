package ru.gostandup.tickets.ui.control;

import android.content.Intent;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import ru.gostandup.tickets.R;
import ru.gostandup.tickets.data.FakeRepository;
import ru.gostandup.tickets.data.entities.Event;
import ru.gostandup.tickets.data.entities.Order;
import ru.gostandup.tickets.data.entities.Seat;
import ru.gostandup.tickets.data.entities.Ticket;
import ru.gostandup.tickets.matchers.MenuIconMatcher;
import ru.gostandup.tickets.matchers.RecyclerViewItemCountAssertion;
import ru.gostandup.tickets.ui.main.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withTagValue;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by ishmukhametov on 21.04.17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ControlScreenTest {
    private static final String EVENT_TITLE = "title";
    private static final String TICKET_1_CODE = "123456";

    private static final String TICKET_GROUPED_CODE_1 = "223456";
    private static final String TICKET_GROUPED_CODE_2 = "243456";

    private static Event EVENT;
    private static List<Ticket> TICKETS;
    private static int TICKET_1_ROW = 1;

    private static int TICKET_2_ROW = 2;

    private static final Seat SEAT_1 = Seat.create(1, TICKET_1_ROW, "", "", Seat.SOLD);
    private static final Seat SEAT_2 = Seat.create(1, TICKET_2_ROW, "", "", Seat.SOLD);
    private static final Seat SEAT_3 = Seat.create(1, 3, "", "", Seat.SOLD);
    private static final Seat SEAT_4 = Seat.create(1, 4, "", "", Seat.SOLD);
    private static final Seat SEAT_5 = Seat.create(1, 5, "", "", Seat.SOLD);
    private static final Seat SEAT_6 = Seat.create(1, 6, "", "", Seat.SOLD);
    private static final Ticket TICKET_GROUPED_1 = Ticket.create(3, TICKET_GROUPED_CODE_1, Arrays.asList(SEAT_3, SEAT_4), Order.create("", "", 0));
    private static final Ticket TICKET_GROUPED_2 = Ticket.create(4, TICKET_GROUPED_CODE_2, Arrays.asList(SEAT_5, SEAT_6), Order.create("", "", 0));


    @Before
    public void setUp(){
        EVENT = Event.builder()
                .setCity("city1")
                .setDate("date")
                .setId(123)
                .setPlace("place")
                .setTime("time")
                .setTitle(EVENT_TITLE)
                .build();

        Ticket t1 = Ticket.create(1, TICKET_1_CODE, Collections.singletonList(SEAT_1), Order.create("", "", 0));
        Ticket t2 = Ticket.create(2, "234567", Collections.singletonList(SEAT_2), Order.create("", "", 0));

        TICKETS = Arrays.asList(t1, t2, TICKET_GROUPED_1, TICKET_GROUPED_2);
    }



    private Matcher<View> withItemText(final String itemText) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View item) {
                return allOf(
                        isDescendantOfA(isAssignableFrom(RecyclerView.class)),
                        withText(itemText)).matches(item);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is isDescendantOfA LV with text " + itemText);
            }
        };
    }


    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class, true, false);


    @Test
    public void showInitialInputModeComponents() {
        loadTickets();

        onView(withId(R.id.viewPager)).check(matches(isDisplayed()));
        onView(withId(R.id.barcodeTextView)).check(matches(isDisplayed()));
        onView(withId(R.id.cameraSourcePreview)).check(matches(isDisplayed()));
        onView(withId(R.id.tabLayout)).check(matches(isDisplayed()));

        onView(withId(R.id.keyboardView)).check(matches(not(isDisplayed())));

        onView(withId(R.id.camera)).check(matches(new MenuIconMatcher(R.drawable.ic_camera_accent_36dp)));
        onView(withId(R.id.keyboard)).check(matches(new MenuIconMatcher(R.drawable.ic_keyboard_white_24dp)));

    }

    @Test
    public void changeMode_DisplayKeyboardMode() {
        loadTickets();

        onView(withId(R.id.keyboard)).perform(click());

        onView(withId(R.id.viewPager)).check(matches(not(isDisplayed())));
        onView(withId(R.id.barcodeTextView)).check(matches(not(isDisplayed())));
        onView(withId(R.id.tabLayout)).check(matches(not(isDisplayed())));

        onView(withId(R.id.keyboardView)).check(matches(isDisplayed()));

        onView(withId(R.id.camera)).check(matches(new MenuIconMatcher(R.drawable.ic_camera_white_36dp)));
        onView(withId(R.id.keyboard)).check(matches(new MenuIconMatcher(R.drawable.ic_keyboard_accent_24dp)));
    }


    @Test
    public void controlTicket_HideKeyboard() {
        loadTickets();

        onView(withId(R.id.keyboard)).perform(click());

        controlTicketUseKeyboard(TICKET_1_CODE);


        onView(withId(R.id.keyboardView)).check(matches(not(isDisplayed())));

        onView(withId(R.id.camera)).check(matches(new MenuIconMatcher(R.drawable.ic_camera_accent_36dp)));
        onView(withId(R.id.keyboard)).check(matches(new MenuIconMatcher(R.drawable.ic_keyboard_white_24dp)));
    }

    private String getText(int stringId, Object ...args) {
        return mainActivityTestRule.getActivity().getResources().getString(stringId, args);
    }

    @Test
    public void loadTickets_DisplayedInUi() throws Exception {
        loadTickets();


        onView(withItemText(getText(R.string.row, TICKET_1_ROW))).check(matches(isDisplayed()));
        onView(withItemText(getText(R.string.row, TICKET_2_ROW))).check(matches(isDisplayed()));
    }


    @Test
    public void controlGroupTicket_DisplayedExpandGroup() throws Exception {
        loadTickets();

        controlTicketUseKeyboard(TICKET_GROUPED_CODE_1);

        showAllSeatInTicket(TICKET_GROUPED_CODE_1);

        controlTicketUseKeyboard(TICKET_GROUPED_CODE_2);

        showAllControlledSeat(TICKET_GROUPED_CODE_1);
        showAllSeatInTicket(TICKET_GROUPED_CODE_2);

        controlTicketUseKeyboard(TICKET_1_CODE);
        showAllControlledSeat(TICKET_GROUPED_CODE_2);
    }

    @Test
    public void controlTicket_MoveToFirstPosition() throws Exception {
        loadTickets();

        onView(withTagValue(is("ticketsRecyclerView"))).check(new RecyclerViewItemCountAssertion(TICKETS.size()));

        controlTicketUseKeyboard(TICKET_GROUPED_CODE_1);
        showTicketOnFirstPosition(TICKET_GROUPED_CODE_1);


        int size = TICKETS.size() + TICKET_GROUPED_1.seats().size() - 1;
        onView(withTagValue(is("ticketsRecyclerView"))).check(new RecyclerViewItemCountAssertion(size));

        controlTicketUseKeyboard(TICKET_GROUPED_CODE_2);
        showTicketOnFirstPosition(TICKET_GROUPED_CODE_2);

        size = TICKETS.size() + TICKET_GROUPED_2.seats().size() - 1;
        onView(withTagValue(is("ticketsRecyclerView"))).check(new RecyclerViewItemCountAssertion(size));


        int size1 = TICKET_GROUPED_2.seats().size();

        for (int i = 0; i < size1; i++) {
            onView(withTagValue(is("ticketsRecyclerView"))).perform(actionOnItemAtPosition(i, click()));
        }

        controlTicketUseKeyboard(TICKET_GROUPED_CODE_1);
        showTicketOnFirstPosition(TICKET_GROUPED_CODE_1);

        size = TICKETS.size() + (TICKET_GROUPED_1.seats().size() - 1) + (size1 - 1);
        onView(withTagValue(is("ticketsRecyclerView"))).check(new RecyclerViewItemCountAssertion(size));
    }

    private void showOnlyHeadSeat(String code){
        int position = Observable.fromIterable(TICKETS)
                .takeWhile(ticket -> !ticket.code().equals(code))
                .count()
                .map(count -> count == TICKETS.size() ? -1 : count)
                .blockingGet()
                .intValue();


        List<Seat> seats = new ArrayList<>(TICKETS.get(position).seats());

        Seat head = seats.remove(0);

        onView(withTagValue(is("ticketsRecyclerView")))
                .check(matches(atLeastOne(hasDescendant(withText(getText(R.string.row, head.row()))))));

        for (Seat seat : seats) {
            onView(withTagValue(is("ticketsRecyclerView")))
                    .check(matches(allListHas(not(hasDescendant(withText(getText(R.string.row, seat.row())))))));
        }
    }

    private void showAllControlledSeat(String code){
        int position = Observable.fromIterable(TICKETS)
                .takeWhile(ticket -> !ticket.code().equals(code))
                .count()
                .map(count -> count == TICKETS.size() ? -1 : count)
                .blockingGet()
                .intValue();


        List<Seat> seats = new ArrayList<>(TICKETS.get(position).seats());

        for (Seat seat : seats) {
            if (seat.status() == Seat.SOLD && !seat.equals(seats.get(0))){
                onView(withTagValue(is("ticketsRecyclerView")))
                        .check(matches(allListHas(not(hasDescendant(withText(getText(R.string.row, seat.row())))))));
            }else {
                onView(withTagValue(is("ticketsRecyclerView")))
                        .check(matches(atLeastOne(hasDescendant(withText(getText(R.string.row, seat.row()))))));
            }
        }
    }


    private void showTicketOnFirstPosition(String code){
        int position = Observable.fromIterable(TICKETS)
                .takeWhile(ticket -> !ticket.code().equals(code))
                .count()
                .map(count -> count == TICKETS.size() ? -1 : count)
                .blockingGet()
                .intValue();

        List<Seat> seats = TICKETS.get(position).seats();

        int index = 0;

        for (Seat seat : seats) {
            onView(withTagValue(is("ticketsRecyclerView")))
                    .perform(RecyclerViewActions.scrollToPosition(index))
                    .check(matches(atPosition(index, hasDescendant(withText(getText(R.string.row, seat.row()))))));
            index++;
        }
    }

    private void showAllSeatInTicket(String code){
        int position = Observable.fromIterable(TICKETS)
                .takeWhile(ticket -> !ticket.code().equals(code))
                .count()
                .map(count -> count == TICKETS.size() ? -1 : count)
                .blockingGet()
                .intValue();


        List<Seat> seats = TICKETS.get(position).seats();
        int index = 0;

        for (Seat seat : seats) {
            onView(withTagValue(is("ticketsRecyclerView")))
                    .perform(RecyclerViewActions.scrollToPosition(index))
                    .check(matches(atPosition(index, hasDescendant(withText(getText(R.string.row, seat.row()))))));
            index++;
        }
    }


    public static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    return false;
                }
                return itemMatcher.matches(viewHolder.itemView);
            }
        };
    }

    public static Matcher<View> atLeastOne( @NonNull final Matcher<View> itemMatcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item in list : ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                int itemCount = view.getAdapter().getItemCount();
                for (int i = 0; i < itemCount; i++) {
                    RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(i);
                    if (viewHolder == null) {
                        return false;
                    }
                    if (itemMatcher.matches(viewHolder.itemView)) return true;
                }
                return false;
            }
        };
    }

    public static Matcher<View> allListHas( @NonNull final Matcher<View> itemMatcher) {
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item in list : ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                int itemCount = view.getAdapter().getItemCount();
                boolean b = true;
                for (int i = 0; i < itemCount; i++) {
                    RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(i);
                    if (viewHolder == null) {
                        return false;
                    }
                    b = b && itemMatcher.matches(viewHolder.itemView);
                }
                return b;
            }
        };
    }

    private void controlTicketUseKeyboard(String code){
        onView(withId(R.id.keyboard)).perform(click());

        onView(withId(R.id.clearButton)).perform(click());

        for (int i = 0; i < code.length(); i++) {
            Matcher<View> viewMatcher = allOf(instanceOf(Button.class), withText(code.charAt(i) + ""));
            onView(viewMatcher).perform(click());
        }
    }

    private void loadTickets() {
        startActivityWithTickets(EVENT, TICKETS);
        onView(withText(EVENT_TITLE)).perform(click());
    }

    private void startActivityWithTickets(Event event, List<Ticket> tickets) {
        FakeRepository.getInstance().saveEvents(Collections.singletonList(event)).blockingGet();
        FakeRepository.getInstance().saveTickets(tickets, event.id()).blockingGet();

        Intent startIntent = new Intent();
        mainActivityTestRule.launchActivity(startIntent);
    }

    @Test
    public void controlToolbarTitle_DisplayedInUi() {
        loadTickets();
        onView(withText(EVENT.city())).check(matches(isDisplayed()));
    }
}