package su.tagir.lib.sqlite

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteOpenHelper
import org.sqlite.database.sqlite.SQLiteDatabase
import org.sqlite.database.sqlite.SQLiteOpenHelper


class SqliteOpenHelper(configuration: SupportSQLiteOpenHelper.Configuration) : SupportSQLiteOpenHelper {

    companion object{
        init {
            System.loadLibrary("sqliteX")
        }
    }

    private val delegate = OpenHelper(configuration, arrayOfNulls(1))

    override fun close() = delegate.close()

    override fun getDatabaseName(): String? = delegate.databaseName

    override fun setWriteAheadLoggingEnabled(enabled: Boolean) =
        delegate.setWriteAheadLoggingEnabled(
            enabled
        )

    override fun getWritableDatabase(): SupportSQLiteDatabase = delegate.getWritableSupportDatabase()

    override fun getReadableDatabase(): SupportSQLiteDatabase = delegate.getWritableSupportDatabase()

    class OpenHelper(
        private val configuration: SupportSQLiteOpenHelper.Configuration,
        private val dbRef: Array<SqliteDatabase?>
    ) : SQLiteOpenHelper(
        configuration.context,
        configuration.context.getDatabasePath(configuration.name).path,
        null,
        configuration.callback.version
    ) {

        @Volatile
        private var migrated = false

        @Synchronized
        fun getWritableSupportDatabase(): SupportSQLiteDatabase {
            migrated = false
            val db = super.getWritableDatabase()
            if (migrated) {
                close()
                return getWritableSupportDatabase()
            }
            return getWrappedDb(db)
        }

        @Synchronized
        fun getWrappedDb(db: SQLiteDatabase): SqliteDatabase {
            var wrappedDb: SqliteDatabase? = dbRef[0]
            if (wrappedDb == null) {
                wrappedDb = SqliteDatabase(db)
                dbRef[0] = wrappedDb
            }
            return dbRef[0]!!
        }

        override fun onConfigure(db: SQLiteDatabase) {
            configuration.callback.onConfigure(getWrappedDb(db))
        }

        override fun onCreate(sqliteDatabase: SQLiteDatabase) {
            configuration.callback.onCreate(getWrappedDb(sqliteDatabase))
        }

        override fun onUpgrade(sqliteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            migrated = true
            configuration.callback.onUpgrade(getWrappedDb(sqliteDatabase), oldVersion, newVersion)
        }

        override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            migrated = true
            configuration.callback.onDowngrade(getWrappedDb(db), oldVersion, newVersion)
        }

        override fun onOpen(db: SQLiteDatabase?) {
            if (!migrated) {
                // from Google: "if we've migrated, we'll re-open the db so we  should not call the callback."
                configuration.callback.onOpen(getWrappedDb(db!!))
            }
        }

        /**
         * {@inheritDoc}
         */
        @Synchronized
        override fun close() {
            super.close()
            dbRef[0] = null
        }

    }
}