package su.tagir.lib.sqlite

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteTransactionListener
import android.os.CancellationSignal
import android.text.TextUtils
import android.util.Pair
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.sqlite.db.SupportSQLiteStatement
import org.sqlite.database.sqlite.SQLiteCursor
import org.sqlite.database.sqlite.SQLiteDatabase
import java.util.*


class SqliteDatabase(private val sqlDb: SQLiteDatabase) : SupportSQLiteDatabase{

    companion object {
        private val CONFLICT_VALUES =
            arrayOf("", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE ")
    }


    override fun close() {
        sqlDb.close()
    }

    override fun compileStatement(sql: String?): SupportSQLiteStatement = SqliteStatement(
        sqlDb.compileStatement(
            sql
        )
    )

    override fun beginTransaction() = sqlDb.beginTransaction()

    override fun beginTransactionNonExclusive() = sqlDb.beginTransactionNonExclusive()

    override fun beginTransactionWithListener(transactionListener: SQLiteTransactionListener) =
        sqlDb.beginTransactionWithListener(object :
            org.sqlite.database.sqlite.SQLiteTransactionListener {
            override fun onBegin() {
                transactionListener.onBegin()
            }

            override fun onCommit() {
                transactionListener.onCommit()
            }

            override fun onRollback() {
                transactionListener.onRollback()
            }

        })

    override fun beginTransactionWithListenerNonExclusive(transactionListener: SQLiteTransactionListener) =
        sqlDb.beginTransactionWithListenerNonExclusive(object :
            org.sqlite.database.sqlite.SQLiteTransactionListener {
            override fun onBegin() {
                transactionListener.onBegin()
            }

            override fun onCommit() {
                transactionListener.onCommit()
            }

            override fun onRollback() {
                transactionListener.onRollback()
            }

        })

    override fun endTransaction() = sqlDb.endTransaction()

    override fun setTransactionSuccessful() = sqlDb.setTransactionSuccessful()

    override fun inTransaction(): Boolean = sqlDb.inTransaction()

    override fun isDbLockedByCurrentThread(): Boolean = sqlDb.isDbLockedByCurrentThread

    override fun yieldIfContendedSafely(): Boolean = sqlDb.yieldIfContendedSafely()

    override fun yieldIfContendedSafely(sleepAfterYieldDelay: Long): Boolean = sqlDb.yieldIfContendedSafely()

    override fun getVersion(): Int = sqlDb.version

    override fun setVersion(version: Int) { sqlDb.version = version }

    override fun getMaximumSize(): Long = sqlDb.maximumSize

    override fun setMaximumSize(numBytes: Long): Long = sqlDb.setMaximumSize(numBytes)

    override fun getPageSize(): Long = sqlDb.pageSize

    override fun setPageSize(numBytes: Long) {
        sqlDb.pageSize = numBytes
    }

    override fun query(query: String): Cursor = query(SimpleSQLiteQuery(query))

    override fun query(query: String, bindArgs: Array<out Any>?): Cursor = query(
        SimpleSQLiteQuery(
            query,
            bindArgs
        )
    )

    override fun query(query: SupportSQLiteQuery): Cursor = query(query, null)

    override fun query(
        supportQuery: SupportSQLiteQuery,
        cancellationSignal: CancellationSignal?
    ): Cursor {
        val hack = BindingsRecorder()

        supportQuery.bindTo(hack)

        return sqlDb.rawQueryWithFactory(
            { _, masterQuery, editTable, query ->
                supportQuery.bindTo(SqliteProgram(query))
                SQLiteCursor(masterQuery, editTable, query)
            }, supportQuery.sql, hack.getBindings(), null
        )
    }

    override fun insert(table: String, conflictAlgorithm: Int, values: ContentValues?): Long =
        sqlDb.insertWithOnConflict(table, null, values, conflictAlgorithm)

    override fun delete(table: String, whereClause: String?, whereArgs: Array<out Any>?): Int {
        val query = ("DELETE FROM " + table
                + if (TextUtils.isEmpty(whereClause)) "" else " WHERE $whereClause")
        val statement = compileStatement(query)

        return try {
            SimpleSQLiteQuery.bind(statement, whereArgs)
            statement.executeUpdateDelete()
        } finally {
            try {
                statement.close()
            } catch (e: Exception) {
                throw RuntimeException("Exception attempting to close statement", e)
            }
        }
    }

    override fun update(
        table: String?,
        conflictAlgorithm: Int,
        values: ContentValues?,
        whereClause: String?,
        whereArgs: Array<out Any>?
    ): Int {
        require(!(values == null || values.size() == 0)) { "Empty values" }
        val sql = StringBuilder(120)
        sql.append("UPDATE ")
        sql.append(CONFLICT_VALUES[conflictAlgorithm])
        sql.append(table)
        sql.append(" SET ")

        // move all bind args to one array

        // move all bind args to one array
        val setValuesSize = values.size()
        val bindArgsSize =
            if (whereArgs == null) setValuesSize else setValuesSize + whereArgs.size
        val bindArgs = arrayOfNulls<Any>(bindArgsSize)
        var i = 0
        for (colName in values.keySet()) {
            sql.append(if (i > 0) "," else "")
            sql.append(colName)
            bindArgs[i++] = values[colName]
            sql.append("=?")
        }
        if (whereArgs != null) {
            i = setValuesSize
            while (i < bindArgsSize) {
                bindArgs[i] = whereArgs[i - setValuesSize]
                i++
            }
        }
        if (!TextUtils.isEmpty(whereClause)) {
            sql.append(" WHERE ")
            sql.append(whereClause)
        }
        val statement = compileStatement(sql.toString())

        return try {
            SimpleSQLiteQuery.bind(statement, bindArgs)
            statement.executeUpdateDelete()
        } finally {
            try {
                statement.close()
            } catch (e: java.lang.Exception) {
                throw java.lang.RuntimeException("Exception attempting to close statement", e)
            }
        }
    }

    override fun execSQL(sql: String) = sqlDb.execSQL(sql)

    override fun execSQL(sql: String, bindArgs: Array<out Any>?) = sqlDb.execSQL(sql, bindArgs)

    override fun isReadOnly(): Boolean = sqlDb.isReadOnly

    override fun isOpen(): Boolean = sqlDb.isOpen

    override fun needUpgrade(newVersion: Int): Boolean = sqlDb.needUpgrade(newVersion)

    override fun getPath(): String = sqlDb.path

    override fun setLocale(locale: Locale?) = sqlDb.setLocale(locale)

    override fun setMaxSqlCacheSize(cacheSize: Int) = sqlDb.setMaxSqlCacheSize(cacheSize)

    override fun setForeignKeyConstraintsEnabled(enable: Boolean) = sqlDb.setForeignKeyConstraintsEnabled(enable)

    override fun enableWriteAheadLogging(): Boolean = sqlDb.enableWriteAheadLogging()

    override fun disableWriteAheadLogging() = sqlDb.disableWriteAheadLogging()

    override fun isWriteAheadLoggingEnabled(): Boolean = sqlDb.isWriteAheadLoggingEnabled

    override fun getAttachedDbs(): MutableList<Pair<String, String>> = sqlDb.attachedDbs

    override fun isDatabaseIntegrityOk(): Boolean = sqlDb.isDatabaseIntegrityOk

}