package su.tagir.lib.sqlite

import androidx.sqlite.db.SupportSQLiteOpenHelper

class SqliteOpenHelperFactory: SupportSQLiteOpenHelper.Factory {


    override fun create(configuration: SupportSQLiteOpenHelper.Configuration): SupportSQLiteOpenHelper {
        return SqliteOpenHelper(configuration)
    }
}