package su.tagir.lib.sqlite

import androidx.sqlite.db.SupportSQLiteStatement
import org.sqlite.database.sqlite.SQLiteStatement

class SqliteStatement(private val statement: SQLiteStatement): SqliteProgram(statement), SupportSQLiteStatement {

    override fun execute() = statement.execute()

    override fun executeUpdateDelete(): Int = statement.executeUpdateDelete()

    override fun executeInsert(): Long = statement.executeInsert()

    override fun simpleQueryForLong(): Long = statement.simpleQueryForLong()

    override fun simpleQueryForString(): String = statement.simpleQueryForString()
}