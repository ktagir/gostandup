package com.codemonkeylabs.fpslibrary.ui;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Service;
import android.graphics.PixelFormat;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.codemonkeylabs.fpslibrary.Calculation;
import com.codemonkeylabs.fpslibrary.FPSConfig;

import java.util.AbstractMap;
import java.util.List;

import su.tagir.lib.developersettings.R;

public class TinyCoach
{
    private final FPSConfig fpsConfig;
    private final TextView meterView;
    private final WindowManager windowManager;

    // detect double tap so we can hide tinyDancer
    private final GestureDetector.SimpleOnGestureListener simpleOnGestureListener = new GestureDetector.SimpleOnGestureListener(){
        @Override
        public boolean onDoubleTap(MotionEvent e)
        {
            // hide but don't remove view
            hide(false);
            return super.onDoubleTap(e);
        }
    };

    @SuppressLint({"SetTextI18n", "InflateParams"})
    public TinyCoach(Application context, FPSConfig config) {

        fpsConfig = config;

        //create meter view
        meterView = (TextView) LayoutInflater.from(context).inflate(R.layout.meter_view, null);

        //set initial fps value....might change...
        meterView.setText((int) fpsConfig.refreshRate + "");

        // grab window manager and add view to the window
        windowManager = (WindowManager) meterView.getContext().getSystemService(Service.WINDOW_SERVICE);

        int minWidth = meterView.getLineHeight()
                + meterView.getTotalPaddingTop()
                + meterView.getTotalPaddingBottom()
                + (int) meterView.getPaint().getFontMetrics().bottom;
        meterView.setMinWidth(minWidth);

        addViewToWindow(meterView);
    }

    private void addViewToWindow(View view) {

        int permissionFlag = PermissionCompat.getFlag();

        WindowManager.LayoutParams paramsF = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                permissionFlag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        // configure starting coordinates
        if (fpsConfig.xOrYSpecified) {
            paramsF.x = fpsConfig.startingXPosition;
            paramsF.y = fpsConfig.startingYPosition;
            paramsF.gravity = FPSConfig.DEFAULT_GRAVITY;
        } else if (fpsConfig.gravitySpecified) {
            paramsF.x = 0;
            paramsF.y = 0;
            paramsF.gravity = fpsConfig.startingGravity;
        } else {
            paramsF.gravity = FPSConfig.DEFAULT_GRAVITY;
            paramsF.x = fpsConfig.startingXPosition;
            paramsF.y = fpsConfig.startingYPosition;
        }

        // add view to the window
        windowManager.addView(view, paramsF);

        // create gesture detector to listen for double taps
        GestureDetector gestureDetector = new GestureDetector(view.getContext(), simpleOnGestureListener);

        // attach touch listener
        view.setOnTouchListener(new DancerTouchListener(paramsF, windowManager, gestureDetector));

        // disable haptic feedback
        view.setHapticFeedbackEnabled(false);

        // show the meter
        show();
    }

    @SuppressLint("SetTextI18n")
    public void showData(FPSConfig fpsConfig, List<Long> dataSet) {

        List<Integer> droppedSet = Calculation.getDroppedSet(fpsConfig, dataSet);
        AbstractMap.SimpleEntry<Calculation.Metric, Long> answer = Calculation.calculateMetric(fpsConfig, dataSet, droppedSet);

        if (answer.getKey() == Calculation.Metric.BAD) {
            meterView.setBackgroundResource(R.drawable.fpsmeterring_bad);
        } else if (answer.getKey() == Calculation.Metric.MEDIUM) {
            meterView.setBackgroundResource(R.drawable.fpsmeterring_medium);
        } else {
            meterView.setBackgroundResource(R.drawable.fpsmeterring_good);
        }

        meterView.setText(answer.getValue() + "");
    }

    @SuppressLint("ClickableViewAccessibility")
    public void destroy() {
        meterView.setOnTouchListener(null);
        hide(true);
    }

    public void show() {
        meterView.setAlpha(0f);
        meterView.setVisibility(View.VISIBLE);
        int longAnimationDuration = 700;
        meterView.animate()
                .alpha(1f)
                .setDuration(longAnimationDuration)
                .setListener(null);
    }

    public void hide (final boolean remove) {
        int shortAnimationDuration = 200;
        meterView.animate()
                .alpha(0f)
                .setDuration(shortAnimationDuration)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {}

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        meterView.setVisibility(View.GONE);
                        if (remove) {
                            windowManager.removeView(meterView);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
        });

    }
}
