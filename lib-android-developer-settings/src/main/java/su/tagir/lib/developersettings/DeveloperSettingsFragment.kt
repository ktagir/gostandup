package su.tagir.lib.developersettings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.FileProvider
import by.kirich1409.viewbindingdelegate.viewBinding
import com.github.pedrovgs.lynx.LynxActivity
import com.github.pedrovgs.lynx.LynxConfig
import com.jakewharton.processphoenix.ProcessPhoenix
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.android.get
import su.tagir.lib.android.mvp.MvpFragment
import su.tagir.lib.developersettings.adapters.DeveloperSettingsSpinnerAdapter
import su.tagir.lib.developersettings.databinding.FragmentDeveloperSettingsBinding
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*


class DeveloperSettingsFragment :
    MvpFragment<DeveloperSettingsContract.View, DeveloperSettingsContract.Presenter>(
        R.layout.fragment_developer_settings
    ), DeveloperSettingsContract.View {

    private val binding: FragmentDeveloperSettingsBinding by viewBinding()

    override fun createPresenter(): DeveloperSettingsContract.Presenter = get()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.developerSettingsHttpLoggingLevelSpinner.adapter =
            DeveloperSettingsSpinnerAdapter<HttpLoggingLevel>(requireActivity().layoutInflater)
                .setSelectionOptions(HttpLoggingLevel.allValues())

        binding.developerSettingsLeakCanarySwitch.setOnCheckedChangeListener { _, isChecked ->
            presenter.changeLeakCanaryState(isChecked)
        }
        binding.developerSettingsStethoSwitch.setOnCheckedChangeListener { _, isChecked ->
            presenter.changFlipperState(isChecked)
        }
        binding.developerSettingsTinyDancerSwitch.setOnCheckedChangeListener { _, isChecked ->
            presenter.changeTinyDanceryState(isChecked)
        }
        binding.developerSettingsHttpLoggingLevelSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    presenter.changeHttpLoggingLevel(HttpLoggingInterceptor.Level.values()[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

            }

        binding.developerSettingsRestartAppButton.setOnClickListener {
            ProcessPhoenix.triggerRebirth(activity, Intent(activity, activity!!::class.java))
        }
        binding.bShowLog.setOnClickListener {
            val lynxConfig = LynxConfig()
            lynxConfig.setMaxNumberOfTracesToShow(4000).filter = "okhttp"

            val lynxActivityIntent = LynxActivity.getIntent(activity, lynxConfig)
            startActivity(lynxActivityIntent)
        }

        binding.developerSettingsExportDb.setOnClickListener {
            val dbPath = copyDbToFilesDir()
            val path: Uri = FileProvider.getUriForFile(requireContext(), getString(R.string.file_provider), dbPath!!)
            val shareIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_STREAM, path)
                type = "*/*"
            }
            startActivity(Intent.createChooser(shareIntent, "Send to"))
        }
        presenter.syncDeveloperSettings()
    }

    override fun changeGitSha(gitSha: String) {
        binding.developerSettingsGitShaTextView.text = gitSha
    }

    override fun changeBuildDate(date: String) {
        binding.developerSettingsBuildDateTextView.text = date
    }

    override fun changeBuildVersionCode(versionCode: String) {
        binding.developerSettingsBuildVersionCodeTextView.text = versionCode
    }

    override fun changeBuildVersionName(versionName: String) {
        binding.developerSettingsBuildVersionNameTextView.text = versionName
    }

    override fun changeFlipperState(enabled: Boolean) {
        binding.developerSettingsStethoSwitch.isChecked = enabled
    }

    override fun changeLeakCanaryState(enabled: Boolean) {
        binding.developerSettingsLeakCanarySwitch.isChecked = enabled
    }

    override fun changeTinyDancerState(enabled: Boolean) {
        binding.developerSettingsTinyDancerSwitch.isChecked = enabled
    }

    override fun changeHttpLoggingLevel(loggingLevel: HttpLoggingInterceptor.Level) {

        var position = 0
        val count: Int = binding.developerSettingsHttpLoggingLevelSpinner.count
        while (position < count) {
            if (loggingLevel == (binding.developerSettingsHttpLoggingLevelSpinner.getItemAtPosition(
                    position
                ) as HttpLoggingLevel).loggingLevel
            ) {
                binding.developerSettingsHttpLoggingLevelSpinner.setSelection(position)
                return
            }
            position++
        }
        throw IllegalStateException("Unknown loggingLevel, looks like a serious bug. Passed loggingLevel = $loggingLevel")
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showAppNeedsToBeRestarted() {
        Toast.makeText(
            context,
            "To apply new settings app needs to be restarted",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun copyDbToFilesDir(): File? {
        val newPath = File(requireContext().filesDir, "databases")
        val oldPath = requireContext().getDatabasePath("database.db")
        try {
            newPath.mkdirs()
            val fin = FileInputStream(oldPath)
            val fos = FileOutputStream("$newPath/database.db")
            val buffer = ByteArray(1024)
            var len1: Int
            while (fin.read(buffer).also { len1 = it } != -1) {
                fos.write(buffer, 0, len1)
            }
            fin.close()
            fos.close()
            val file = File("$newPath/database.db")
            if (file.exists()) return file
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }


    private class HttpLoggingLevel(val loggingLevel: HttpLoggingInterceptor.Level) :
        DeveloperSettingsSpinnerAdapter.SelectionOption {

        override fun title(): String {
            return loggingLevel.toString()
        }

        companion object {

            fun allValues(): List<HttpLoggingLevel> {
                val loggingLevels = HttpLoggingInterceptor.Level.values()
                val values: MutableList<HttpLoggingLevel> = ArrayList(loggingLevels.size)
                for (loggingLevel in loggingLevels) {
                    values.add(HttpLoggingLevel(loggingLevel))
                }
                return values
            }
        }
    }
}