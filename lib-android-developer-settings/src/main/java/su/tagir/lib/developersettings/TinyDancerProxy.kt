package su.tagir.lib.developersettings

import android.app.Application
import com.codemonkeylabs.fpslibrary.TinyDancer

interface TinyDancerProxy {

    fun init()

    class Impl(private val application: Application) : TinyDancerProxy {

        override fun init() {
            TinyDancer.create()
                    .show(application)
        }
    }

    class NoOp: TinyDancerProxy {
        override fun init() {}

    }
}

