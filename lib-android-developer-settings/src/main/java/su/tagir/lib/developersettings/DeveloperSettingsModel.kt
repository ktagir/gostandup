package su.tagir.lib.developersettings

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.preferencesKey
import androidx.datastore.preferences.createDataStore
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.plugins.crashreporter.CrashReporterPlugin
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin
import com.facebook.soloader.SoLoader
import hu.supercluster.paperwork.Paperwork
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean

interface DeveloperSettingsModel {

    fun apply()
    val gitSha: String
    val buildDate: String
    val buildVersionCode: String
    val buildVersionName: String
    fun isFlipperEnabled(): Flow<Boolean>
    suspend fun setFlipperEnabled(isEnabled: Boolean)
    fun isLeakCanaryEnabled(): Flow<Boolean>
    suspend fun setLeakCanaryEnabled(isEnabled: Boolean)
    fun isTinyDancerEnabled(): Flow<Boolean>
    suspend fun setTinyDancerEnabled(isEnabled: Boolean)
    fun httpLoggingLevel(): Flow<HttpLoggingInterceptor.Level>
    suspend fun setHttpLoggingLevel(level: HttpLoggingInterceptor.Level)

    abstract class Impl(
        private val httpLoggingInterceptor: HttpLoggingInterceptor,
        private val leakCanaryProxy: LeakCanaryProxy,
        private val tinyDancerProxy: TinyDancerProxy,
        private val paperwork: Paperwork,
        private val application: Application
    ) : DeveloperSettingsModel {


        companion object {
            private val KEY_IS_FLIPPER_ENABLED = preferencesKey<Boolean>("is_flipper_enabled")
            private val KEY_IS_LEAK_CANARY_ENABLED =
                preferencesKey<Boolean>("is_leak_canary_enabled")
            private val KEY_IS_TINY_DANCER_ENABLED =
                    preferencesKey<Boolean>("is_tiny_dancer_enabled")
            private val KEY_HTTP_LOGGING_LEVEL = preferencesKey<String>("key_http_logging_level")
        }

        private val preferences: DataStore<Preferences> by lazy { application.createDataStore(name = "developer_settings") }

        private val flipperAlreadyEnabled = AtomicBoolean()

        private val leakCanaryAlreadyEnabled = AtomicBoolean()

        private val tinyDancerAlreadyEnabled = AtomicBoolean()

        override fun apply() {
            // Flipper can not be enabled twice.
            if (flipperAlreadyEnabled.compareAndSet(false, true)) {
                if (runBlocking { isFlipperEnabled().first() }) {
                    Timber.d("Flipper enabled")
                    SoLoader.init(application, false)
                    val client = AndroidFlipperClient.getInstance(application)
                    client.addPlugin(CrashReporterPlugin.getInstance())
                    client.addPlugin(DatabasesFlipperPlugin(application))
                    client.start()
                }
            }

            // LeakCanary can not be enabled twice.
            if (leakCanaryAlreadyEnabled.compareAndSet(false, true)) {
                if (runBlocking { isLeakCanaryEnabled().first() }) {
                    leakCanaryProxy.init()
                }
            }

            // LeakCanary can not be enabled twice.
            if (tinyDancerAlreadyEnabled.compareAndSet(false, true)) {
                if (runBlocking { isTinyDancerEnabled().first() }) {
                    tinyDancerProxy.init()
                }
            }

            httpLoggingInterceptor.setLevel(runBlocking { httpLoggingLevel().first() })
        }

        override val gitSha: String
            get() = paperwork.get("gitSha")


        override val buildDate: String
            get() = paperwork.get("buildDate")

        override fun isFlipperEnabled(): Flow<Boolean> =
            preferences.data.map { it[KEY_IS_FLIPPER_ENABLED] ?: false }

        override suspend fun setFlipperEnabled(isEnabled: Boolean) {
            preferences
                .edit { it[KEY_IS_FLIPPER_ENABLED] = isEnabled }
                .apply { apply() }

        }

        override fun isLeakCanaryEnabled(): Flow<Boolean> =
            preferences.data.map { it[KEY_IS_LEAK_CANARY_ENABLED] ?: false }

        override suspend fun setLeakCanaryEnabled(isEnabled: Boolean) {
            preferences
                .edit { it[KEY_IS_LEAK_CANARY_ENABLED] = isEnabled }
                .apply { apply() }
        }

        override fun isTinyDancerEnabled(): Flow<Boolean> =
            preferences.data.map { it[KEY_IS_TINY_DANCER_ENABLED] ?: false }

        override suspend fun setTinyDancerEnabled(isEnabled: Boolean) {
            preferences
                    .edit { it[KEY_IS_TINY_DANCER_ENABLED] = isEnabled }
                    .apply { apply() }
        }

        override fun httpLoggingLevel(): Flow<HttpLoggingInterceptor.Level> = preferences.data.map {
            val name = it[KEY_HTTP_LOGGING_LEVEL] ?: HttpLoggingInterceptor.Level.BASIC.name
            HttpLoggingInterceptor.Level.valueOf(name)
        }

        override suspend fun setHttpLoggingLevel(level: HttpLoggingInterceptor.Level) {
            preferences
                .edit { it[KEY_HTTP_LOGGING_LEVEL] = level.name }
                .apply { apply() }
        }
    }

    class NoOp : DeveloperSettingsModel {
        override fun apply() {
            throw UnsupportedOperationException()
        }

        override val gitSha: String
            get() = throw UnsupportedOperationException()
        override val buildDate: String
            get() = throw UnsupportedOperationException()
        override val buildVersionCode: String
            get() = throw UnsupportedOperationException()
        override val buildVersionName: String
            get() = throw UnsupportedOperationException()

        override fun isFlipperEnabled(): Flow<Boolean> {
            throw UnsupportedOperationException()
        }

        override suspend fun setFlipperEnabled(isEnabled: Boolean) {
            throw UnsupportedOperationException()
        }

        override fun isLeakCanaryEnabled(): Flow<Boolean> {
            throw UnsupportedOperationException()
        }

        override suspend fun setLeakCanaryEnabled(isEnabled: Boolean) {
            throw UnsupportedOperationException()
        }

        override fun isTinyDancerEnabled(): Flow<Boolean> {
            throw UnsupportedOperationException()
        }

        override suspend fun setTinyDancerEnabled(isEnabled: Boolean) {
            throw UnsupportedOperationException()
        }

        override fun httpLoggingLevel(): Flow<HttpLoggingInterceptor.Level> {
            throw UnsupportedOperationException()
        }

        override suspend fun setHttpLoggingLevel(level: HttpLoggingInterceptor.Level) {
            throw UnsupportedOperationException()
        }


    }
}