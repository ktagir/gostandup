package su.tagir.lib.developersettings

import android.app.Application
import leakcanary.AppWatcher

interface LeakCanaryProxy {
    fun init()
    fun watch(`object`: Any, description: String = "")

    class Impl(private val application: Application) : LeakCanaryProxy {

        override fun init() {
            AppWatcher.manualInstall(application)
        }

        override fun watch(`object`: Any, description: String) {
            AppWatcher.objectWatcher.watch(`object`, description)
        }

    }

    class NoOp: LeakCanaryProxy {
        override fun init() {}

        override fun watch(`object`: Any, description: String) {}

    }
}