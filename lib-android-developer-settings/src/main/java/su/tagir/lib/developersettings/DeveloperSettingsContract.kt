package su.tagir.lib.developersettings

import androidx.annotation.UiThread
import okhttp3.logging.HttpLoggingInterceptor
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView

interface DeveloperSettingsContract {

    interface View: MvpView {
        @UiThread
        fun changeGitSha(gitSha: String)

        @UiThread
        fun changeBuildDate(date: String)

        @UiThread
        fun changeBuildVersionCode(versionCode: String)

        @UiThread
        fun changeBuildVersionName(versionName: String)

        @UiThread
        fun changeFlipperState(enabled: Boolean)

        @UiThread
        fun changeLeakCanaryState(enabled: Boolean)

        @UiThread
        fun changeTinyDancerState(enabled: Boolean)

        @UiThread
        fun changeHttpLoggingLevel(loggingLevel: HttpLoggingInterceptor.Level)

        @UiThread
        fun showMessage(message: String)

        @UiThread
        fun showAppNeedsToBeRestarted()
    }

    interface Presenter: MvpPresenter<View> {

        fun syncDeveloperSettings()

        fun changFlipperState(enabled: Boolean)

        fun changeLeakCanaryState(enabled: Boolean)

        fun changeTinyDanceryState(enabled: Boolean)

        fun changeHttpLoggingLevel(loggingLevel: HttpLoggingInterceptor.Level)
    }
}