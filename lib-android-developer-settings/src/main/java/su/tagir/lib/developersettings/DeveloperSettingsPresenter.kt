package su.tagir.lib.developersettings

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.logging.HttpLoggingInterceptor
import su.tagir.lib.mvp.MvpBasePresenter

class DeveloperSettingsPresenter(private val developerSettingsModel: DeveloperSettingsModel) :
    MvpBasePresenter<DeveloperSettingsContract.View>(),
        DeveloperSettingsContract.Presenter {


    override fun syncDeveloperSettings() {
        ifViewAttached({ view ->
            view.changeGitSha(developerSettingsModel.gitSha)
            view.changeBuildDate(developerSettingsModel.buildDate)
            view.changeBuildVersionCode(developerSettingsModel.buildVersionCode)
            view.changeBuildVersionName(developerSettingsModel.buildVersionName)
            launch {
                val isFlipperEnabled = developerSettingsModel.isFlipperEnabled().first()
                val isLeakCanaryEnabled = developerSettingsModel.isLeakCanaryEnabled().first()
                val httpLoggingLevel = developerSettingsModel.httpLoggingLevel().first()
                view.changeFlipperState(isFlipperEnabled)
                view.changeLeakCanaryState(isLeakCanaryEnabled)
                view.changeHttpLoggingLevel(httpLoggingLevel)
            }
        })
    }

    override fun changFlipperState(enabled: Boolean) {
        val flipperWasEnabled: Boolean =
            runBlocking { developerSettingsModel.isFlipperEnabled().first() }
        if (flipperWasEnabled == enabled) {
            return  // no-op
        }
        launch {
            developerSettingsModel.setFlipperEnabled(enabled)
            ifViewAttached({ view ->
                view.showMessage("Flipper was " + booleanToEnabledDisabled(enabled))
//                if (flipperWasEnabled) {
                    view.showAppNeedsToBeRestarted()
//                }
            })
        }
    }

    override fun changeLeakCanaryState(enabled: Boolean) {
        if (runBlocking { developerSettingsModel.isLeakCanaryEnabled().first() } == enabled) {
            return  // no-op
        }
        launch {
            developerSettingsModel.setLeakCanaryEnabled(enabled)
            ifViewAttached({ view ->
                view.showMessage("LeakCanary was " + booleanToEnabledDisabled(enabled))
                view.showAppNeedsToBeRestarted()
            })
        }
    }

    override fun changeTinyDanceryState(enabled: Boolean) {
        if (runBlocking { developerSettingsModel.isTinyDancerEnabled().first() } == enabled) {
            return  // no-op
        }
        launch {
            developerSettingsModel.setTinyDancerEnabled(enabled)
            ifViewAttached({ view ->
                view.showMessage("TinyDancer was " + booleanToEnabledDisabled(enabled))
                view.showAppNeedsToBeRestarted()
            })
        }
    }


    override fun changeHttpLoggingLevel(loggingLevel: HttpLoggingInterceptor.Level) {
        if (runBlocking { developerSettingsModel.httpLoggingLevel().first() } == loggingLevel) {
            return  // no-op
        }
        launch {
            developerSettingsModel.setHttpLoggingLevel(loggingLevel)
            ifViewAttached({ view ->
                view.showMessage("Http logging level was changed to $loggingLevel")
            })
        }
    }

    private fun booleanToEnabledDisabled(enabled: Boolean): String {
        return if (enabled) "enabled" else "disabled"
    }
}