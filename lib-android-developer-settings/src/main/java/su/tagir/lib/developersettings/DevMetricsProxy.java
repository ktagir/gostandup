package su.tagir.lib.developersettings;

public interface DevMetricsProxy {
  /**
   * Applies performance metrics library.
   */
  void apply();
}
