//
//  EventCell.swift
//  ios
//
//  Created by Tagir Kuramshin on 05.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import gostandup_core

class EventCell: UITableViewCell {
    
    let cityTime: UILabel = {
        let label = UILabel()
        label.textColor = colorPrimaryText
        label.font = .systemFont(ofSize: 14)
        return label
    }()
    
    let title: UILabel = {
        let label = UILabel()
        label.textColor = colorPrimaryText
        label.font = .systemFont(ofSize: 14)
        return label
    }()
    
    let club: UILabel = {
        let label = UILabel()
        label.textColor = colorPrimaryText
        label.font = .systemFont(ofSize: 16)
        return label
    }()
    
    let seats: UILabel = {
        let label = UILabel()
        label.textColor = colorPrimaryText
        label.font = .systemFont(ofSize: 24)
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let v = UIView()
        v.backgroundColor = colorSelection
        backgroundColor = .white
        contentView.addSubview(cityTime)
        contentView.addSubview(title)
        contentView.addSubview(club)
        contentView.addSubview(seats)
        
        cityTime.snp.makeConstraints{make in
            make.leading.top.equalToSuperview().inset(UIEdgeInsets(horizontal: 16, vertical: 8))
            make.trailing.equalTo(seats.snp.leading).offset(-16)
        }
        title.snp.makeConstraints{make in
            make.leading.equalToSuperview().offset(8)
            make.top.equalTo(cityTime.snp.bottom).offset(4)
            make.trailing.equalTo(seats.snp.leading).offset(-8)
        }
        club.snp.makeConstraints{make in
            make.leading.equalToSuperview().offset(8)
            make.top.equalTo(title.snp.bottom).offset(4)
            make.trailing.equalTo(seats.snp.leading).offset(-8)
        }
        seats.snp.makeConstraints{make in
            make.trailing.top.bottom.equalToSuperview().inset(UIEdgeInsets(horizontal: 8, vertical: 0))
            make.width.equalTo(96)
            make.height.equalTo(72)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func bind(event: EventItem){
        cityTime.text = (event.event.city ?? "") + " " + (event.event.date ?? "")
        title.text = event.event.title
//        seats.text = "\(event.ticketsCount)"
        club.text = event.event.place
    }
}
