//
//  EventsViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 01.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import gostandup_core
import UIKit

class EventsViewController: MvpTableViewController<EventItem, EventsContractPresenter>, EventsContractView {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(Strings.events)
        tableView.register(cellWithClass: EventCell.self)
        
        tableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow{
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }
    
    override func createPresenter() -> EventsContractPresenter {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        return container.resolve(EventsContractPresenter.self)!
    }
    
    override func cell(tableView: UITableView, indexPath: IndexPath, item: EventItem)-> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withClass: EventCell.self, for: indexPath)
        cell.bind(event: item)
        return cell
    }
}

extension EventsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let event = getItem(indexPath: indexPath){
            presenter.showEvent(event: event.event)
        }
    }
}
