//
//  Strings.swift
//  ios
//
//  Created by Tagir Kuramshin on 31.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation

struct StringBundle{
    
    static var strings: NSDictionary = NSDictionary()
  
    static func loadLocalization(){
        let path = Bundle.main.path(forResource: "Localizable", ofType: "strings", inDirectory: nil, forLocalization: Locale.current.languageCode) ?? ""
            if let tempStrings = NSDictionary.init(contentsOfFile: path){
                self.strings = tempStrings
            }
    }
}

extension String{
    var localized: String{
        return StringBundle.strings[self] as? String ?? self
    }
}

struct Strings {

    static let username = "username".localized
    static let password = "password".localized
    static let go = "go".localized
    static let field_must_not_be_empty = "field_must_not_be_empty".localized
    static let empty_list = "empty_list".localized
    static let error_happen = "error_happen".localized
    static let refresh = "refresh".localized
    static let scanning_unavailable = "scanning_unavailable".localized
    static let app_does_not_have_camera_permission = "app_does_not_have_camera_permission".localized
    static let place_barcode_inside_viewfinder = "place_barcode_inside_viewfinder".localized
    static let passed_at = "passed_at".localized
    static let passed = "passed".localized
    static let row = "row".localized
    static let column = "column".localized
    static let error = "error".localized
    static let update_error = "update_error".localized
    static let without_seats = "without_seats".localized
    static let pull_to_refresh = "pull_to_refresh".localized
    static let events = "events".localized
    static let about_app = "about_app".localized
    static let history = "history".localized
    static let statistics = "statistics".localized
    static let torch = "torch".localized
    static let autofocus = "autofocus".localized
    static let passed_by_device = "passed_by_device".localized
    static let passed_total = "passed_total".localized
    static let sold_total = "sold_total".localized
    static let cancel = "cancel".localized
    static let select_action = "select_action".localized
    static let check_username_and_password = "check_username_and_password".localized
    static let ticket_not_found = "ticket_not_found".localized
}
