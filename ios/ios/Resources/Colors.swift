//
//  Colors.swift
//  ios
//
//  Created by Tagir Kuramshin on 31.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import SwifterSwift

let colorPrimary = UIColor(named: "colorPrimary")!
let colorPrimaryDark = UIColor(named: "colorPrimaryDark")!
let colorPrimaryLight = UIColor(named: "colorPrimaryLight")!
let colorSelection = UIColor(named: "colorSelection")!
let colorAccent = UIColor(named: "colorAccent")!
let colorError = UIColor(named: "colorError")!
let colorSuccess = UIColor(named: "colorSuccess")!
let colorHintText = UIColor(named: "colorHintText")!

let colorPrimaryTextDark = UIColor(named: "colorPrimaryTextDark")!
let colorPrimaryText = UIColor(named: "colorPrimaryText")!
let colorSecondaryText = UIColor(named: "colorSecondaryText")!
let colorKey = UIColor(named: "colorKey")!
