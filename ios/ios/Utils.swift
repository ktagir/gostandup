//
//  Utils.swift
//  ios
//
//  Created by Tagir Kuramshin on 09.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit

var screenHeightSafe: CGFloat{
    let window = UIApplication.shared.windows.filter{w in w.isKeyWindow}.first!
    return window.height - window.safeAreaInsets.top - window.safeAreaInsets.bottom
}

var screenWidth: CGFloat{
    let window = UIApplication.shared.windows.filter{w in w.isKeyWindow}.first!
    switch UIDevice.current.orientation {
    case .landscapeLeft, .landscapeRight:
        return window.width - window.safeAreaInsets.left - window.safeAreaInsets.right
    default:
        return window.width
    }
}

var screenHeight: CGFloat{
    let window = UIApplication.shared.windows.filter{w in w.isKeyWindow}.first
    return window?.height ?? 0
}

var topSafeAreaHeight: CGFloat{
    let window = UIApplication.shared.windows.filter{w in w.isKeyWindow}.first
    return window?.safeAreaInsets.top ?? 0
}

var bottomSafeAreaHeight: CGFloat{
    let window = UIApplication.shared.windows.filter{w in w.isKeyWindow}.first
    return window?.safeAreaInsets.bottom ?? 0
}
