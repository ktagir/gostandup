//
//  AppDelegate.swift
//  ios
//
//  Created by Tagir Kuramshin on 30.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import UIKit
import Swinject
import gostandup_core
import AFNetworking
import AFNetworkActivityLogger
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    let container: Container = {
        let container = Container()
        container.register(Preferences.self, factory: {_ in Preferences()}).inObjectScope(.container)
        container.register(AuthHolder.self, factory: {r in GoStandUpAuthHolder(prefs: r.resolve(Preferences.self)!)}).inObjectScope(.container)
        container.register(RestClient.self, factory: {r in RestClient(authHolder: r.resolve(AuthHolder.self)!,
                                                                      responseSerializer: JsonResponseSerializer(),
                                                                      debug: true)}).inObjectScope(.container)
        container.register(GoStandUpDb.self, factory: {r in Db().instance}).inObjectScope(.container)
        container.register(Repository.self, factory: {r in RepositoryImpl(database: r.resolve(GoStandUpDb.self)!,
                                                                          restClient: r.resolve(RestClient.self)!,
                                                                          preferences: r.resolve(Preferences.self)!)}).inObjectScope(.container)
        
        container.register(Coordinator.self, factory: {_ in Coordinator(rootViewController: NavigationController())}).inObjectScope(.container)
        container.register(Router.self, factory: {r in GoStandupRouter(coordinator: r.resolve(Coordinator.self)!)}).inObjectScope(.container)
        
        container.register(LoginContractPresenter.self, factory: {r in LoginPresenter(repository: r.resolve(Repository.self)!, router: r.resolve(Router.self)!)})
            .inObjectScope(.weak)
        
        container.register(EventsContractPresenter.self, factory: {r in EventsPresenter(repository: r.resolve(Repository.self)!, router: r.resolve(Router.self)!)})
            .inObjectScope(.weak)
        
        container.register(ControlContractPresenter.self,
            factory: {r, eventId in ControlPresenter(eventId: eventId, repository: r.resolve(Repository.self)!)})
            .inObjectScope(.weak)
        
        container.register(HistoryContractPresenter.self,
                           factory: {r, eventId in HistoryPresenter(eventId: eventId, repository: r.resolve(Repository.self)!)})
        
        container.register(StatisticsContractPresenter.self,
                           factory: {r, eventId in StatisticsPresenter(eventId: eventId, repository: r.resolve(Repository.self)!)})
     
        return container
    }()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        FirebaseApp.configure()
        
        Db().defaultDriver()
    
        StringBundle.loadLocalization()
        
        #if DEBUG
        AFNetworkActivityLogger.shared()?.setLogLevel(.AFLoggerLevelDebug)
        AFNetworkActivityLogger.shared()?.startLogging()
        #endif
        
        if #available(iOS 13, *) {
            // do only pure app launch stuff, not interface stuff
        } else {
            
            let drawerController  = DrawerController()
            drawerController.mainViewController = container.resolve(Coordinator.self)?.rootViewController
            drawerController.drawerViewController = NavigationVC()
            
            let window = UIWindow()
            window.backgroundColor = .white
            window.rootViewController = drawerController
            self.window = window
            window.makeKeyAndVisible()
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

