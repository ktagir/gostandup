//
//  DrawerController.swift
//  ios
//
//  Created by Tagir Kuramshin on 10.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import KYDrawerController
import gostandup_core

class DrawerController: KYDrawerController, SessionListener {
    
    
    
    init() {
        super.init(drawerDirection: .left, drawerWidth: 250)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        let authHolder = container.resolve(AuthHolder.self)!
        let router = container.resolve(Router.self)!
        if authHolder.accessToken == nil{
            router.navigateToLogin()
        }else{
            router.navigateToEvents()
        }
        authHolder.subscribeToSessionExpired(sessionListener: self)
    }
    
    func sessionExpired() {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        let router = container.resolve(Router.self)!
        router.navigateToLogin()
    }
}
