//
//  NavigationVC.swift
//  ios
//
//  Created by Tagir Kuramshin on 10.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core
import KYDrawerController

class NavigationVC: UIViewController {
    
    
    private let bg: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    private let headerBg: UIView = {
        return UIView()
    }()
    
    private let userName: UILabel = {
           let label = UILabel()
           label.font = .systemFont(ofSize: 14)
           label.textColor = colorPrimaryTextDark
           return label
       }()
    
    private let email: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.textColor = colorPrimaryTextDark
        return label
    }()
    
    private let logout: UIButton = {
        let btn = UIButton()
        btn.setImageForAllStates(UIImage(named: "exit")!)
        return btn
    }()
    
    private let events: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimary
        label.text = Strings.events
        label.isUserInteractionEnabled = true
        return label
    }()
    
    private let aboutApp: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimary
        label.text = Strings.about_app
        label.isUserInteractionEnabled = true
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(bg)
        self.view.addSubview(headerBg)
        self.view.addSubview(logout)
        self.view.addSubview(email)
        self.view.addSubview(userName)
        self.view.addSubview(events)
        self.view.addSubview(aboutApp)
        bg.snp.makeConstraints{make in
                make.leading.trailing.equalToSuperview()
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
                 make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            }
        headerBg.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.height.equalTo(160)
        }
        userName.snp.makeConstraints{make in
            make.leading.bottom.equalTo(headerBg).inset(UIEdgeInsets(horizontal: 16, vertical: 4))
            make.trailing.equalTo(logout.snp.leading).offset(-16)
            make.height.equalTo(24)
        }
        email.snp.makeConstraints{make in
            make.leading.equalTo(headerBg).inset(UIEdgeInsets(horizontal: 16, vertical: 16))
            make.trailing.equalTo(logout.snp.leading).offset(-16)
            make.bottom.equalTo(userName.snp.top)
            make.height.equalTo(24)
        }
        logout.snp.makeConstraints{make in
            make.size.equalTo(32)
            make.trailing.bottom.equalTo(headerBg).offset(-16)
        }
        events.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(horizontal: 16, vertical: 0))
            make.top.equalTo(headerBg.snp.bottom)
            make.height.equalTo(48)
        }
        aboutApp.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(horizontal: 16, vertical: 0))
            make.top.equalTo(events.snp.bottom)
            make.height.equalTo(48)
        }
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: 250, height: 160)
        gradient.colors = [colorPrimary.cgColor, colorAccent.cgColor, colorPrimaryDark]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = .zero
        headerBg.layer.insertSublayer(gradient, at: 0)
        
        logout.addTarget(self, action: #selector(exit), for: .touchUpInside)
        events.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showEvents)))
        aboutApp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showAboutApp)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        if let user = container.resolve(Repository.self)!.user{
            email.text = user.email
            userName.text = "\(user.firstName) \(user.lastName)"
        }
    }
    
    @objc
    private func exit(){
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        container.resolve(AuthHolder.self)?.refresh()
        closeDrawer()
    }
    
    @objc
    private func showEvents(){
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        container.resolve(Router.self)?.navigateToEvents()
        closeDrawer()
    }
    
    @objc
    private func showAboutApp(){
        let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String) + " - " + (Bundle.main.infoDictionary?["CFBundleVersion"] as! String)
        let alert = UIAlertController(title: Strings.about_app, message: version, defaultActionButtonTitle: "OK", tintColor: colorPrimary)
        present(alert, animated: true, completion: nil)
        closeDrawer()
    }
    
    private func closeDrawer(){
        (parent as? KYDrawerController)?.setDrawerState(.closed, animated: true)
    }
}
