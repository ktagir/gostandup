//
//  NavigationController.swift
//  ios
//
//  Created by Tagir Kuramshin on 10.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.barTintColor = colorPrimary
        navigationBar.tintColor = colorPrimaryTextDark
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colorPrimaryTextDark]
      
      
    }
}
