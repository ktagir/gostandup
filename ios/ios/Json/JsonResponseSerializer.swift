//
//  JsonResponseSerializer.swift
//  ios
//
//  Created by Tagir Kuramshin on 22.12.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import AFNetworking

class JsonResponseSerializer: AFJSONResponseSerializer {
    
    override func responseObject(for response: URLResponse?, data: Data?, error: NSErrorPointer) -> Any? {
        let json = super.responseObject(for: response, data: data, error: error) as AnyObject
        
        if let errorValue = error?.pointee {
            let userInfo = errorValue.userInfo as NSDictionary
            let copy = userInfo.mutableCopy() as! NSMutableDictionary
            
            copy["data"] = json
            error?.pointee = NSError(domain: errorValue.domain, code: errorValue.code, userInfo: copy as? [String : Any])
        }
        
        return json
    }
}
