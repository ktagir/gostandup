//
//  Keyboard.swift
//  ios
//
//  Created by Tagir Kuramshin on 08.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class KeyCell: UICollectionViewCell {
    
    let key: UIButton = {
        let btn = MDCButton()
        btn.backgroundColor = colorKey
        btn.setTitleColorForAllStates(.white)
        btn.setTitleFont(.systemFont(ofSize: 24), for: .normal)
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(key)
        key.snp.makeConstraints{make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class Keyboard: UIView {
    
    let code: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 24)
        return label
    }()
    
    let keys: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    let keyTitles = ["1", "2", "3", "X", "4", "5", "6", "A", "7", "8", "9", "E", "0", "T", "M", "K"]
    
    let clear: UIButton = {
        let btn = MDCButton()
        btn.backgroundColor = .red
        btn.setTitleColorForAllStates(.white)
        btn.setTitleForAllStates("X")
        btn.setTitleFont(.systemFont(ofSize: 24), for: .normal)
        return btn
    }()
    
    private var inputText: String = ""{
        didSet{
            code.text = inputText
        }
    }
    
    var nextCode: ((String) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .lightGray
        addSubview(code)
        addSubview(keys)
        code.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(horizontal: 16, vertical: 16))
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.height.equalTo(72)
        }
        keys.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(horizontal: 8, vertical: 0))
            make.top.equalTo(code.snp.bottom).offset(8)
            make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottom).offset(-8)
        }
        keys.register(cellWithClass: KeyCell.self)
        let cellWidth = (screenWidth - 24) * 0.25
        let layout = keys.collectionViewLayout as! UICollectionViewFlowLayout
       
        layout.itemSize = CGSize.init(width: cellWidth, height: (frame.height - 72 - 32) * 0.25 )
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 4
        keys.dataSource = self
        keys.delegate = self
        
        
        
        keys.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func hide(){
        UIView.animate(withDuration: 0.3, animations: {
            var frame = self.frame
            frame.origin.y = self.superview?.frame.height ?? 900
            self.frame = frame
        }) { _ in
            self.removeFromSuperview()
        }
    }
    
    static func show(superView: UIView, height: CGFloat) -> Keyboard{
        
        
        let frame = CGRect(x: 0, y: superView.frame.size.height, width: screenWidth, height: height)
        let keyboard = Keyboard(frame: frame)
        superView.addSubview(keyboard)
        
        UIView.animate(withDuration: 0.5,
                       animations: {
                        var frame = keyboard.frame
                        frame.origin.y = superView.frame.size.height - height - bottomSafeAreaHeight
                        keyboard.frame = frame
        },
                       completion: nil)
        
        return keyboard
    }
}

extension Keyboard: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keyTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: KeyCell.self, for: indexPath)
        let title = keyTitles[indexPath.row]
        cell.key.setTitleForAllStates(title)
        if(title == "X"){
            cell.key.backgroundColor = .red
            cell.key.addTarget(self, action: #selector(onClearTap), for: .touchUpInside)
        }else{
            cell.key.backgroundColor = colorKey
            cell.key.addTarget(self, action: #selector(onTap), for: .touchUpInside)
        }
        return cell
    }
    
    @objc
    private func onTap(_ sender: UIButton){
        if inputText.count < 6{
           inputText = inputText + (sender.titleForNormal ?? "")
        }
        if inputText.count == 6{
            nextCode?(inputText)
        }
    }
    
    @objc
    func onClearTap(){
        inputText = ""
    }
    
}
