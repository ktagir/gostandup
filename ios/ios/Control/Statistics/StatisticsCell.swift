//
//  StatisticsCell.swift
//  ios
//
//  Created by Tagir Kuramshin on 08.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core

class StatisticsCell: UITableViewCell {
    
    let title: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        return label
    }()
    
    let count: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        contentView.addSubview(count)
        contentView.addSubview(title)
        count.snp.makeConstraints{make in
            make.trailing.top.bottom.equalToSuperview().inset(UIEdgeInsets(horizontal: 24, vertical: 24))
            make.width.equalTo(120)
        }
        title.snp.makeConstraints{make in
            make.leading.top.bottom.equalToSuperview().inset(UIEdgeInsets(horizontal: 24, vertical: 24))
            make.trailing.equalTo(count.snp.leading).offset(-24)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = nil
        count.text = nil
    }
    
    func bind(item: StatisticsItem){
        title.text = item.type.text
        count.text = item.count.string
    }
}

extension StatisticsItemType{
    
    var text: String{
        get{
            if self == StatisticsItemType.passedByDevice{
                return Strings.passed_by_device
            }else if self == StatisticsItemType.passedTotal{
                return Strings.passed_total
            }else if self == StatisticsItemType.soldTotal{
                return Strings.sold_total
            }else{
                return ""
            }
        }
    }
}
