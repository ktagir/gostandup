//
//  StatisticsViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 08.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import gostandup_core

class StatisticsViewController: MvpTableViewController<StatisticsItem, StatisticsContractPresenter>, StatisticsContractView {
    
    private var eventId: Int64!
    
    convenience init(eventId: Int64) {
        self.init()
        self.eventId = eventId
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isUserInteractionEnabled = false
        tableView.register(cellWithClass: StatisticsCell.self)
        tableView.delegate = self
        tableView.alwaysBounceVertical = false

    }
    
    override func createPresenter() -> StatisticsContractPresenter {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        return container.resolve(StatisticsContractPresenter.self, argument: self.eventId!)!
    }
    
    override func cell(tableView: UITableView, indexPath: IndexPath, item: StatisticsItem) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: StatisticsCell.self, for: indexPath)
        cell.bind(item: item)
        return cell
    }
    
    override func showError(t: KotlinThrowable) {}
}

extension StatisticsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
}
