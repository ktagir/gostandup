//
//  HistoryCell.swift
//  ios
//
//  Created by Tagir Kuramshin on 08.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core

class HistoryCell: UITableViewCell {
    
    let place: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        return label
    }()
    
    let passed: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = .white
        label.backgroundColor = colorSuccess
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let v = UIView()
        v.backgroundColor = colorSelection
        selectedBackgroundView = v
        backgroundColor = .white
        contentView.addSubview(place)
        contentView.addSubview(passed)
        passed.snp.makeConstraints{make in
            make.width.equalTo(110)
            make.top.bottom.trailing.equalToSuperview()
            make.height.greaterThanOrEqualTo(76)
            
        }
        place.snp.makeConstraints{make in
            make.leading.top.bottom.equalToSuperview().inset(UIEdgeInsets(horizontal: 16, vertical: 8))
            make.trailing.equalTo(passed.snp.leading).offset(-16)
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.place.text = nil
        self.passed.text = nil
        self.passed.isHidden = true
    }
    
    func bind(ticket: TicketWithSeat) {        
        var placeStr = ticket.sector ?? ""
    
        if let row = ticket.row, !row.isEmpty{
            if (!placeStr.isEmpty) {
                placeStr += ", "
            }
            placeStr += String(format: Strings.row, arguments: [row])
        }
        
        if let column = ticket.column, !column.isEmpty{
            if (!placeStr.isEmpty) {
                placeStr += ", "
            }
            placeStr += String(format: Strings.column, arguments: [column])
        }
        if(placeStr.isEmpty){
            placeStr = Strings.without_seats
        }
        place.text = placeStr
        
        
        self.passed.isHidden = !ticket.isUsed
        let passTime = DateFormatter.localizedString(from: Date(timeIntervalSince1970: ticket.passTime?.doubleValue ?? 0), dateStyle: .none, timeStyle: .medium)
        self.passed.text = String(format: Strings.passed, arguments: [passTime])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
