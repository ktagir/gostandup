//
//  HistoryViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 08.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core

class HistoryViewController: MvpTableViewController<TicketWithSeat, HistoryContractPresenter>, HistoryContractView {
    
    private var eventId: Int64!
    
    convenience init(eventId: Int64) {
        self.init()
        self.eventId = eventId
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(cellWithClass: HistoryCell.self)
        tableView.delegate = self
        
        //        self.setupLongPressGesture()
    }
    
    override func createPresenter() -> HistoryContractPresenter {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        return container.resolve(HistoryContractPresenter.self, argument: self.eventId!)!
    }
    
    override func cell(tableView: UITableView, indexPath: IndexPath, item: TicketWithSeat) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: HistoryCell.self, for: indexPath)
        cell.bind(ticket: item)
        return cell
    }
    
    override func showError(t: KotlinThrowable) {}
    
    func showInfo(ticket: TicketFull) {
        let message = "\(ticket.fio ?? "")\n\(ticket.phone ?? "")"
        let alert = UIAlertController(title: ticket.code ?? "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {_ in
            if let index = self.tableView.indexPathForSelectedRow{
                self.tableView.deselectRow(at: index, animated: true)
            }
        }))
        alert.view.tintColor = colorPrimary
        present(alert, animated: true, completion: nil)
    }
    
    
    //    private func setupLongPressGesture() {
    //        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
    //        longPressGesture.minimumPressDuration = 0.75
    //        self.tableView.addGestureRecognizer(longPressGesture)
    //    }
    //
    //    @objc
    //    private func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
    //        let touchPoint = gestureRecognizer.location(in: self.tableView)
    //        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
    //            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
    //            if let ticket = getItem(indexPath: indexPath){
    //                self.presenter.showTicket(ticket: ticket)
    //            }
    //        }
    //    }
    
}

extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let ticket = getItem(indexPath: indexPath){
            self.presenter.showTicket(ticket: ticket)
        }
    }
    
}
