//
//  ControlViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 05.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import gostandup_core
import MTBBarcodeScanner
import MBProgressHUD
import AudioToolbox
import CarbonKit

class ControlViewController: MvpViewController<ControlContractPresenter>, ControlContractView {
  
    
    
    private let btnCamera: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        btn.setImageForAllStates(UIImage(named: "camera")!.withRenderingMode(.alwaysTemplate))
        return btn
    }()
    
    private let btnKeyboard: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        btn.setImageForAllStates(UIImage(named: "keyboard")!.withRenderingMode(.alwaysTemplate))
        return btn
    }()
    
    private let btnMore: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 32))
        btn.setImageForAllStates(UIImage(named: "more")!)
        return btn
    }()
    
    private let ticketStatusView: UIView = {
        return UIView()
    }()
    
    private let previewView: UIView = {
        let view = UIView()
        return view
    }()
    
    private let sector: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        return label
    }()
    
    private let place: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.textColor = colorPrimaryText
        return label
    }()
    
    private let passTime: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        label.numberOfLines = 0
        label.isHidden = true
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private let name: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textColor = colorPrimaryText
        label.setContentHuggingPriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private let phone: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16)
        label.textColor = colorPrimaryText
        return label
    }()
    
    private let barcode: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14, weight: .bold)
        label.textColor = colorPrimaryText
        label.text = Strings.place_barcode_inside_viewfinder
        return label
    }()
    
    private let progress: UIActivityIndicatorView = {
        let activityIndicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            activityIndicator = UIActivityIndicatorView(style: .large)
        } else {
            activityIndicator = UIActivityIndicatorView()
        }
        activityIndicator.isHidden = true
        activityIndicator.color = colorAccent
        return activityIndicator
    }()
    
    private let viewPager: UIView = {
        let view = UIView()
        return view
    }()
    
    private var scanner: MTBBarcodeScanner?
    
    private var lastScanning: String?
    
    private let pattern = try! NSRegularExpression(pattern: "[0-9AETMK]+")
    
    private var resetLastCodeTimer: Timer?
    private var clearDataTimer: Timer?
    
    private var keyboardView: Keyboard?
    
    private var eventId: Int64!
    
    private var viewControllers: [UIViewController] = []
    private var selectedViewController: UIViewController?
    
    private let keyboardMenuItems = [Strings.refresh]
    
    convenience init(eventId: Int64) {
        self.init()
        self.eventId = eventId
    }
    
    
    override func createPresenter() -> ControlContractPresenter {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        return container.resolve(ControlContractPresenter.self, argument: eventId!)!
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCamera.addTarget(self, action: #selector(cameraMode), for: .touchUpInside)
        btnKeyboard.addTarget(self, action: #selector(keyboardMode), for: .touchUpInside)
        btnMore.addTarget(self, action: #selector(showMore), for: .touchUpInside)
        let camera = UIBarButtonItem(customView: btnCamera)
        let keyboard = UIBarButtonItem(customView: btnKeyboard)
        let more = UIBarButtonItem(customView: btnMore)
        self.navigationItem.setRightBarButtonItems([more, keyboard, camera], animated: false)
        
        
        
        self.view.addSubview(ticketStatusView)
        self.view.addSubview(previewView)
        self.view.addSubview(sector)
        self.view.addSubview(place)
        self.view.addSubview(passTime)
        self.view.addSubview(name)
        self.view.addSubview(phone)
        self.view.addSubview(barcode)
        self.view.addSubview(progress)
        self.view.addSubview(viewPager)
        
        ticketStatusView.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(barcode.snp.bottom)
        }
        
        previewView.snp.makeConstraints{make in
            make.size.equalTo(150)
            make.leading.equalToSuperview().offset(8)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(8)
        }
        
        sector.snp.makeConstraints{make in
            make.leading.equalTo(previewView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(8)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(8)
        }
        
        place.snp.makeConstraints{make in
            make.leading.equalTo(previewView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(8)
            make.top.equalTo(sector.snp.bottom).offset(8)
        }
        
        passTime.snp.makeConstraints{make in
            make.leading.equalTo(previewView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(8)
            make.top.equalTo(place.snp.bottom).offset(8)
        }
        
        name.snp.makeConstraints{make in
            make.leading.equalTo(previewView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(8)
            make.top.equalTo(passTime.snp.bottom).offset(8)
        }
        
        phone.snp.makeConstraints{make in
            make.leading.equalTo(previewView.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(8)
            make.top.equalTo(name.snp.bottom).offset(8)
        }
        
        progress.snp.makeConstraints{make in
            make.trailing.top.bottom.equalTo(ticketStatusView)
            make.leading.equalTo(previewView.snp.trailing)
        }
        barcode.snp.makeConstraints{make in
            make.leading.trailing.equalTo(previewView)
            make.top.equalTo(previewView.snp.bottom)
        }
        viewPager.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(ticketStatusView.snp.bottom)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
        let historyVC = HistoryViewController(eventId: eventId)
        let statisticsVC = StatisticsViewController(eventId: eventId)
        viewControllers = [historyVC, statisticsVC]
        let titles = [Strings.history, Strings.statistics]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: titles, delegate: self)
        carbonTabSwipeNavigation.setIndicatorColor(colorPrimary)
        carbonTabSwipeNavigation.setNormalColor(colorPrimary)
        carbonTabSwipeNavigation.setSelectedColor(colorAccent)
        
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: viewPager)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(screenWidth / 2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(screenWidth / 2, forSegmentAt: 1)
        
        scanner = MTBBarcodeScanner(metadataObjectTypes: [AVMetadataObject.ObjectType.qr.rawValue], previewView: previewView)
        //        scanner?.didTapToFocusBlock
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cameraMode()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stopScanning()
        if true == resetLastCodeTimer?.isValid{
            resetLastCodeTimer?.invalidate()
            
        }
        if true == clearDataTimer?.isValid{
            clearDataTimer?.invalidate()
            
        }
        clearTicketData()
        clearDataTimer = nil
        resetLastCodeTimer = nil
    }
    
    func showEventTitle(title: String?) {
        self.navigationItem.title = title
    }
    
    func playSuccessTone() {
        AudioServicesPlaySystemSoundWithCompletion(1210, nil)
    }
    
    func setCheckTicketProgressIndicator(b: Bool) {
        if b{
            AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, nil)
            progress.isHidden = false
            progress.startAnimating()
        }else{
            progress.stopAnimating()
            progress.isHidden = true
        }
    }
    
    func setLoadingTicketsIndicator(show: Bool) {
        if show{
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func showAlreadyPassedError(ticket: TicketFull?) {
        let phone = ticket?.phone ?? ""
        
        passTime.isHidden = false
        if (ticket?.passTime != nil) {
            let seconds = ticket?.passTime?.doubleValue ?? 0
            let date = Date(timeIntervalSince1970: seconds)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            passTime.text = String(format: Strings.passed_at, arguments: [dateFormatter.string(from: date)])
        } else {
            passTime.text = ""
        }
        self.phone.text = phone
        var place = ""
        if let row = ticket?.row, !row.isEmpty{
            place = String(format: Strings.row, arguments: [row])
        }
        if let column = ticket?.column, !column.isEmpty{
            if !place.isEmpty{
                place += ", "
            }
            place += String(format: Strings.column, arguments: [column])
        }
        self.place.text = place
        self.name.text = ticket?.fio
        self.sector.text = ticket?.sector
        self.barcode.text = ticket?.code
        self.ticketStatusView.backgroundColor = .red
        clearDataTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(clearTicketData), userInfo: nil, repeats: false)
        
    }
    
    func showCheckTicketError(error: KotlinThrowable) {
        NSLog("\(error)")
        AudioServicesPlaySystemSoundWithCompletion(1073, nil)
        if let ticketError = error as? TicketError, let ticket = ticketError.ticketFull{
           showAlreadyPassedError(ticket: ticket)
            return
        }
        var message = error.message
        if let ticketError = error as? TicketError, ticketError.type == TicketError.Type_.notFound{
            message = Strings.ticket_not_found
        }
        
        let alert = UIAlertController.init(title: Strings.error, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK",
                                      style: .default, handler: {_ in
                                    self.clearTicketData()
        }))
        alert.view.tintColor = colorPrimary
        present(alert, animated: true)
    }
    
    func showPassedTicket(ticket: TicketFull) {
        passTime.isHidden = true
        phone.text = ticket.phone
        var place = ""
        if let row = ticket.row, !row.isEmpty{
            place = String(format: Strings.row, arguments: [row])
        }
        if let column = ticket.column, !column.isEmpty{
            if !place.isEmpty{
                place += ", "
            }
            place += String(format: Strings.column, arguments: [column])
        }
        self.place.text = place
        self.name.text = ticket.fio
        self.sector.text = ticket.sector
        self.barcode.text = ticket.code
        self.ticketStatusView.backgroundColor = .green
        playSuccessTone()
        
        AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, nil)
        clearDataTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(clearTicketData), userInfo: nil, repeats: false)
    }
    
    func showTickedPassedSuccess() {
        ticketStatusView.backgroundColor = colorSuccess
        playSuccessTone()
    }
    
    func showUpdateError() {
        let alert = UIAlertController(title: Strings.error, message: Strings.update_error, defaultActionButtonTitle: "OK", tintColor: colorPrimary)
        present(alert, animated: true, completion: nil)
    }
    
    private func onScan(code: String){
        if (lastScanning != nil) {
            return
        }
        lastScanning = code
        barcode.text = code
        if (code.count != 6
                || !pattern.matches(code)) {
            AudioServicesPlaySystemSoundWithCompletion(1052, nil)
            AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, nil)
            resetLastCodeTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(resetLastScanningCode), userInfo: nil, repeats: false)
            return
        }
        presenter.onScan(code: code)
    }
    
    @objc
    private func resetLastScanningCode(){
        lastScanning = nil
    }
    
    
    @objc
    private func clearTicketData() {
        ticketStatusView.backgroundColor = .clear
        lastScanning = nil
        self.keyboardView?.onClearTap()
    }
    
    @objc
    private func keyboardMode(){
        if  barcode.isHidden == false{
            btnCamera.tintColor = .white
            btnKeyboard.tintColor = colorAccent
            barcode.isHidden = true
            viewPager.isHidden = true
            self.scanner?.torchMode = .off
            self.scanner?.stopScanning()
            let navBarHeight = (navigationController?.navigationBar.frame.height ?? 0.0)
            let height = screenHeight - topSafeAreaHeight - 150 - navBarHeight - 32 - bottomSafeAreaHeight
            keyboardView = Keyboard.show(superView: self.view, height: height)
            lastScanning = nil
            keyboardView?.nextCode = {code in self.presenter.onScan(code: code)}
        }
    }
    
    @objc
    private func cameraMode(){
        if scanner?.isScanning() == false {
            btnCamera.tintColor = colorAccent
            btnKeyboard.tintColor = .white
            barcode.isHidden = false
            keyboardView?.hide()
            viewPager.isHidden = false
            startDecodingWithRequestPermission()
        }
    }
    
    @objc
    private func showMore(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Strings.refresh, style: .default, handler: {_ in self.didTapUpdate()}))
        if !barcode.isHidden{
            alert.addAction(UIAlertAction(title: Strings.torch, style: .default, handler: {_ in self.didTapTorch()}))
        }
        alert.addAction(title: Strings.cancel, style: .cancel)
        alert.view.tintColor = colorPrimary
        present(alert, animated: true, completion: nil)
    }
    
    @objc
    private func didTapAutofocus(){
        
    }
    
    @objc
    private func didTapTorch(){
        scanner?.toggleTorch()
    }
    
    @objc
    private func didTapUpdate(){
        presenter.loadTickets(eventId: eventId)
    }
    
    private func startDecodingWithRequestPermission() {
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        
                        if let codes = codes {
                            for code in codes {
                                let stringValue = code.stringValue!
                                self.onScan(code: stringValue)
                            }
                        }
                    })
                } catch {
                    let alert = UIAlertController.init(title: Strings.scanning_unavailable,
                                                       message: Strings.error_happen,
                                                       defaultActionButtonTitle: "OK",
                                                       tintColor: colorPrimary)
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let alert = UIAlertController.init(title: Strings.scanning_unavailable,
                                                   message: Strings.app_does_not_have_camera_permission,
                                                   defaultActionButtonTitle: "OK",
                                                   tintColor: colorPrimary)
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
}

extension ControlViewController: CarbonTabSwipeNavigationDelegate{
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        return viewControllers[Int(index)]
    }
    
}
