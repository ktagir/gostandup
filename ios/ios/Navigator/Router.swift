//
//  Router.swift
//  ios
//
//  Created by Tagir Kuramshin on 31.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import gostandup_core
import XCoordinator

enum MainRoute: Route{
    case login
    case events
    case control(eventId: Int64)
    case pop
    case dismiss
}

class Coordinator: NavigationCoordinator<MainRoute> {
    
    init(rootViewController: UINavigationController) {
        super.init(rootViewController: rootViewController, initialRoute: nil)
    }
    
    override func prepareTransition(for route: MainRoute) -> NavigationTransition {
        switch route {
        case .login:
            return navigateToLogin()
        case .events:
            return navigateToEvents()
        case .control(let eventId):
            return navigateToEvent(eventId: eventId)
        case .pop:
            return .pop()
        case .dismiss:
            return .dismiss()
        }
        
    }
    
    private func navigateToLogin() -> NavigationTransition{
        let vc = LoginViewController()
        return .set([vc], animation: .none)
    }
    
    private func navigateToEvents() -> NavigationTransition{
        let vc = EventsViewController()
        return .set([vc], animation: .none)
    }
    
    private func navigateToEvent(eventId: Int64) -> NavigationTransition{
        NSLog("navigateToEvent: \(eventId)")
        let vc = ControlViewController(eventId: eventId)
        return .set([vc], animation: .none)
    }
}

class GoStandupRouter: gostandup_core.Router {
    
    private let coordinator: Coordinator
    
    init(coordinator: Coordinator) {
        self.coordinator = coordinator
    }
    
    func dismiss() {
        coordinator.trigger(.dismiss)
    }
    
    func navigateToControl(eventId: Int64) {
        coordinator.trigger(.control(eventId: eventId))
    }
    
    func navigateToEvents() {
        coordinator.trigger(.events)
    }
    
    func navigateToLogin() {
        coordinator.trigger(.login)
    }
    
    func pop() {
        coordinator.trigger(.pop)
    }
    
    
}
