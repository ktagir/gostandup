//
//  NSRegularExpression+Matches.swift
//  ios
//
//  Created by Tagir Kuramshin on 06.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation

extension NSRegularExpression {
    func matches(_ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return firstMatch(in: string, options: [], range: range) != nil
    }
}
