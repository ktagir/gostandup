//
//  MvpViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 31.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core
import MBProgressHUD
import KYDrawerController

class MvpViewController<P: Lib_kmm_mvpMvpPresenter>: UIViewController, Lib_kmm_mvpMvpView{
    
    private var handleKeyboardShow = false
    private var targetScrollView: UIScrollView?
    private var defaultInset: UIEdgeInsets?
   
    private var hideGesture: UIGestureRecognizer!
    
    var presenter: P!
    
    deinit {
        presenter = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
           return .lightContent
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter = createPresenter()
        
        let menuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        menuBtn.setImageForAllStates(UIImage(named: "menu")!)
        menuBtn.addTarget(self, action: #selector(menuClicked), for: .touchUpInside)
        let menu = UIBarButtonItem(customView: menuBtn)
        self.navigationItem.setLeftBarButton(menu, animated: false)
        
        hideGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard(sender:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        if handleKeyboardShow == true {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIWindow.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIWindow.keyboardWillHideNotification, object: nil)
        }
        
        presenter.attachView(view: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.detachView()
        if handleKeyboardShow == true {
            NotificationCenter.default.removeObserver(self, name: UIWindow.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIWindow.keyboardWillHideNotification, object: nil)
        }
    }
    
    func createPresenter() -> P {
        fatalError("Not Implemented")
    }
    
    func showProgress(show: Bool) {
        if(show){
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func showError(t: KotlinThrowable) {
        let alert = UIAlertController(title: Strings.error, message: t.description(), defaultActionButtonTitle: "OK", tintColor: colorPrimary)
        present(alert, animated: true, completion: nil)
    }
    
    func setTitle(_ title:String){
        self.navigationItem.title = title
    }
    
    @objc func menuClicked(){
        if let drawerController = navigationController?.parent as? KYDrawerController {
            let state = drawerController.drawerState
            if state == .closed{
                drawerController.setDrawerState(.opened, animated: true)
            }
        }
    }
    
    func handleKeyboardShow(for targetScrollView: UIScrollView) {
        handleKeyboardShow = true
        self.targetScrollView = targetScrollView
        targetScrollView.isUserInteractionEnabled = true
        targetScrollView.removeGestureRecognizer(hideGesture)
        targetScrollView.addGestureRecognizer(hideGesture)
    }
    
    //MARK:- Notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            
            if defaultInset == nil {
                defaultInset = targetScrollView?.contentInset
            }
            
            if var inset = defaultInset {
                inset.bottom += keyboardHeight
                targetScrollView?.contentInset = inset
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let defaultInset = defaultInset {
            targetScrollView?.contentInset = defaultInset
        }
    }
    @objc func hideKeyboard(sender: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    func hideKeyboard(){
        view.endEditing(true)
    }
  
    
}
