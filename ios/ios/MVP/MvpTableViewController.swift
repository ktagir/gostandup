//
//  MvpTableViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 01.08.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core
import SnapKit
import MaterialComponents

class MvpTableViewController<M: Hashable, P: Lib_kmm_mvpMvpListPresenter>: MvpViewController<P>, Lib_kmm_mvpMvpListView {
    
    let tableView: UITableView = {
        let tableview = UITableView(frame: .zero, style: .grouped)
        tableview.separatorInset = UIEdgeInsets(horizontal: 0, vertical: 0)
        tableview.backgroundColor = .clear
        return tableview
    }()
    
    let emptyView: UILabel = {
        let label = UILabel()
        label.textColor = colorPrimaryText
        label.text = Strings.empty_list
        label.textAlignment = .center
        label.isHidden = true
        return label
    }()
    
    let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = colorAccent
        return refreshControl
    }()
    
    let progress: UIActivityIndicatorView = {
        let progress: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            progress = UIActivityIndicatorView(style: .large)
        } else {
            progress = UIActivityIndicatorView()
        }
        progress.color = colorAccent
        progress.hidesWhenStopped = true
        progress.isHidden = true
        return progress
    }()
    
    
    
    private(set) var tableDataSource: DataSourceBase!
    private var items: [M] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(emptyView)
        self.view.addSubview(tableView)
        self.view.addSubview(progress)
        
        tableView.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
        }
        emptyView.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(horizontal: 32, vertical: 32))
            make.height.height.equalTo(48)
            make.centerY.equalToSuperview()
        }
        progress.snp.makeConstraints{make in
            make.center.equalToSuperview()
        }
        
        if #available(iOS 13, *) {
            tableDataSource = DiffableTableDataSource(tableView: self.tableView, cell: {t, i, m in self.cell(tableView: t, indexPath: i, item: m)} )
        }else{
            tableDataSource = TableDataSource(tableView: self.tableView, cell: {t, i, m in self.cell(tableView: t, indexPath: i, item: m)})
            tableView.dataSource = (tableDataSource as! TableDataSource<M>)
        }
        
        
        tableView.refreshControl = refreshControl
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: UIControl.Event.valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.loadData()
    }
    
    @objc
    private func pullToRefresh(_ sender: Any){
        presenter.update()
    }
    
    func showLoadMoreError(throwable: KotlinThrowable) {
        
    }
    
    func showUpdateError(throwable: KotlinThrowable) {
        refreshControl.endRefreshing()
        let alert = UIAlertController(title: Strings.error, message: Strings.update_error, defaultActionButtonTitle: "OK", tintColor: colorPrimary)
        present(alert, animated: true, completion: nil)
    }
    
    func updateState(viewState: Lib_kmm_mvpViewState<NSArray>) {
        let data = viewState.data
        let empty = data == nil || data!.count == 0
        emptyView.isHidden = !(empty && viewState.completed)
        if viewState.loading{
            progress.isHidden = false
            progress.startAnimating()
        }else{
            progress.stopAnimating()
        }
        
        if viewState.refreshing{
            if !refreshControl.isRefreshing{
                refreshControl.beginRefreshing()
            }
        }else{
            refreshControl.endRefreshing()
        }
        if #available(iOS 13, *){
            (tableDataSource as! DiffableTableDataSource).update(items: data as? Array<M>)
        }else{
            (tableDataSource as! TableDataSource).update(items: data as? Array<M>)
        }
    }
    
    
    func cell(tableView: UITableView, indexPath: IndexPath, item: M)-> UITableViewCell{
        fatalError("Unimplemented!")
    }
    
    func getItem(indexPath: IndexPath) -> M?{
        if #available(iOS 13, *) {
            return (tableDataSource as! DiffableTableDataSource<M>).tableDataSource.itemIdentifier(for: indexPath)
        }else{
            return (tableDataSource as! TableDataSource<M>).items[indexPath.row]
        }
    }
}

protocol DataSourceBase {
    
}

protocol DataSource: DataSourceBase{
    
    associatedtype M: Hashable
    
    func update(items: [M]?)
}

class TableDataSource<M: Hashable>: NSObject, UITableViewDataSource, DataSource {
    
    var items: [M] = []
    
    weak var tableView: UITableView!
    
    private let cell: ((UITableView, IndexPath, M) -> UITableViewCell)
    
    
    init(tableView: UITableView, cell: @escaping ((UITableView, IndexPath, M) -> UITableViewCell)) {
        self.tableView = tableView
        self.cell = cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell(tableView, indexPath, items[indexPath.row])
    }
    
    func update(items: [M]?){
        self.items = items ?? []
        tableView.reloadData()
    }
    
    
    
}

@available(iOS 13.0, *)
class DiffableTableDataSource<M: Hashable>: NSObject, DataSource{
    
    let tableDataSource: UITableViewDiffableDataSource<String, M>
    
    init(tableView: UITableView, cell: @escaping ((UITableView, IndexPath, M) -> UITableViewCell)) {
        tableDataSource = UITableViewDiffableDataSource(tableView: tableView,
                                                        cellProvider: {(tableView, indexPath, item) in
                                                            cell(tableView, indexPath, item)})
        tableDataSource.defaultRowAnimation = .fade
    }
    
    func update(items: [M]?){
        var snapshot = NSDiffableDataSourceSnapshot<String, M>()
        snapshot.appendSections(["1"])
        snapshot.appendItems(items ?? [], toSection: "1")
        tableDataSource.apply(snapshot)
        
    }
}
