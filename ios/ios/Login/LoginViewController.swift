//
//  LoginViewController.swift
//  ios
//
//  Created by Tagir Kuramshin on 31.07.2020.
//  Copyright © 2020 Tagir Kuramshin. All rights reserved.
//

import Foundation
import UIKit
import gostandup_core
import SnapKit
import MaterialComponents

class LoginViewController: MvpViewController< LoginContractPresenter>, LoginContractView {
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.backgroundColor = colorPrimary
        return scrollView
    }()
    
    private let logo: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo_96dp"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let username: MDCTextField = {
        let textField = MDCTextField()
        textField.keyboardType = .emailAddress
        textField.textContentType = .emailAddress
        textField.textColor = colorPrimaryTextDark
        textField.clearButton.tintColor = colorHintText
        textField.returnKeyType = .next
        return textField
    }()
    
    private let password: MDCTextField = {
        let textField = MDCTextField()
        textField.textContentType = .password
        textField.isSecureTextEntry = true
        textField.textColor = colorPrimaryTextDark
        textField.clearButton.tintColor = colorHintText
        textField.returnKeyType = .done
        return textField
    }()
    
    private let textFieldColorScheme: MDCContainerScheme = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.primaryColor = colorAccent
        containerScheme.colorScheme.primaryColorVariant = colorHintText
        containerScheme.colorScheme.secondaryColor = colorHintText
        return containerScheme
    }()
    
    private var usernameWrapper: MDCTextInputControllerUnderline!
    private var passwordWrapper: MDCTextInputControllerUnderline!
    
    private let goBtn: MDCButton = {
        let btn = MDCButton()
        btn.setTitle(Strings.go, for: .normal)
        btn.addTarget(self, action: #selector(go), for: .touchUpInside)
        btn.backgroundColor = colorAccent
        btn.setTitleColor(colorPrimaryText, for: .normal)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.delegate = self
        password.delegate = self
        
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        if let user = container.resolve(Repository.self)!.user{
            username.text = user.email
        }
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints{make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        let contentView = UIView()
        scrollView.addSubview(contentView)
        
        contentView.snp.makeConstraints{make in
            make.width.height.top.bottom.equalToSuperview()
            make.leading.trailing.equalTo(view)
        }
        
        //        self.view.backgroundColor = colorPrimary
        contentView.addSubview(logo)
        contentView.addSubview(username)
        contentView.addSubview(password)
        contentView.addSubview(goBtn)
        
        let margin: CGFloat = 48
        
        logo.snp.makeConstraints{make in
            //            make.top.greaterThanOrEqualToSuperview().offset(32)
            make.size.equalTo(80)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(username.snp.top).offset(-60)
        }
        username.snp.makeConstraints{make in
            make.leading.trailing.centerY.equalToSuperview().inset(UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin))
            make.height.equalTo(72)
        }
        password.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin))
            make.top.equalTo(username.snp.bottom)
            make.height.equalTo(72)
        }
        goBtn.snp.makeConstraints{make in
            make.leading.trailing.equalToSuperview().inset(UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin))
            make.top.equalTo(password.snp.bottom).offset(16).priority(.required)
            make.bottom.greaterThanOrEqualToSuperview().offset(-32).priority(.low)
        }
        usernameWrapper = MDCTextInputControllerUnderline(textInput: username)
        usernameWrapper.applyTheme(withScheme: textFieldColorScheme)
        usernameWrapper.placeholderText = Strings.username
        usernameWrapper.inlinePlaceholderColor = colorHintText
        usernameWrapper.floatingPlaceholderNormalColor = colorHintText
        
        passwordWrapper = MDCTextInputControllerUnderline(textInput: password)
        passwordWrapper.applyTheme(withScheme: textFieldColorScheme)
        passwordWrapper.placeholderText = Strings.password
        passwordWrapper.inlinePlaceholderColor = colorHintText
        passwordWrapper.floatingPlaceholderNormalColor = colorHintText
        
        handleKeyboardShow(for: scrollView)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc
    private func go(){
        presenter.loginWith(email: username.text ?? "", password: password.text ?? "")
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        container.resolve(Preferences.self)!.putString(key: "login", value: username.text)
    }
    
    override func createPresenter() -> LoginContractPresenter {
        let container = (UIApplication.shared.delegate as! AppDelegate).container
        return container.resolve(LoginContractPresenter.self)!
    }
    
    func hideInputErrors() {
        usernameWrapper.setErrorText(nil, errorAccessibilityValue: nil)
        passwordWrapper.setErrorText(nil, errorAccessibilityValue: nil)
    }
    
    func showEmptyLoginError() {
        usernameWrapper.setErrorText(Strings.field_must_not_be_empty, errorAccessibilityValue: nil)
    }
    
    func showEmptyPasswordError() {
        passwordWrapper.setErrorText(Strings.field_must_not_be_empty, errorAccessibilityValue: nil)
    }
    
    override func showError(t: KotlinThrowable) {
        var error = Strings.error_happen
        if let e = t as? HttpException, e.code == 403{
            error = Strings.check_username_and_password
        }
        let alert = UIAlertController(title: Strings.error, message: error, preferredStyle: .alert)
        alert.addAction(title: "OK")
        present(alert, animated: true, completion: nil)
    }
    
}

extension LoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === username{
            password.becomeFirstResponder()
        } else if textField === password{
            self.go()
            self.hideKeyboard()
        }else{
            self.hideKeyboard()
        }
        
        return false
    }
}


