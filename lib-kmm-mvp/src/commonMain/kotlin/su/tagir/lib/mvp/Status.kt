package su.tagir.lib.mvp

enum class Status {
    LOADING,
    REFRESHING,
    LOADING_MORE,
    ERROR,
    SUCCESS
}