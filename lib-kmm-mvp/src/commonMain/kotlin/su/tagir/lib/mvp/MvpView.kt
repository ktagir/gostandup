package su.tagir.lib.mvp

interface MvpView{
    fun showError(t: Throwable)
    fun showProgress(show: Boolean)
}