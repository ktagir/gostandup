package su.tagir.lib.mvp

interface MvpPresenter<V : MvpView> {

    fun attachView(view: V)
    fun detachView()
    fun destroy()

}