package su.tagir.lib.mvp

interface MvpListView<M> : MvpView {
    fun updateState(viewState: ViewState<List<M>>)
    fun showUpdateError(throwable: Throwable){}
    fun showLoadMoreError(throwable: Throwable){}
}