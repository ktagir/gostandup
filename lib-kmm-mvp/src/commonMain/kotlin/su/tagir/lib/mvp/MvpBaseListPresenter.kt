package su.tagir.lib.mvp


import co.touchlab.stately.concurrency.AtomicReference
import co.touchlab.stately.concurrency.value
import co.touchlab.stately.freeze
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import su.tagir.lib.kmm.logger.Logger

abstract class MvpBaseListPresenter<M: Any, V: MvpListView<M>> : MvpBasePresenter<V>(), MvpListPresenter<M, V> {

    protected val state = AtomicReference<ViewState<List<M>>>(ViewState(status = Status.SUCCESS))

    override var exceptionHandler: CoroutineExceptionHandler = CoroutineExceptionHandler{ _, throwable ->
        Logger.e(this::class.simpleName, throwable)
        updateState(Status.ERROR)
    }

    protected var updateExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        ifViewAttached({v -> v.showUpdateError(throwable)})
        updateState(Status.SUCCESS)
    }

    private var loadMoreExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        ifViewAttached({v -> v.showUpdateError(throwable)})
        updateState(Status.SUCCESS)
    }

    override fun loadMore(lastIndex: Int) {
        if (!state.value.isCanLoadMore) {
            return
        }
        updateState(Status.LOADING_MORE)
        launch(loadMoreExceptionHandler){
            loadMoreBlock(lastIndex)
            updateState(Status.SUCCESS)
        }
    }

    override fun update() {
        updateState(Status.REFRESHING)
        launch(updateExceptionHandler){
            updateBlock()
            updateState(Status.SUCCESS)
        }
    }

    open suspend fun updateBlock() {}

    open suspend fun loadMoreBlock(lastIndex: Int) {}

    protected fun updateState(status: Status,
                              data: List<M>? = state.value.data,
                              error: Throwable? = state.value.throwable,
                              hasNextPage: Boolean = state.value.hasNextPage){
        state.value = ViewState(status, data, error, hasNextPage).freeze()
        ifViewAttached({v -> v.updateState(state.value)})
    }
}