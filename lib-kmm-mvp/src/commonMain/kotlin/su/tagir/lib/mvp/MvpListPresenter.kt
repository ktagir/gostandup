package su.tagir.lib.mvp

interface MvpListPresenter<M, V: MvpListView<M>>: MvpPresenter<V> {

    fun loadData()

    fun loadMore(lastIndex: Int)

    fun update()

}