package su.tagir.lib.kmm.logger

import platform.Foundation.NSLog

actual object Logger {

    private val isDebug = Platform.isDebugBinary


    actual fun d(tag: String?, message: String?){
        if(isDebug) {
            NSLog("$tag: $message")
        }
    }
    actual fun e(tag: String?, exception: Throwable?){
        if(isDebug) {
            NSLog("$tag: ${exception?.message}")
            exception?.getStackTrace()?.joinToString("\n")?.let {
                NSLog(it)
            }
        }

    }
    actual fun e(tag: String?, message: String?, exception: Throwable?){
        if(isDebug) {
            NSLog("$tag: $message")
            exception?.getStackTrace()?.joinToString("\n")?.let {
                NSLog(it)
            }
        }
    }
}