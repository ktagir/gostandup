package su.tagir.lib.kmm.logger

import timber.log.Timber

actual object Logger {

    actual fun d(tag: String?, message: String?) {
        Timber.tag(tag).d(message)
    }

    actual fun e(tag: String?, exception: Throwable?) {
        Timber.tag(tag).e(exception)
    }

    actual fun e(tag: String?, message: String?, exception: Throwable?) {
        Timber.tag(tag).e(exception, message)
    }

}