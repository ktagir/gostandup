package ru.gostandup.tickets.model

import co.touchlab.stately.concurrency.AtomicReference
import co.touchlab.stately.concurrency.value
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import ru.gostandup.tickets.GoStandUpDb
import ru.gostandup.tickets.model.db.Schema

import kotlin.native.concurrent.freeze

object Db {
    private val driverRef = AtomicReference<SqlDriver?>(null)
    private val dbRef = AtomicReference<GoStandUpDb?>(null)

    internal fun dbSetup(driver: SqlDriver) {
        val db = createQueryWrapper(driver)
        driverRef.value = driver.freeze()
        dbRef.value = db.freeze()
    }

    internal fun dbClear() {
        driverRef.value!!.close()
        dbRef.value = null
        driverRef.value = null
    }

    // Called from Swift
    @Suppress("unused")
    fun defaultDriver() {
        Db.dbSetup(NativeSqliteDriver(Schema, "database.db"))
    }

    val instance: GoStandUpDb
        get() = dbRef.value!!
}