package ru.gostandup.tickets.platform.preferences

import platform.Foundation.NSUserDefaults

actual class Preferences {

    private val userDefault: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    actual fun getString(key: String, defaultValue: String?): String? = userDefault.stringForKey(key) ?: defaultValue

    actual fun putString(key: String, value: String?) {
        userDefault.setObject(value, key)
    }

    actual fun getInt(key: String, defaultValue: Int): Int {
        return if (contains(key))
            userDefault.integerForKey(key).toInt()
        else defaultValue
    }

    actual fun putInt(key: String, value: Int) {
        userDefault.setInteger(value.toLong(), key)
    }

    actual fun getLong(key: String, defaultValue: Long): Long {
        return if (contains(key))
            userDefault.integerForKey(key)
        else defaultValue
    }

    actual fun putLong(key: String, value: Long) {
        userDefault.setInteger(value, key)
    }

    actual fun getFloat(key: String, defaultValue: Float): Float {
        return if (contains(key))
            userDefault.floatForKey(key)
        else defaultValue
    }

    actual fun putFloat(key: String, value: Float) {
        userDefault.setFloat(value, key)
    }


    actual fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return if (contains(key))
            userDefault.boolForKey(key)
        else defaultValue
    }

    actual fun putBoolean(key: String, value: Boolean) {
        userDefault.setBool(value, key)
    }

    actual fun contains(key: String): Boolean = userDefault.objectForKey(key) != null

    actual fun remove(key: String) {
        userDefault.removeObjectForKey(key)
    }

    actual fun clear() {
        for (key in userDefault.dictionaryRepresentation().keys) {
            remove(key as String)
        }
    }


}

