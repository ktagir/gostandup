package ru.gostandup.tickets.platform.network

import cocoapods.AFNetworking.AFHTTPSessionManager
import cocoapods.AFNetworking.AFJSONRequestSerializer
import cocoapods.AFNetworking.AFJSONResponseSerializer
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import platform.Foundation.*
import ru.gostandup.tickets.model.HttpException
import ru.gostandup.tickets.model.TicketError
import ru.gostandup.tickets.model.auth.AuthHolder
import ru.gostandup.tickets.model.entities.AuthData
import ru.gostandup.tickets.model.entities.Event
import ru.gostandup.tickets.model.entities.Ticket
import ru.gostandup.tickets.model.entities.User
import ru.gostandup.tickets.model.json
import su.tagir.lib.kmm.logger.Logger
import kotlin.coroutines.resumeWithException

actual class RestClient(private val authHolder: AuthHolder,
                        responseSerializer: AFJSONResponseSerializer,
                        debug: Boolean) {

    private val baseUrl: String = if (debug) BASE_URL_DEV else BASE_URL
    private val manager: AFHTTPSessionManager

    private val headers: Map<Any?, *> = mapOf(Pair("Content-Type", "application/json; charset=UTF-8"))

    init {
        manager = AFHTTPSessionManager(baseURL = NSURL.URLWithString(baseUrl))
        manager.responseSerializer = responseSerializer
        manager.requestSerializer = AFJSONRequestSerializer()
    }


    actual suspend fun login(authData: AuthData): User {
        val jsonData = (json.encodeToString(authData) as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        val data = performRequest(RequestMethod.POST, "auth", jsonData)
        val jsonString = NSString.create(data, NSUTF8StringEncoding) as String
        val user: User =  json.decodeFromString(jsonString)
        authHolder.accessToken = user.authKey
        return user
    }

    actual suspend fun getEvents(): List<Event> {
        val data = performRequest(RequestMethod.GET, "events")
        val jsonString = NSString.create(data, NSUTF8StringEncoding) as String
        return json.decodeFromString(ListSerializer(Event.serializer()), jsonString)
    }

    actual suspend fun getTickets(eventId: Long): Event {
        val data = performRequest(RequestMethod.GET, "events/$eventId")
        val jsonString = NSString.create(data, NSUTF8StringEncoding) as String
        return json.decodeFromString(jsonString)
    }

    actual suspend fun updateTicket(ticketCode: String): Ticket = suspendCancellableCoroutine{ cont ->
        val onSuccess = { _: NSURLSessionDataTask?, response: Any? ->
            val data = response as? NSDictionary
            if (data == null) {
                cont.resumeWithException(Throwable("Update Ticket. Response body is null!"))
            } else {
                try {
                    val ticket: Ticket = parseJsonDictionary(data)
                    cont.resumeWith(Result.success(ticket))
                } catch (e: Exception) {
                    cont.resumeWithException(e)
                }
            }
        }
        val onError = { task: NSURLSessionDataTask?, error: NSError? ->
            val response = task?.response as? NSHTTPURLResponse
            if(response != null){
                when (response.statusCode) {
                    404L -> cont.resumeWithException(TicketError.notFound())

                    406L -> {
                        var ticket: Ticket? = null
                        try {
                            ( error?.userInfo?.get("data") as? NSDictionary)?.let {data ->
                                ticket = parseJsonDictionary(data)
                            }
                        }catch (e: Exception){
                            Logger.e("RestClient", e)
                        }
                        Logger.d("RestClient", "ticket: $ticket")
                        cont.resumeWithException(TicketError(type = TicketError.Type.ALREADY_PASSED, message = error?.localizedDescription, ticket = ticket))
                    }
                    else -> cont.resumeWithException(HttpException(code = response.statusCode.toInt(), errorMessage = error?.localizedDescription))
                }

            }else {
                cont.resumeWithException(HttpException(errorMessage = error?.localizedDescription))
            }
        }
        val requestUrl =  "tickets/$ticketCode?access-token=${authHolder.accessToken}"
        manager.PATCH(requestUrl, null, headers, onSuccess, onError)

    }

    actual suspend fun getTicket(ticketId: Long): Ticket {
        TODO("Not yet implemented")
    }

    private suspend fun performRequest(method: RequestMethod, url: String, body: NSData? = null): NSData = suspendCancellableCoroutine { susp ->
        val onSuccess = { _: NSURLSessionDataTask?, response: Any? ->
            val data = response as? NSDictionary ?: response as? NSArray
            if (data == null) {
                susp.resumeWithException(Throwable("$method $url. Response body is null!"))
            } else {
                try {
                    val jsonData = NSJSONSerialization.dataWithJSONObject(data, NSJSONWritingPrettyPrinted, null)!!
                    susp.resumeWith(Result.success(jsonData))
                } catch (e: Exception) {
                    susp.resumeWithException(e)
                }
            }
        }
        val onError = { task: NSURLSessionDataTask?, error: NSError? ->
            val response = task?.response as? NSHTTPURLResponse
            val code = response?.statusCode?.toInt() ?: 500
            susp.resumeWithException(HttpException(code = code, errorMessage = error?.localizedDescription))
        }

        val data = body?.let { NSJSONSerialization.JSONObjectWithData(it, NSJSONReadingAllowFragments, null) }

        val requestUrl = authHolder.accessToken?.let { "$url?access-token=$it" } ?: url
        when (method) {
            RequestMethod.GET -> manager.GET(requestUrl, null, headers, null, onSuccess, onError)
            RequestMethod.POST -> manager.POST(requestUrl, data, headers, null, onSuccess, onError)
            RequestMethod.PUT -> manager.PUT(requestUrl, data, headers, onSuccess, onError)
            RequestMethod.PATCH -> manager.PATCH(requestUrl, data, headers, onSuccess, onError)
            RequestMethod.DELETE -> manager.DELETE(requestUrl, data, headers, onSuccess, onError)
        }
    }

    private inline fun <reified T>parseJsonDictionary(dictionary: NSDictionary): T {
        val jsonData = NSJSONSerialization.dataWithJSONObject(dictionary, NSJSONWritingPrettyPrinted, null)!!
        val jsonString = NSString.create(jsonData, NSUTF8StringEncoding) as String
        return json.decodeFromString(jsonString)
    }

    private enum class RequestMethod {
        GET, POST, PUT, PATCH, DELETE
    }
}