package ru.gostandup.tickets.platform.preferences

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

actual class Preferences(context: Context) {

    private val sharedPreferences: SharedPreferences

    init {

        val keyGenParameterSpec = KeyGenParameterSpec.Builder(
                MasterKey.DEFAULT_MASTER_KEY_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(256)
                .build()

        val masterKey = MasterKey.Builder(context).setKeyGenParameterSpec(keyGenParameterSpec).build()
        sharedPreferences = EncryptedSharedPreferences
                .create(context,
                        "preferences",
                        masterKey,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
    }

    actual fun getString(key: String, defaultValue: String?): String? = sharedPreferences.getString(key, defaultValue)

    actual fun putString(key: String, value: String?) = sharedPreferences.edit().putString(key, value).apply()

    actual fun getInt(key: String, defaultValue: Int): Int = sharedPreferences.getInt(key, defaultValue)

    actual fun putInt(key: String, value: Int)  = sharedPreferences.edit().putInt(key, value).apply()

    actual fun getLong(key: String, defaultValue: Long): Long = sharedPreferences.getLong(key, defaultValue)

    actual fun putLong(key: String, value: Long)  = sharedPreferences.edit().putLong(key, value).apply()

    actual fun getFloat(key: String, defaultValue: Float): Float = sharedPreferences.getFloat(key, defaultValue)

    actual fun putFloat(key: String, value: Float)  = sharedPreferences.edit().putFloat(key, value).apply()

    actual fun getBoolean(key: String, defaultValue: Boolean): Boolean = sharedPreferences.getBoolean(key, defaultValue)

    actual fun putBoolean(key: String, value: Boolean)  = sharedPreferences.edit().putBoolean(key, value).apply()

    actual fun contains(key: String): Boolean = sharedPreferences.contains(key)

    actual fun remove(key: String) = sharedPreferences.edit().remove(key).apply()

    actual fun clear() = sharedPreferences.edit().clear().apply()

}