package ru.gostandup.tickets.platform

import android.content.Context
import androidx.sqlite.db.SupportSQLiteOpenHelper
import com.squareup.sqldelight.android.AndroidSqliteDriver
import ru.gostandup.tickets.GoStandUpDb.Companion.Schema
import su.tagir.lib.sqlite.SqliteOpenHelperFactory

fun driver(sqLiteOpenHelper: SupportSQLiteOpenHelper): AndroidSqliteDriver = AndroidSqliteDriver(sqLiteOpenHelper)

fun sqlOpenHelper(dbName: String,
        context: Context
): SupportSQLiteOpenHelper = SqliteOpenHelperFactory()
            .create(
                    SupportSQLiteOpenHelper.Configuration.builder(context)
                            .name(dbName)
                            .callback(AndroidSqliteDriver.Callback(Schema))
                            .build()
            )