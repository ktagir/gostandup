package ru.gostandup.tickets.platform.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.http.*
import ru.gostandup.tickets.BuildConfig
import ru.gostandup.tickets.model.auth.AuthHolder
import ru.gostandup.tickets.model.entities.AuthData
import ru.gostandup.tickets.model.entities.Event
import ru.gostandup.tickets.model.entities.Ticket
import ru.gostandup.tickets.model.entities.User
import ru.gostandup.tickets.model.json
import java.io.IOException
import java.util.concurrent.TimeUnit

@OptIn(ExperimentalSerializationApi::class)
actual class RestClient(
        private val authHolder: AuthHolder,
        interceptors: List<Interceptor>) {

    private val client: RetrofitRestClient

    init {
        val builder = OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)

        builder.addInterceptor(ApiHeadersInterceptor(authHolder))

        interceptors.forEach {
            builder.addInterceptor(it)
        }

        client = Retrofit.Builder()
                .client(builder.build())
                .baseUrl(if (BuildConfig.DEBUG) BASE_URL_DEV else BASE_URL)
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .build()
                .create(RetrofitRestClient::class.java)


    }

    actual suspend fun login(authData: AuthData): User {
        val user = client.login(authData)
        authHolder.accessToken = user.authKey
        return user
    }

    actual suspend fun getEvents(): List<Event> = client.getEvents()

    actual suspend fun getTickets(eventId: Long): Event = client.getTickets(eventId)

    actual suspend fun updateTicket(ticketCode: String): Ticket = client.updateTicket(ticketCode)

    actual suspend fun getTicket(ticketId: Long): Ticket = client.getTicket(ticketId)
}

interface RetrofitRestClient {


    @POST("auth")
    suspend fun login(@Body authData: AuthData): User

    @GET("events")
    suspend fun getEvents(): List<Event>

    @GET("events/{eventId}")
    suspend fun getTickets(@Path("eventId") eventId: Long): Event

    @PATCH("tickets/{ticketCode}")
    suspend fun updateTicket(@Path("ticketCode")ticketCode: String): Ticket

    @GET("tickets/{ticketId}")
    suspend fun getTicket(@Path("ticketId") ticketId: Long): Ticket
}

class ApiHeadersInterceptor(private val authHolder: AuthHolder) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
//        val authHeader = authHolder.authHeader
        val requestBuilder = request.newBuilder()
        if(!authHolder.accessToken.isNullOrBlank()) {
            val url = chain.request().url
                    .newBuilder()
                    .addQueryParameter("access-token", authHolder.accessToken)
                    .build()
            requestBuilder.url(url)
        }


//        if(request.header(authHeader) == null && authHolder.accessToken != null && authHolder.tokenType != null) {
//            requestBuilder.addHeader(authHeader, "${authHolder.tokenType} ${authHolder.accessToken}")
//        }
        return chain.proceed(requestBuilder.build())
    }
}