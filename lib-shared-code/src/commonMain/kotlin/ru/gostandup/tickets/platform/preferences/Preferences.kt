package ru.gostandup.tickets.platform.preferences

expect class Preferences{

    fun getString(key: String, defaultValue: String? = null): String?
    fun putString(key: String, value: String?)

    fun getInt(key: String,  defaultValue: Int = 0): Int
    fun putInt(key: String, value: Int)

    fun getLong(key: String,  defaultValue: Long = 0): Long
    fun putLong(key: String, value: Long)

    fun getFloat(key: String,  defaultValue: Float = 0f): Float
    fun putFloat(key: String, value: Float)

    fun getBoolean(key: String,  defaultValue: Boolean = false): Boolean
    fun putBoolean(key: String, value: Boolean)

    fun contains(key: String): Boolean

    fun remove(key: String)
    fun clear()

}