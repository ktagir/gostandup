package ru.gostandup.tickets.platform.network


import ru.gostandup.tickets.model.entities.AuthData
import ru.gostandup.tickets.model.entities.Event
import ru.gostandup.tickets.model.entities.Ticket
import ru.gostandup.tickets.model.entities.User

const val BASE_URL = "https://gostandup.ru/api/v1/"

const val BASE_URL_DEV = "https://dev.gostandup.ru:9443/api/v1/"

expect class RestClient {

    suspend fun login(authData: AuthData): User

    suspend fun getEvents(): List<Event>

    suspend fun getTickets(eventId: Long): Event

    suspend fun updateTicket(ticketCode: String): Ticket

    suspend fun getTicket(ticketId: Long): Ticket
}