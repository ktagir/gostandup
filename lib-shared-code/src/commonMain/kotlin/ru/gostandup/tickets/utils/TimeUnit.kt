package ru.gostandup.tickets.utils

enum class TimeUnit {

    NANOSECONDS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toNanos(sourceDuration)
        override fun toNanos(duration: Long): Long = duration
        override fun toMicros(duration: Long): Long = duration/(C1/C0)
        override fun toMillis(duration: Long): Long = duration/(C2/C0)
        override fun toSeconds(duration: Long): Long= duration/(C3/C0)
        override fun toMinutes(duration: Long): Long = duration/(C4/C0)
        override fun toHours(duration: Long): Long = duration/(C5/C0)
        override fun toDays(duration: Long): Long= duration/(C6/C0)
    },
    MICROSECONDS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toMicros(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C1/C0, MAX/(C1/C0))
        override fun toMicros(duration: Long): Long = duration
        override fun toMillis(duration: Long): Long = duration/(C2/C1)
        override fun toSeconds(duration: Long): Long = duration/(C3/C1)
        override fun toMinutes(duration: Long): Long = duration/(C4/C1)
        override fun toHours(duration: Long): Long = duration/(C5/C1)
        override fun toDays(duration: Long): Long = duration/(C6/C1)
    },
    MILLISECONDS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toMillis(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C2/C0, MAX/(C2/C0))
        override fun toMicros(duration: Long): Long = x(duration, C2/C1, MAX/(C2/C1))
        override fun toMillis(duration: Long): Long = duration
        override fun toSeconds(duration: Long): Long = duration/(C3/C2)
        override fun toMinutes(duration: Long): Long = duration/(C4/C2)
        override fun toHours(duration: Long): Long = duration/(C5/C2)
        override fun toDays(duration: Long): Long = duration/(C6/C2)
    },
    SECONDS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toSeconds(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C3/C0, MAX/(C3/C0))
        override fun toMicros(duration: Long): Long = x(duration, C3/C1, MAX/(C3/C1))
        override fun toMillis(duration: Long): Long = x(duration, C3/C2, MAX/(C3/C2))
        override fun toSeconds(duration: Long): Long = duration
        override fun toMinutes(duration: Long): Long = duration/(C4/C3)
        override fun toHours(duration: Long): Long = duration/(C5/C3)
        override fun toDays(duration: Long): Long = duration/(C6/C3)
    },
    MINUTES {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toMinutes(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C4/C0, MAX/(C4/C0))
        override fun toMicros(duration: Long): Long = x(duration, C4/C1, MAX/(C4/C1))
        override fun toMillis(duration: Long): Long = x(duration, C4/C2, MAX/(C4/C2))
        override fun toSeconds(duration: Long): Long = x(duration, C4/C3, MAX/(C4/C3))
        override fun toMinutes(duration: Long): Long = duration
        override fun toHours(duration: Long): Long = duration/(C5/C4)
        override fun toDays(duration: Long): Long = duration/(C6/C4)
    },
    HOURS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toHours(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C5/C0, MAX/(C5/C0))
        override fun toMicros(duration: Long): Long = x(duration, C5/C1, MAX/(C5/C1))
        override fun toMillis(duration: Long): Long = x(duration, C5/C2, MAX/(C5/C2))
        override fun toSeconds(duration: Long): Long = x(duration, C5/C3, MAX/(C5/C3))
        override fun toMinutes(duration: Long): Long = x(duration, C5/C4, MAX/(C5/C4))
        override fun toHours(duration: Long): Long = duration
        override fun toDays(duration: Long): Long = duration/(C6/C5)
    },
    DAYS {
        override fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long = sourceUnit.toDays(sourceDuration)
        override fun toNanos(duration: Long): Long = x(duration, C6/C0, MAX/(C6/C0))
        override fun toMicros(duration: Long): Long = x(duration, C6/C1, MAX/(C6/C1))
        override fun toMillis(duration: Long): Long = x(duration, C6/C2, MAX/(C6/C2))
        override fun toSeconds(duration: Long): Long = x(duration, C6/C3, MAX/(C6/C3))
        override fun toMinutes(duration: Long): Long = x(duration, C6/C4, MAX/(C6/C4))
        override fun toHours(duration: Long): Long = x(duration, C6/C5, MAX/(C6/C5))
        override fun toDays(duration: Long): Long = duration
    };

    companion object{
        const val C0 = 1L;
        const val  C1 = C0 * 1000L;
        const val  C2 = C1 * 1000L;
        const val  C3 = C2 * 1000L;
        const val  C4 = C3 * 60L;
        const val  C5 = C4 * 60L;
        const val  C6 = C5 * 24L;
        const val  MAX = Long.MAX_VALUE;

        fun x(d: Long, m: Long, over: Long): Long {
            if (d > over) return Long.MAX_VALUE;
            if (d < -over) return Long.MIN_VALUE;
            return d * m;
        }
    }

    abstract fun convert(sourceDuration: Long, sourceUnit: TimeUnit): Long

    abstract fun toNanos(duration: Long): Long

    abstract fun toMicros(duration: Long): Long

    abstract fun toMillis(duration: Long): Long

    abstract fun toSeconds(duration: Long): Long

    abstract fun toMinutes(duration: Long): Long

    abstract fun toHours(duration: Long): Long

    abstract fun toDays(duration: Long): Long

}