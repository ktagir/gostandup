package ru.gostandup.tickets.ui.control.statistics


import ru.gostandup.tickets.model.entities.StatisticsItem
import su.tagir.lib.mvp.MvpListPresenter
import su.tagir.lib.mvp.MvpListView

interface StatisticsContract {

    interface View: MvpListView<StatisticsItem>

    interface Presenter: MvpListPresenter<StatisticsItem, View>
}