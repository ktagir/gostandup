package ru.gostandup.tickets.ui.navigator

interface Router {

    fun navigateToLogin()
    fun navigateToEvents()
    fun navigateToControl(eventId: Long)
    fun pop()
    fun dismiss()
}