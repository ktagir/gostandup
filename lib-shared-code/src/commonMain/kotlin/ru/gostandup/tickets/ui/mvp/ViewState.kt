//package ru.gostandup.tickets.ui.mvp
//
//data class ViewState<out T>(
//        val status: Status,
//        val data: T? = null,
//        val throwable: Throwable? = null,
//        val hasNextPage: Boolean = true
//) {
//
//    val loading
//        get() = status == Status.LOADING
//
//    val loadingMore
//        get() = status == Status.LOADING_MORE
//
//    val refreshing
//        get() = status == Status.REFRESHING
//
//    val error
//        get() = status == Status.ERROR
//
//    val completed
//        get() = status == Status.SUCCESS
//
//
//    val isCanLoadMore: Boolean
//        get() = hasNextPage && status != Status.LOADING && status != Status.REFRESHING && status != Status.LOADING_MORE
//}