package ru.gostandup.tickets.ui.login

import kotlinx.coroutines.launch
import ru.gostandup.tickets.model.Repository
import ru.gostandup.tickets.ui.navigator.Router
import su.tagir.lib.mvp.MvpBasePresenter

class LoginPresenter(private val repository: Repository,
                     private val router: Router) : MvpBasePresenter<LoginContract.View>(), LoginContract.Presenter {

    override fun loginWith(email: String, password: String) {
        if (!checkValues(email, password)) {
            return
        }

       ifViewAttached({ v -> v.hideInputErrors() })
        showProgress(true)
        launch {
            repository.login(email, password)
            showProgress(false)
            router.navigateToEvents()
        }
    }

    private fun checkValues(email: String, password: String): Boolean {
        var validValues = true
        if (email.isBlank()) {
            ifViewAttached( { v -> v.showEmptyLoginError() })
            validValues = false
        }
        if (password.isBlank()) {
            ifViewAttached({ v -> v.showEmptyPasswordError() })
            validValues = false
        }
        return validValues
    }

}