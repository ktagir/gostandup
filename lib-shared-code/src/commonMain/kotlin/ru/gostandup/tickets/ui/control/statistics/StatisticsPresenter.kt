package ru.gostandup.tickets.ui.control.statistics

import co.touchlab.stately.concurrency.AtomicReference
import co.touchlab.stately.concurrency.value
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.gostandup.tickets.model.Repository
import ru.gostandup.tickets.model.entities.StatisticsItem
import su.tagir.lib.mvp.MvpBaseListPresenter
import su.tagir.lib.mvp.Status

class StatisticsPresenter(private val eventId: Long,
                          private val repository: Repository) : MvpBaseListPresenter<StatisticsItem, StatisticsContract.View>(), StatisticsContract.Presenter {

    private val loadJob = AtomicReference<Job?>(null)

    override fun loadData() {
        loadJob.value?.cancel()
        val job =
           repository.getStatistics(eventId)
                   .onEach { updateState(Status.SUCCESS, data = it) }
                   .launchIn(this)

        loadJob.value = job
    }
}