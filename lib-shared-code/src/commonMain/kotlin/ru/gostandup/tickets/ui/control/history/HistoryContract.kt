package ru.gostandup.tickets.ui.control.history

import ru.gostandup.tickets.data.entries.TicketFull
import ru.gostandup.tickets.data.entries.TicketWithSeat
import su.tagir.lib.mvp.MvpListPresenter
import su.tagir.lib.mvp.MvpListView

interface HistoryContract {

    interface View: MvpListView<TicketWithSeat> {

        fun showInfo(ticket: TicketFull)
    }

    interface Presenter: MvpListPresenter<TicketWithSeat, View> {
        fun showTicket(ticket: TicketWithSeat)
    }
}