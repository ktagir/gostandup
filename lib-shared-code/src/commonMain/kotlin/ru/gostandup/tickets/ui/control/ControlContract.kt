package ru.gostandup.tickets.ui.control

import ru.gostandup.tickets.data.entries.TicketFull
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView

interface ControlContract {
    interface View : MvpView {
        fun showEventTitle(title: String?)
        fun showPassedTicket(ticket: TicketFull)
        fun showTickedPassedSuccess()
        fun playSuccessTone()
        fun setCheckTicketProgressIndicator(b: Boolean)
        fun showAlreadyPassedError(ticket: TicketFull?)
        fun showCheckTicketError(error: Throwable)
        fun setLoadingTicketsIndicator(show: Boolean)
        fun showUpdateError()
    }

    interface Presenter : MvpPresenter<View> {
        fun onScan(code: String)
        fun loadTickets(eventId: Long)
        fun loadEvent(eventId: Long)
    }
}