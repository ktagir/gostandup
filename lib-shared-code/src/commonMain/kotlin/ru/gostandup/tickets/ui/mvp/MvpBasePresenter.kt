//package ru.gostandup.tickets.ui.mvp
//
//import co.touchlab.stately.concurrency.AtomicBoolean
//import co.touchlab.stately.concurrency.AtomicReference
//import co.touchlab.stately.concurrency.value
//import co.touchlab.stately.freeze
//import kotlinx.coroutines.*
//import ru.gostandup.tickets.platform.Logger
//import kotlin.coroutines.CoroutineContext
//
//
//abstract class MvpBasePresenter<V: MvpView>: MvpPresenter<V>, CoroutineScope {
//
//
//    private var view = AtomicReference<V?>(null)
//
//    private var presenterDestroyed = AtomicBoolean(false)
//
//    protected val job: Job = SupervisorJob()
//
//    protected open var exceptionHandler = CoroutineExceptionHandler { _, throwable ->
//        Logger.e(this::class.simpleName, throwable)
//        ifViewAttached(action = {v ->
//            v.showProgress(false)
//            v.showError(throwable)
//        })
//    }
//
//    override val coroutineContext: CoroutineContext
//        get() = job + Dispatchers.Main + exceptionHandler
//
//    protected fun ifViewAttached(action: (V) -> Unit, exceptionIfViewNotAttached: Boolean = false) {
//        if (view.value != null) {
//            action(view.value!!)
//        } else check(!exceptionIfViewNotAttached) { "No View attached to Presenter. Presenter destroyed = $presenterDestroyed" }
//    }
//
//    override fun attachView(view: V) {
//        this.view.value = view.freeze()
//        presenterDestroyed.value = false
//    }
//
//    override fun detachView() {
//        this.view.value = null
//    }
//
//    override fun destroy() {
//        job.cancel()
//        presenterDestroyed.value = true
//    }
//
//    protected fun showProgress(show: Boolean){
//        ifViewAttached({v -> v.showProgress(show)})
//    }
//}