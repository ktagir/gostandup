package ru.gostandup.tickets.ui.control

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import ru.gostandup.tickets.model.Repository
import su.tagir.lib.kmm.logger.Logger
import su.tagir.lib.mvp.MvpBasePresenter


class ControlPresenter(private val eventId: Long,
                       private val repository: Repository) : MvpBasePresenter<ControlContract.View>(), ControlContract.Presenter {

    private val scanExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Logger.e(this::class.simpleName, throwable)
        ifViewAttached({ v -> v.setCheckTicketProgressIndicator(false) })
        ifViewAttached({ v -> v.showCheckTicketError(throwable) })
    }

    override fun attachView(view: ControlContract.View) {
        super.attachView(view)
        loadEvent(eventId)
    }

    override fun onScan(code: String) {
        ifViewAttached({ v -> v.setCheckTicketProgressIndicator(true) })
        launch(scanExceptionHandler) {
            val ticket = repository.passTicket(eventId, code)
            ifViewAttached({ v ->
                v.setCheckTicketProgressIndicator(false)
                v.showPassedTicket(ticket)
            })
        }
    }

    override fun loadTickets(eventId: Long) {
        showProgress(true)
        launch {
            repository.refreshTickets(eventId)
            showProgress(false)
        }
    }

    override fun loadEvent(eventId: Long) {
        launch {
            val event = repository.getEvent(eventId)
            ifViewAttached({v -> v.showEventTitle(event?.title)})
        }
    }

}