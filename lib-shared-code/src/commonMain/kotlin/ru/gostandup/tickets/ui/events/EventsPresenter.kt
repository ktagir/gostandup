package ru.gostandup.tickets.ui.events

import co.touchlab.stately.concurrency.AtomicReference
import co.touchlab.stately.concurrency.value
import co.touchlab.stately.freeze
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import ru.gostandup.tickets.data.entries.EventEntry
import ru.gostandup.tickets.model.Repository
import ru.gostandup.tickets.model.Resource
import ru.gostandup.tickets.ui.navigator.Router
import su.tagir.lib.kmm.logger.Logger
import su.tagir.lib.mvp.MvpBaseListPresenter
import su.tagir.lib.mvp.Status

class EventsPresenter(private val repository: Repository,
                      private val router: Router) : MvpBaseListPresenter<EventItem, EventsContract.View>(), EventsContract.Presenter {

    private var loadJob = AtomicReference<Job?>(null)

    override fun showEvent(event: EventEntry) {
        router.navigateToControl(event.id)
    }

    override suspend fun updateBlock() {
        repository.refreshEvents()
    }

    override fun loadData() {
        loadJob.value?.cancel()
        val job =
                repository.events()
                        .map { resource ->
                            val data = resource.data?.map {
                                EventItem(it, 0)
                            }
                            Resource(resource.status, data, resource.throwable)
                        }
                        .onEach { res ->
                            when {
                                res.isLoading -> updateState(Status.LOADING, data = res.data)
                                res.isSuccess -> updateState(Status.SUCCESS, data = res.data)
                                res.isError -> {
                                    updateState(Status.ERROR, data = res.data, error = res.throwable)
                                    Logger.e(EventsPresenter::class.simpleName, res.throwable)
                                }
                            }
                        }
                        .catch { e ->
                            Logger.e(EventsPresenter::class.simpleName, e)
                            updateState(Status.ERROR, error = e)

                        }
                        .launchIn(this)
        loadJob.value = job.freeze()
    }
}
