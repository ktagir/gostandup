package ru.gostandup.tickets.ui.events

import ru.gostandup.tickets.data.entries.EventEntry
import su.tagir.lib.mvp.MvpListPresenter
import su.tagir.lib.mvp.MvpListView

interface EventsContract {

    interface View : MvpListView<EventItem>

    interface Presenter : MvpListPresenter<EventItem, View> {
        fun showEvent(event: EventEntry)
    }
}