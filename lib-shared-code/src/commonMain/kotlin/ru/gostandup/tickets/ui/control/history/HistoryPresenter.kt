package ru.gostandup.tickets.ui.control.history

import co.touchlab.stately.concurrency.AtomicReference
import co.touchlab.stately.concurrency.value
import co.touchlab.stately.freeze
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.gostandup.tickets.data.entries.TicketWithSeat
import ru.gostandup.tickets.model.Repository
import su.tagir.lib.kmm.logger.Logger
import su.tagir.lib.mvp.MvpBaseListPresenter
import su.tagir.lib.mvp.Status

class HistoryPresenter(private val eventId: Long,
                       private val repository: Repository) : MvpBaseListPresenter<TicketWithSeat, HistoryContract.View>(), HistoryContract.Presenter {

    private val loadJob = AtomicReference<Job?>(null)

    override fun loadData() {
        loadJob.value?.cancel()
        val job =
                repository.getTickets(eventId)
                        .onEach { res ->
                            when {
                                res.isLoading -> updateState(Status.LOADING, data = res.data)
                                res.isSuccess -> updateState(Status.SUCCESS, data = res.data)
                                res.isError -> {
                                    updateState(Status.ERROR, data = res.data, error = res.throwable)
                                    Logger.e(HistoryPresenter::class.simpleName, res.throwable)
                                }
                            }
                        }
                        .catch { e ->
                            Logger.e(HistoryPresenter::class.simpleName, e)
                            updateState(Status.ERROR, error = e)
                        }
                        .launchIn(this)

        loadJob.value = job.freeze()
    }

    override fun showTicket(ticket: TicketWithSeat) {
        launch {
            val t = repository.getTicket(eventId, ticket.code ?: "")
            ifViewAttached({ v -> v.showInfo(t) })
        }
    }

    override suspend fun updateBlock() {
        repository.refreshTickets(eventId)
    }
}