package ru.gostandup.tickets.ui.events

import ru.gostandup.tickets.data.entries.EventEntry

data class EventItem(val event: EventEntry, val ticketsCount: Int)