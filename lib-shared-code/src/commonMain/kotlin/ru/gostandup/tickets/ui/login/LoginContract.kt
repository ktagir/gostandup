package ru.gostandup.tickets.ui.login

import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView

/**
 * Created by ishmukhametov on 07.04.17.
 */
interface LoginContract {
    interface View : MvpView {
        fun showEmptyLoginError()
        fun showEmptyPasswordError()
        fun hideInputErrors()
    }

    interface Presenter : MvpPresenter<View> {
        fun loginWith(email: String, password: String)
    }
}