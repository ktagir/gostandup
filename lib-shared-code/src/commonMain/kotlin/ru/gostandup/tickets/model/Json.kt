package ru.gostandup.tickets.model

import kotlinx.serialization.json.Json


val json = Json {
    ignoreUnknownKeys = true
    isLenient = true
}