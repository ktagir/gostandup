package ru.gostandup.tickets.model

enum class Status{
    LOADING,
    SUCCESS,
    ERROR,
}

data class Resource<out T> (val status: Status, val data: T? = null, val throwable: Throwable? = null, val progress: Int = 0) {

    val isLoading: Boolean
        get() = status === Status.LOADING

    val isError: Boolean
        get() = status === Status.ERROR

    val isSuccess: Boolean
        get() = status === Status.SUCCESS


    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null, 100)
        }

        fun <T> error(error: Throwable?, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, error, 0)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null, 0)
        }
    }
}
