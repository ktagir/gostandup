package ru.gostandup.tickets.model

import ru.gostandup.tickets.data.entries.TicketFull
import ru.gostandup.tickets.model.entities.Ticket


class Unauthorized : Throwable()
class HttpException(val code: Int? = 500, private val errorMessage: String?) : Exception(errorMessage)


class TicketError(val type: Type,
                  message: String?,
                  val ticket: Ticket? = null,
                  val ticketFull: TicketFull? = null) : Throwable(message) {

    enum class Type {
        ALREADY_PASSED, NOT_FOUND
    }

    companion object {

        fun alreadyPassedTicket(ticket: TicketFull?): TicketError {
            return TicketError(Type.ALREADY_PASSED, "Someone has already passed with the ticket.", ticketFull = ticket)
        }

        fun notFound(): TicketError {
            return TicketError(Type.NOT_FOUND, "The ticket was not found.")
        }
    }
}