package ru.gostandup.tickets.model

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOneOrDefault
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import ru.gostandup.tickets.GoStandUpDb
import ru.gostandup.tickets.data.entries.EventEntry
import ru.gostandup.tickets.data.entries.TicketFull
import ru.gostandup.tickets.data.entries.TicketWithSeat
import ru.gostandup.tickets.model.entities.*
import ru.gostandup.tickets.platform.currentTimeMillis
import ru.gostandup.tickets.platform.network.RestClient
import ru.gostandup.tickets.platform.preferences.Preferences
import ru.gostandup.tickets.secretNumbers
import ru.gostandup.tickets.utils.TimeUnit
import su.tagir.lib.kmm.logger.Logger

interface Repository {

    var user: User?
    suspend fun login(email: String, password: String): User
    suspend fun deleteAllEvents()
    suspend fun deleteAllTickets()
    suspend fun deleteTickets(eventId: Long)
    suspend fun getTicket(eventId: Long, code: String): TicketFull
    fun getTickets(eventId: Long): Flow<Resource<List<TicketWithSeat>>>
    suspend fun refreshTickets(eventId: Long)
    suspend fun getEvent(eventId: Long): EventEntry?
    suspend fun passTicket(eventId: Long, code: String): TicketFull
    fun events(): Flow<Resource<List<EventEntry>>>
    suspend fun getTicketsCountForEvent(eventId: Long): Int?
    suspend fun refreshEvents()
    fun getStatistics(eventId: Long): Flow<List<StatisticsItem>>


    class Impl(private val database: GoStandUpDb,
               private val restClient: RestClient,
               private val preferences: Preferences) : Repository {

        private val dispatcher: CoroutineDispatcher = Dispatchers.Default

        override var user: User?
            get() = preferences.getString("user")?.let{ json.decodeFromString(it) }
            set(value) {
                if(value != null) {
                    preferences.putString("user", json.encodeToString(value))
                }else{
                    preferences.remove("user")
                }
            }

        override suspend fun login(email: String, password: String): User = restClient.login(AuthData(email, password)).also { user = it }

        override suspend fun deleteAllEvents() {
            database.eventQueries.deleteAll()
        }

        override suspend fun deleteAllTickets() {
            database.ticketQueries.deleteAll()
        }

        override suspend fun deleteTickets(eventId: Long) {
            database.ticketQueries.deleteByEventId(eventId)
        }

        override suspend fun getTicket(eventId: Long, code: String): TicketFull {
            return database.ticketQueries.ticketFull(eventId, code).executeAsOne()
        }

        override fun getTickets(eventId: Long): Flow<Resource<List<TicketWithSeat>>> =
                networkBoundResource(query = {
                    database.ticketQueries
                            .ticketWithSeat(eventId)
                            .asFlow()
                            .mapToList(dispatcher)
                },
                        fetch = {
                            restClient.getTickets(eventId)
                        },
                        saveFetchResult = { event ->
                            Logger.d(Repository::class.simpleName, "event: $event")
                            event.tickets?.let { tickets ->
                                database.transaction {
                                    saveTickets(eventId, tickets, database)
                                }
                            }
                        },
                        shouldFetch = {
                            if (it.isEmpty()) {
                                true
                            } else {
                                val timeStamp = database.ticketQueries.oldestTimeStamp()
                                        .executeAsOneOrNull()?.toLong()
                                        ?.times(1000) ?: Long.MIN_VALUE

                                val should = (currentTimeMillis() - timeStamp >= TimeUnit.MINUTES.toMillis(5))
                                should
                            }
                        }
                )


        override suspend fun refreshTickets(eventId: Long) {
            val event = restClient.getTickets(eventId)
            event.tickets?.let { tickets ->
                database.transaction {
                    saveTickets(eventId, tickets, database)
                }
            }
        }

        override suspend fun getEvent(eventId: Long): EventEntry? = database.eventQueries.findById(eventId).executeAsOneOrNull()

        override suspend fun passTicket(eventId: Long, code: String): TicketFull {
            val tickets = database.ticketQueries.ticketFull(eventId, code).executeAsList()
            if (tickets.isNotEmpty() && tickets[0].isUsed) {
                throw TicketError.alreadyPassedTicket(tickets[0])
            }
            try {
                val ticket = restClient.updateTicket(code)
                ticket.code?.let {
                    database.transaction {
                        val count = database.ticketQueries.countById(ticket.id).executeAsOneOrNull()
                                ?: 0
                        if (count == 0L) {
                            database.ticketQueries.insertOrReplace(ticket.id, eventId, ticket.code, ticket.isAdmission != 0, true, ticket.passTime, true)
                            ticket.order?.let {
                                saveOrder(ticket.order, ticket.id)
                            }
                            ticket.seats?.let { seats ->
                                saveSeats(json.decodeFromString(ListSerializer(Seat.serializer()), seats), ticket.id)
                            }
                        } else {
                            database.ticketQueries.passByThisDevice(ticket.isAdmission != 0, true, ticket.passTime, eventId, ticket.code)
                        }
                    }
                }
            } catch (e: Throwable) {
                if (e is TicketError) {
                    if (e.type == TicketError.Type.ALREADY_PASSED) {
                        throw saveAndReturnAlreadyPassedError(e.ticket, eventId)
                    } else if (e.type == TicketError.Type.NOT_FOUND) {
                        database.ticketQueries.deleteByEventIdAndCode(eventId, code)
                        throw e
                    }
                }
                throw e
            }
            return database.ticketQueries.ticketFull(eventId, code).executeAsOne()

        }

        override fun events(): Flow<Resource<List<EventEntry>>> =
                networkBoundResource(query = {
                    database.eventQueries.findAll()
                            .asFlow()
                            .mapToList(dispatcher)
                },
                        fetch = { restClient.getEvents() },
                        shouldFetch = {
                            if (it.isEmpty()) {
                                true
                            } else {
                                val timeStamp = database.eventQueries.oldestTimeStamp()
                                        .executeAsOneOrNull()?.toLong()
                                        ?.times(1000) ?: Long.MIN_VALUE

                                val should = (currentTimeMillis() - timeStamp >= TimeUnit.MINUTES.toMillis(5))
                                should
                            }
                        },
                        saveFetchResult = {
                            saveEvents(it)
                        }
                )


        override suspend fun getTicketsCountForEvent(eventId: Long): Int? =
                database.ticketQueries.countByEventId(eventId).executeAsOneOrNull()?.toInt() ?: 0

        override suspend fun refreshEvents() {
            val events = restClient.getEvents()
            saveEvents(events)
        }


        override fun getStatistics(eventId: Long): Flow<List<StatisticsItem>> {
            val passedByThis = database.ticketQueries.countPassedByThisDevice(eventId).asFlow().mapToOneOrDefault(0, dispatcher)
            val totalPassed = database.ticketQueries.countTotalPassed(eventId).asFlow().mapToOneOrDefault(0, dispatcher)
            val sold = database.ticketQueries.countByEventId(eventId).asFlow().mapToOneOrDefault(0, dispatcher)
            return combine(passedByThis, totalPassed, sold) { p, t, s ->
                listOf(StatisticsItem(StatisticsItemType.PASSED_BY_DEVICE, p),
                        StatisticsItem(StatisticsItemType.PASSED_TOTAL, t),
                        StatisticsItem(StatisticsItemType.SOLD_TOTAL, s))
            }
        }

        fun saveAndReturnAlreadyPassedError(ticket: Ticket?, eventId: Long): TicketError {
            var ticketFull: TicketFull? = null
            ticket?.let {
                database.transaction {
                    try {
                        val count = database.ticketQueries.countById(ticket.id).executeAsOneOrNull()
                                ?: 0
                        if (count == 0L) {
                            database.ticketQueries.insertOrReplace(ticket.id, eventId, ticket.code, ticket.isAdmission != 0, ticket.isUsed != 0, ticket.passTime, false)
                            ticket.order?.let {
                                saveOrder(ticket.order, ticket.id)
                            }
                            ticket.seats?.let { seats ->
                                saveSeats(json.decodeFromString(ListSerializer(Seat.serializer()), seats), ticket.id)
                            }
                        } else {
                            database.ticketQueries.update(ticket.isAdmission != 0, true, ticket.passTime, ticket.id)
                        }
                        ticketFull = database.ticketQueries.ticketFull(eventId, ticket.code).executeAsOneOrNull()
                    } catch (e: Exception) {
                        Logger.e("Repository", e)
                    }
                }
            }
            return TicketError.alreadyPassedTicket(ticketFull)
        }

        fun saveEvents(events: List<Event>) {
            database.transaction {
                database.eventQueries.deleteAll()
                events.forEach { event ->
                    database.eventQueries.insert(event.id, event.title, "", event.date, event.place, event.city)
                }
            }
        }

        fun saveTickets(eventId: Long, tickets: List<Ticket>?, database: GoStandUpDb) {
            tickets?.filter { t -> t.code != null }?.let { list ->
                val ids = list.map { t -> t.id }
                if (ids.isEmpty()) {
                    database.ticketQueries.deleteByEventId(eventId)
                } else {
                    database.ticketQueries.deleteNotIncludedByEventId(eventId, ids)
                }
                list.forEach { ticket ->
                    saveTicket(ticket, eventId, database)
                }
            }
        }

        fun saveTicket(ticket: Ticket, eventId: Long, database: GoStandUpDb) {
            if ((database.ticketQueries.countById(ticket.id).executeAsOneOrNull() ?: 0) == 0L) {
                database.ticketQueries.insertOrReplace(ticket.id, eventId, ticket.code, ticket.isAdmission != 0, ticket.isUsed != 0, ticket.passTime, false)
            } else {
                database.ticketQueries.update(ticket.isAdmission != 0, ticket.isUsed != 0, ticket.passTime, ticket.id)
            }
            ticket.order?.let { order ->
                saveOrder(order, ticket.id)
            }
            ticket.seats?.let { seats ->
                saveSeats(json.decodeFromString(ListSerializer(Seat.serializer()), seats), ticket.id)
            }
        }

        fun saveOrder(order: Order, ticketId: Long) {
            val fio = order.fio?.secretNumbers(3, 3)
            val phone = order.phone?.secretNumbers(6, 2)
            if ((database.orderQueries.countById(order.id, ticketId).executeAsOneOrNull()
                            ?: 0) == 0L) {
                database.orderQueries.insertOrReplace(order.id, fio, phone, "***", order.createdAt, order.status, ticketId)
            } else {
                database.orderQueries.update(fio, phone, "***", order.createdAt, order.status, order.id, ticketId)
            }
        }

        fun saveSeats(seats: List<Seat>, ticketId: Long) {
            seats.forEach { s ->
                if ((database.seatQueries.countById(ticketId).executeAsOneOrNull() ?: 0) == 0L) {
                    database.seatQueries.insert(s.col, s.row, s.price, s.singleRow, s.status, s.title, ticketId)
                } else {
                    database.seatQueries.update(s.col, s.row, s.price, s.singleRow, s.status, s.title, ticketId)
                }
            }
        }


    }
}