package ru.gostandup.tickets.model.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.gostandup.tickets.secretNumbers


@Serializable
data class AuthData(val email: String,
                    val password: String)

@Serializable
data class User(@SerialName("auth_key") val authKey: String = "",
                @SerialName("firstname") val firstName: String = "",
                @SerialName("lastname") val lastName: String = "",
                @SerialName("email") val email: String = "")

@Serializable
data class Event(val id: Long,
                 val date: String? = null,
                 val time: String? = null,
                 val title: String? = null,
                 val place: String? = null,
                 val city: String? = null,
                 val tickets: List<Ticket>? = null)

@Serializable
data class Order(
        val id: Long,
        val fio: String? = null,
        val phone: String? = null,
        val email: String? = null,

        @SerialName("created_at")
        val createdAt: Long = 0,
        val status: Int = 0)

@Serializable
data class Seat(@SerialName("object_id") val id: String,
                @SerialName("c") val col: String? = null,
                @SerialName("r") val row: String? = null,
                @SerialName("p") val price: String? = null,
                @SerialName("object_title") val title: String? = null,
                @SerialName("isSingleRow") val singleRow: Boolean? = null,
                @SerialName("s") val status: String? = null) {

    companion object {
        const val SOLD = 0
        const val PASSED = 1
        const val EXIT = 2
    }
}

@Serializable
data class Ticket(
        @SerialName("id") val id: Long,
        @SerialName("code") val code: String? = null,
        @SerialName("seats") val seats: String? = null,
        @SerialName("is_admission") val isAdmission: Int = 0,
        @SerialName("is_used") val isUsed: Int = 0,
        @SerialName("used_time") val passTime: Long? = null,
        @SerialName("order") val order: Order? = null) {


    fun secretNumber(): String {
        return order?.phone?.secretNumbers(4, 4) ?: ""
    }

    val isEmpty: Boolean
        get() = id == -1L


    companion object {

        val EMPTY = Ticket(-1, null, null, 0, 0, 0, null)


//        fun fromErrorResponse(response: Response): Ticket? {
//            return if (response.errorBody() == null) {
//                null
//            } else try {
//                val json: String = response.errorBody().string()
//                val gson = Gson()
//                gson.fromJson(json, Ticket::class.java)
//            } catch (e: IOException) {
//                Timber.e(e)
//                null
//            } catch (e: JsonSyntaxException) {
//                Timber.e(e)
//                null
//            }
//        }
    }
}

@Serializable
data class TicketWithSeatList(
        @SerialName("id") val id: Long,
        @SerialName("code") val code: String?,
        @SerialName("seats") val seats: List<Seat>?,
        @SerialName("is_admission") val isAdmission: Boolean,
        @SerialName("is_used") val isUsed: Boolean,
        @SerialName("used_time") val passTime: Long,
        @SerialName("order") val order: Order?) {

    fun secretNumber(): String {
        return order?.phone?.secretNumbers(4, 4) ?: ""
    }
}

data class SeatTicket(val seat: Seat, val ticket: Ticket)

data class Statistics(val eventId: Int,
                      val currentDeviceControl: Int,
                      val totalControl: Int,
                      val total: Int,
                      val sold: Int,
                      val exit: Int)

data class StatisticsItem(val type: StatisticsItemType, val count: Long)

enum class StatisticsItemType{
    PASSED_BY_DEVICE,
    PASSED_TOTAL,
    SOLD_TOTAL
}
