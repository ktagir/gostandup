package ru.gostandup.tickets.model.auth

import ru.gostandup.tickets.platform.preferences.Preferences

class GoStandUpAuthHolder(private val prefs: Preferences) : AuthHolder {

    override var authHeader: String = "Authorization"

    private var sessionListener: SessionListener? = null

    override var accessToken: String?
        get() = prefs.getString(KEY_ACCESS_TOKEN, null)
        set(value) = prefs.putString(KEY_ACCESS_TOKEN, value)

    override var tokenType: String?
        get() = prefs.getString(KEY_TOKEN_TYPE, null)
        set(value) = prefs.putString(KEY_TOKEN_TYPE, value)

    override var refreshToken: String?
        get() = prefs.getString(KEY_REFRESH_TOKEN, null)
        set(value) = prefs.putString(KEY_REFRESH_TOKEN, value)

    override var expiresIn: Long
        get() = prefs.getLong(KEY_TOKEN_EXPIRES_IN, 0)
        set(value) {
            prefs.putLong(KEY_TOKEN_EXPIRES_IN, value)
        }

    override fun clear() {
        prefs.remove(KEY_ACCESS_TOKEN)
        prefs.remove(KEY_TOKEN_TYPE)
        prefs.remove(KEY_REFRESH_TOKEN)
        prefs.remove(KEY_TOKEN_EXPIRES_IN)

    }

    override fun refresh() {
        clear()
        sessionListener?.sessionExpired()
    }

    override fun subscribeToSessionExpired(sessionListener: SessionListener) {
        this.sessionListener = sessionListener
    }

    companion object {
        private const val KEY_ACCESS_TOKEN = "token"
        private const val KEY_TOKEN_TYPE = "token_type"
        private const val KEY_REFRESH_TOKEN = "refresh_token"
        private const val KEY_TOKEN_EXPIRES_IN = "token_expires_in"
    }
}