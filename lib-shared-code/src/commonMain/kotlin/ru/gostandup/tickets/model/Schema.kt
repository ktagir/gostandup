package ru.gostandup.tickets.model

import com.squareup.sqldelight.db.SqlDriver
import ru.gostandup.tickets.GoStandUpDb

fun createQueryWrapper(driver: SqlDriver): GoStandUpDb {
    return GoStandUpDb(driver = driver)
}