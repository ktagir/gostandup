package ru.gostandup.tickets.model.auth


interface AuthHolder {
    var authHeader: String
    var accessToken: String?
    var tokenType: String?
    var refreshToken: String?
    var expiresIn: Long

    fun clear()
    fun refresh()
    fun subscribeToSessionExpired(sessionListener: SessionListener)
}

