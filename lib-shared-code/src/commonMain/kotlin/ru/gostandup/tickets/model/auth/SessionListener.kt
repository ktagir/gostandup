package ru.gostandup.tickets.model.auth

interface SessionListener {

    fun sessionExpired()
}