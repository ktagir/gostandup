package ru.gostandup.tickets.model.db

import com.squareup.sqldelight.db.SqlDriver
import ru.gostandup.tickets.GoStandUpDb
import ru.gostandup.tickets.model.createQueryWrapper

object Schema : SqlDriver.Schema by GoStandUpDb.Schema{

    override fun create(driver: SqlDriver) {
        GoStandUpDb.Schema.create(driver)
        createQueryWrapper(driver).apply {
            eventQueries.onInsertTrigger()
            eventQueries.onUpdateTrigger()
            ticketQueries.onInsertTrigger()
            ticketQueries.onUpdateTrigger()
            orderQueries.onInsertTrigger()
            orderQueries.onUpdateTrigger()
            updateInfoEntryQueries.onInsertTrigger()
            updateInfoEntryQueries.onUpdateTrigger()
            seatQueries.onInsertTrigger()
            seatQueries.onUpdateTrigger()
        }
    }
}