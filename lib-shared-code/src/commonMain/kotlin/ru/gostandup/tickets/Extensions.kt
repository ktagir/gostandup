package ru.gostandup.tickets

infix fun <T> T?.guard(block: () -> Nothing): T{
    return this ?: block()
}

fun String.secretNumbers(hideCount: Int, saveOnEnd: Int): String {
    var hiddenCount = 0
    var savedEnd = 0
    val str = CharArray(length)
    for (i in length - 1 downTo 0) {
        val ch = this[i]
        if (ch.isDigit()) {
            if (savedEnd == saveOnEnd && hiddenCount != hideCount) {
                str[i] = ('*')
                hiddenCount++
            } else {
                str[i] = ch
                savedEnd++
            }
        } else {
            str[i] = ch
        }
    }
    return str.concatToString()
}


val digits = setOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

fun Char.isDigit(): Boolean{
    return digits.contains(this)
}