package su.tagir.lib.android.mvp.delegate

import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView


interface MvpDelegateCallback<V : MvpView, P : MvpPresenter<V>> {

    fun createPresenter(): P

    var presenter: P

    val mvpView: V
}