package su.tagir.lib.android.mvp

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import su.tagir.lib.android.mvp.delegate.ActivityMvpDelegate
import su.tagir.lib.android.mvp.delegate.ActivityMvpDelegateImpl
import su.tagir.lib.android.mvp.delegate.MvpDelegateCallback
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView


/**
 * An Activity that uses a [MvpPresenter] to implement a Model-View-Presenter
 * architecture.
 *
 * @author Hannes Dorfmann
 * @since 1.0.0
 */
abstract class MvpActivity<V : MvpView, P : MvpPresenter<V>> : AppCompatActivity, MvpView, MvpDelegateCallback<V, P> {

    constructor(): super()

    constructor(@LayoutRes layoutId: Int): super(layoutId)

    private var delegate: ActivityMvpDelegate<V, P>? = null

    override lateinit var presenter: P

    @Suppress("UNCHECKED_CAST")
    override val mvpView: V
        get() = this as V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMvpDelegate().onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        getMvpDelegate().onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        getMvpDelegate().onSaveInstanceState(outState)
    }

    override fun onPause() {
        super.onPause()
        getMvpDelegate().onPause()
    }

    override fun onResume() {
        super.onResume()
        getMvpDelegate().onResume()
    }

    override fun onStart() {
        super.onStart()
        getMvpDelegate().onStart()
    }

    override fun onStop() {
        super.onStop()
        getMvpDelegate().onStop()
    }

    override fun onRestart() {
        super.onRestart()
        getMvpDelegate().onRestart()
    }

    override fun onContentChanged() {
        super.onContentChanged()
        getMvpDelegate().onContentChanged()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        getMvpDelegate().onPostCreate(savedInstanceState)
    }

    /**
     * Instantiate a presenter instance
     *
     * @return The [MvpPresenter] for this view
     */
    abstract override fun createPresenter(): P

    /**
     * Get the mvp delegate. This is internally used for creating presenter, attaching and detaching
     * view from presenter.
     *
     *
     * **Please note that only one instance of mvp delegate should be used per Activity
     * instance**.
     *
     *
     *
     *
     * Only override this method if you really know what you are doing.
     *
     *
     * @return [ActivityMvpDelegateImpl]
     */
    private fun getMvpDelegate(): ActivityMvpDelegate<V, P> {
        if (delegate == null) {
            delegate = ActivityMvpDelegateImpl(this, this, true)
        }
        return delegate!!
    }



}