package su.tagir.lib.android.mvp

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import su.tagir.lib.android.mvp.delegate.FragmentMvpDelegate
import su.tagir.lib.android.mvp.delegate.FragmentMvpDelegateImpl
import su.tagir.lib.android.mvp.delegate.MvpDelegateCallback
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView

abstract class MvpDialogFragment <V: MvpView, P: MvpPresenter<V>>: DialogFragment(),
        MvpDelegateCallback<V, P>, MvpView {

    override lateinit var presenter: P

    @Suppress("UNCHECKED_CAST")
    override val mvpView: V
        get() = this as V

    private var delegate: FragmentMvpDelegate<V, P>? = null

    protected open fun getMvpDelegate(): FragmentMvpDelegate<V, P> {
        if (delegate == null){
            delegate = FragmentMvpDelegateImpl(this, this, keepPresenterInstanceDuringScreenOrientationChanges = true, keepPresenterOnBackstack = true)
        }
        return delegate!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getMvpDelegate().onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        getMvpDelegate().onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMvpDelegate().onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getMvpDelegate().onDestroyView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        getMvpDelegate().onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        getMvpDelegate().onDetach()
    }

    override fun onStart() {
        super.onStart()
        getMvpDelegate().onStart()
    }

    override fun onStop() {
        super.onStop()
        getMvpDelegate().onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        getMvpDelegate().onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        getMvpDelegate().onResume()
    }

    override fun onPause() {
        super.onPause()
        getMvpDelegate().onPause()
    }


    abstract override fun createPresenter(): P

    protected fun showToast(message:String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}