package su.tagir.lib.android.mvp

import androidx.annotation.MainThread
import androidx.recyclerview.widget.*


abstract class BindingListAdapter<T> : RecyclerView.Adapter<BindingViewHolder<T>>() {

    private val listUpdateCallback = object : ListUpdateCallback {
        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemRangeChanged(position, count, payload)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

    }

    private val diffCallback: DiffUtil.ItemCallback<T> = object : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
            this@BindingListAdapter.areItemsTheSame(oldItem, newItem)


        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
            this@BindingListAdapter.areContentsTheSame(oldItem, newItem)

    }

    private val differ: AsyncListDiffer<T> = AsyncListDiffer<T>(listUpdateCallback, AsyncDifferConfig.Builder<T>(diffCallback).build())

    override fun getItemCount() = differ.currentList.size

    override fun onBindViewHolder(holder: BindingViewHolder<T>, position: Int) {
        holder.bind(items[position])
    }

    protected abstract fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    protected abstract fun areContentsTheSame(oldItem: T, newItem: T): Boolean

    fun addListener(listener: AsyncListDiffer.ListListener<T>?) {
        differ.addListListener(listener!!)
    }

    fun removeListener(listener: AsyncListDiffer.ListListener<T>?) {
        differ.removeListListener(listener!!)
    }


    @MainThread
    fun replace(update: List<T>?) {
        differ.submitList(update)
    }

    val items: List<T>
        get() = differ.currentList
}