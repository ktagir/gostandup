/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package su.tagir.lib.android.mvp.delegate

import android.app.Activity
import android.os.Bundle
import su.tagir.lib.android.mvp.presentermanager.PresenterManager
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView
import timber.log.Timber
import java.util.*

/**
 * The concrete implementation of []
 *
 * @param <V> The type of [MvpView]
 * @param <P> The type of [MvpPresenter]
 * @author Hannes Dorfmann
 * @see ActivityMvpDelegate
 *
 * @since 1.1.0
</P></V> */
class ActivityMvpDelegateImpl<V : MvpView, P : MvpPresenter<V>>(private val activity: Activity,
                                                                private val delegateCallback: MvpDelegateCallback<V, P>,
                                                                private val keepPresenterInstance: Boolean) :
        ActivityMvpDelegate<V, P> {

    protected var viewId: String? = null

    /**
     * Generates the unique (mosby internal) view id and calls [ ][MvpDelegateCallback.createPresenter]
     * to create a new presenter instance
     *
     * @return The new created presenter instance
     */
    private fun createViewIdAndCreatePresenter(): P {
        val presenter = delegateCallback.createPresenter()
        if (keepPresenterInstance) {
            viewId = UUID.randomUUID().toString()
            PresenterManager.putPresenter(activity, viewId!!, presenter)
        }
        return presenter
    }

    override fun onCreate(bundle: Bundle?) {
        var presenter: P?
        if (bundle != null && keepPresenterInstance) {
            viewId = bundle.getString(KEY_MOSBY_VIEW_ID)
            presenter = if(viewId != null) PresenterManager.getPresenter(activity, viewId!!) else null
            if (DEBUG) {
                Timber.d(
                        "%s%s", "MosbyView ID = $viewId for MvpView: ", delegateCallback.mvpView)
            }
            if (viewId != null
                    && presenter != null) {
                //
                // Presenter restored from cache
                //
                if (DEBUG) {
                    Timber.d(
                            "%s%s", "Reused presenter $presenter for view ", delegateCallback.mvpView)
                }
            } else {
                //
                // No presenter found in cache, most likely caused by process death
                //
                presenter = createViewIdAndCreatePresenter()
                if (DEBUG) {
                    Timber.d("No presenter found although view Id was here: $viewId. Most likely this was caused by a process death. New Presenter created $presenter for view $mvpView")
                }
            }
        } else {
            //
            // Activity starting first time, so create a new presenter
            //
            presenter = createViewIdAndCreatePresenter()
            if (DEBUG) {
                Timber.d("New presenter $presenter for view $mvpView")
            }
        }
        delegateCallback.presenter = presenter
        presenter.attachView(mvpView)
        if (DEBUG) {
            Timber.d("View$mvpView attached to Presenter $presenter")
        }
    }

    private val presenter: P
        get() = delegateCallback.presenter

    private val mvpView: V
        get() = delegateCallback.mvpView

    override fun onDestroy() {
        val retainPresenterInstance = retainPresenterInstance(keepPresenterInstance, activity)
        presenter.detachView()
        if (!retainPresenterInstance) {
            presenter.destroy()
        }
        if (!retainPresenterInstance && viewId != null) {
            PresenterManager.remove(activity, viewId!!)
        }
        if (DEBUG) {
            if (retainPresenterInstance) {
                Timber.d("View $mvpView destroyed temporarily. View detached from presenter $presenter")
            } else {
                Timber.d( "View $mvpView destroyed permanently. View detached permanently from presenter $presenter")
            }
        }
    }

    override fun onPause() {}
    override fun onResume() {}
    override fun onStart() {}
    override fun onStop() {}
    override fun onRestart() {}
    override fun onContentChanged() {}
    override fun onSaveInstanceState(outState: Bundle?) {
        if (keepPresenterInstance && outState != null) {
            outState.putString(KEY_MOSBY_VIEW_ID, viewId)
            if (DEBUG) {
                Timber.d("Saving MosbyViewId into Bundle. ViewId: $viewId for view $mvpView")
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {}

    companion object {
        private const val KEY_MOSBY_VIEW_ID = "com.hannesdorfmann.mosby3.activity.mvp.id"
        var DEBUG = false

        /**
         * Determines whether or not a Presenter Instance should be kept
         *
         * @param keepPresenterInstance true, if the delegate has enabled keep
         */
        fun retainPresenterInstance(keepPresenterInstance: Boolean, activity: Activity): Boolean {
            return keepPresenterInstance && (activity.isChangingConfigurations
                    || !activity.isFinishing)
        }
    }
}