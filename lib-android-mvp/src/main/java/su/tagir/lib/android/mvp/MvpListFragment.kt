package su.tagir.lib.android.mvp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import su.tagir.lib.mvp.MvpListPresenter
import su.tagir.lib.mvp.MvpListView
import su.tagir.lib.mvp.ViewState

abstract class MvpListFragment<M, V : MvpListView<M>, P : MvpListPresenter<M, V>> : MvpFragment<V, P>(), MvpListView<M> {

    protected var adapter by autoCleared<BindingListAdapter<M>>()

    protected var list: RecyclerView? = null
    protected var refreshLayout: SwipeRefreshLayout? = null
    private var btnRetry: Button? = null
    private var textEmpty: TextView? = null
    private var textError: TextView? = null
    private var progress: ProgressBar? = null
    private var loadMoreProgress: ProgressBar? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_list, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list = view.findViewById(R.id.list)
        refreshLayout = view.findViewById(R.id.refresh_layout)
        btnRetry = view.findViewById(R.id.btn_retry)
        textEmpty = view.findViewById(R.id.text_empty)
        textError = view.findViewById(R.id.text_error)
        progress = view.findViewById(R.id.progress)
        loadMoreProgress = view.findViewById(R.id.load_more_progress)

        btnRetry?.setOnClickListener { presenter.loadData() }

        initList()
        presenter.loadData()
    }

    private fun initList() {
        adapter = createAdapter()
        list?.adapter = adapter
        refreshLayout?.setOnRefreshListener { presenter.update() }

        list?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()

                if (lastPosition == adapter.itemCount - 1) {
                    presenter.loadMore(lastIndex = lastPosition)
                }
            }
        })
    }

    override fun updateState(viewState: ViewState<List<M>>) {
        showHideViews(viewState)
        viewState.data?.let { data ->
            adapter.replace(data)
        }
    }

    protected open fun showHideViews(viewState: ViewState<List<M>>) {
        val isEmpty = viewState.data.isNullOrEmpty()
        progress?.visibility = if (viewState.loading && isEmpty) View.VISIBLE else View.GONE
        textEmpty?.visibility =
                if (viewState.completed && isEmpty) View.VISIBLE else View.GONE
        textError?.visibility =
                if (viewState.error && isEmpty) View.VISIBLE else View.GONE
        textError?.text = viewState.throwable?.message
        btnRetry?.visibility =
                if (viewState.error && isEmpty) View.VISIBLE else View.GONE
//        refreshLayout?.visibility = if(viewState.completed || !isEmpty) View.VISIBLE else View.GONE
        loadMoreProgress?.visibility =
                if (viewState.loadingMore) View.VISIBLE else View.GONE
        refreshLayout?.isRefreshing = viewState.refreshing

    }

    abstract fun createAdapter(): BindingListAdapter<M>


}