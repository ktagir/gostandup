package su.tagir.lib.android.mvp;

import android.app.Dialog;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public class ProgressDialog extends DialogFragment {

    public static ProgressDialog show(FragmentManager fm){
        ProgressDialog dialog = getInstance();
        dialog.show(fm, ProgressDialog.class.getCanonicalName());
        return dialog;
    }

    public static ProgressDialog getInstance(){
        ProgressDialog dialog = new ProgressDialog();
        dialog.setCancelable(false);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setView(R.layout.dialog_progress);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() != null && getDialog() != null && getDialog().getWindow() != null)
        try {
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(getDialog().getWindow().getAttributes());
            lp.width = (int) (getActivity().getResources().getDisplayMetrics().density * 240);
            getDialog().getWindow().setAttributes(lp);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}