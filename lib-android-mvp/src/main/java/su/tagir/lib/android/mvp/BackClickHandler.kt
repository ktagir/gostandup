package su.tagir.lib.android.mvp

interface BackClickHandler {

    fun onBackPressed()
}