package su.tagir.lib.android.mvp.delegate

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import su.tagir.lib.mvp.MvpPresenter
import su.tagir.lib.mvp.MvpView


interface FragmentMvpDelegate<V : MvpView, P : MvpPresenter<V>> {
    /**
     * Must be called from [Fragment.onCreate]
     *
     * @param saved The bundle
     */
    fun onCreate(saved: Bundle?)

    /**
     * Must be called from [Fragment.onDestroy]
     */
    fun onDestroy()

    /**
     * Must be called from [Fragment.onViewCreated]
     *
     * @param view The inflated view
     * @param savedInstanceState the bundle with the viewstate
     */
    fun onViewCreated(view: View?, savedInstanceState: Bundle?)

    /**
     * Must be called from [Fragment.onDestroyView]
     */
    fun onDestroyView()

    /**
     * Must be called from [Fragment.onPause]
     */
    fun onPause()

    /**
     * Must be called from [Fragment.onResume]
     */
    fun onResume()

    /**
     * Must be called from [Fragment.onStart]
     */
    fun onStart()

    /**
     * Must be called from [Fragment.onStop]
     */
    fun onStop()

    /**
     * Must be called from [Fragment.onAttach]
     *
     * @param context The context the fragment is attached to
     */
    fun onAttach(context: Context?)

    /**
     * Must be called from [Fragment.onDetach]
     */
    fun onDetach()

    /**
     * Must be called from [Fragment.onSaveInstanceState]
     */
    fun onSaveInstanceState(outState: Bundle?)
}
